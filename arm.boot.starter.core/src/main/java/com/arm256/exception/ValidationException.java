package com.arm256.exception;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class ValidationException extends AbstractArmException {

  private final List<String> constrainValidation = new ArrayList(2);

  public ValidationException() {
    super("BUS_EXE_3", "entity.validation");
  }


  public ValidationException addValidation(String message) {
    constrainValidation.add(message);
    return this;
  }


  public String readValidation() {
    StringBuilder sb = new StringBuilder();
    for (String string : constrainValidation) {
      sb.append(string).append(" && ");
    }
    return sb.toString();
  }

  @Override
  public Object[] getArgs() {
    return new Object[] {readValidation()};
  }

}
