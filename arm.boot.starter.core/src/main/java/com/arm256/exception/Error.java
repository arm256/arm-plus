package com.arm256.exception;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.function.Consumer;

import com.arm256.utils.Pair;


public class Error {

	private final Pair<String, String>[] errors = new Pair[5];

	public Error(String code, String message) {
		errors[0] = Pair.of("timestamp", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
		if (code != null) {
			errors[1] = Pair.of("error-code", code);
		}
		if (message != null) {
			errors[2] = Pair.of("error-message", message);
		}
	}

	public void setError(String error) {
		errors[3] = Pair.of("error-body", error);
	}

	public void setTrackId(String trackId) {
		errors[4] = Pair.of("error-trackId", trackId);
	}

	public Pair[] getErrors() {
		return errors;
	}

	public void streamErrors(Consumer<? super Pair<String, String>> action) {
		Arrays.stream(errors).forEach(action);
	}

}
