package com.arm256.exception;

@SuppressWarnings("serial")
public class NotAcceptedException extends AbstractArmException {

	public NotAcceptedException() {
		super("BUS_EXE_1", "entity.notAccept");
	}

    public NotAcceptedException(String message) {
      super("BUS_EXE_1", "entity.notAccept.param");
      setArgs(message);
    }

}
