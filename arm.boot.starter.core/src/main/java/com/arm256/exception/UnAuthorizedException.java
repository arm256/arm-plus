package com.arm256.exception;

@SuppressWarnings("serial")
public class UnAuthorizedException extends AbstractArmException {

	public UnAuthorizedException() {
		super("BUS_EXE_5", "entity.unauthorized");
	}

}
