package com.arm256.exception;

public class UnsupportClassException extends AbstractArmException {

	private static final long serialVersionUID = 6659493823275332139L;

	public UnsupportClassException() {
		super("BUS_EXE_4", "request.validation.notSupport.class");
	}

}
