package com.arm256.exception;

@SuppressWarnings("serial")
public class MissingParamsException extends AbstractArmException {

	public MissingParamsException(String arg) {
		super("BUS_EXE_3", "entity.missing.param");
		setArgs(arg);
	}
}
