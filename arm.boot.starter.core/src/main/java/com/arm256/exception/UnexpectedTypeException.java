package com.arm256.exception;

public class UnexpectedTypeException extends AbstractArmException {

	private static final long serialVersionUID = -8029018158815650313L;

	public UnexpectedTypeException(String args) {
		super("BUS_EXE_5", "entity.notFound");
		setArgs(args);
	}

}
