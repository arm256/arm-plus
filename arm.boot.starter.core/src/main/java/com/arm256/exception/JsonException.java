package com.arm256.exception;

@SuppressWarnings("serial")
public class JsonException extends AbstractArmException {

	public JsonException() {
		super("BUS_EXE_2", "json.map.exception");
	}

}
