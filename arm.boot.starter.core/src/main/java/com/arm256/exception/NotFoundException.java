package com.arm256.exception;

@SuppressWarnings("serial")
public class NotFoundException extends AbstractArmException {

	public NotFoundException(String args) {
		super("BUS_EXE_0", "entity.notFound");
		setArgs(args);
	}

}
