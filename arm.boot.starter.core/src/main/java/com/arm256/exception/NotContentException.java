package com.arm256.exception;

@SuppressWarnings("serial")
public class NotContentException extends AbstractArmException {

	public NotContentException() {
		super("BUS_EXE_1", "entity.notContenct");
	}

}
