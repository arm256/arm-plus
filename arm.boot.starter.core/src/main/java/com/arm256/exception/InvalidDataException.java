package com.arm256.exception;

@SuppressWarnings("serial")
public class InvalidDataException extends AbstractArmException {

	public InvalidDataException(String code, String messageCode) {
		super(code, messageCode);
	}

	public InvalidDataException(String messageCode) {
		super("", "request.invalid");
		setArgs(messageCode);
	}

	public InvalidDataException() {
		super("", "request.validation");
	}
}