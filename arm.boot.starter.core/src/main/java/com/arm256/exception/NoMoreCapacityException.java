package com.arm256.exception;

@SuppressWarnings("serial")
public class NoMoreCapacityException extends AbstractArmException {

	public NoMoreCapacityException(String code, String messageCode) {
		super(code, messageCode);
	}

}
