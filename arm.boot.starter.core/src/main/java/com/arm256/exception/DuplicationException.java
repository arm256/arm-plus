package com.arm256.exception;

@SuppressWarnings("serial")
public class DuplicationException extends AbstractArmException {

	public DuplicationException(String args) {
      super("BUS_EXE_1", "entity.duplication");
		setArgs(args);
	}

}
