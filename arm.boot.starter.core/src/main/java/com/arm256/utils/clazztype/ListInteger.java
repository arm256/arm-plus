package com.arm256.utils.clazztype;

import java.util.ArrayList;
import java.util.Collection;

public class ListInteger extends ArrayList<Integer> {

	private static final long serialVersionUID = 3403106843057300080L;

	public ListInteger(int initialCapacity) {
		super(initialCapacity);
	}

	public ListInteger() {
		super();
	}

	public ListInteger(Collection<? extends Integer> c) {
		super(c);
	}

}
