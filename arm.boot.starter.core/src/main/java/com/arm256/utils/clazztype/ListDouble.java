package com.arm256.utils.clazztype;

import java.util.ArrayList;
import java.util.Collection;

public class ListDouble extends ArrayList<Double> {

	private static final long serialVersionUID = 1956086844973891896L;

	public ListDouble(int initialCapacity) {
		super(initialCapacity);
	}

	public ListDouble() {
		super();
	}

	public ListDouble(Collection<? extends Double> c) {
		super(c);
	}

}
