package com.arm256.utils.clazztype;

import java.util.ArrayList;
import java.util.Collection;

public class ListLong extends ArrayList<Long> {

	private static final long serialVersionUID = 7623431243731587012L;

	public ListLong(int initialCapacity) {
		super(initialCapacity);
	}

	public ListLong() {
		super();
	}

	public ListLong(Collection<? extends Long> c) {
		super(c);
	}

}
