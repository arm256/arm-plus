package com.arm256.utils.clazztype;

import java.util.ArrayList;
import java.util.Collection;

public class ListString extends ArrayList<String> {

	private static final long serialVersionUID = 63307578706111305L;

	public ListString(int initialCapacity) {
		super(initialCapacity);
	}

	public ListString() {
		super();
	}

	public ListString(Collection<? extends String> c) {
		super(c);
	}

}
