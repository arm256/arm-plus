package com.arm256.utils.clazztype;

import java.util.ArrayList;
import java.util.Collection;

public class ListBoolean extends ArrayList<Boolean> {

	private static final long serialVersionUID = 63307578706111305L;

	public ListBoolean(int initialCapacity) {
		super(initialCapacity);
	}

	public ListBoolean() {
		super();
	}

	public ListBoolean(Collection<? extends Boolean> c) {
		super(c);
	}

}
