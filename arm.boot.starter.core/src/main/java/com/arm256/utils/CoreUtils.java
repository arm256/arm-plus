package com.arm256.utils;

import org.apache.commons.lang3.StringUtils;

import com.arm256.exception.NotFoundException;
import com.jnape.palatable.lambda.adt.hlist.Tuple3;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CoreUtils {
	public static final Tuple3<String, String, String> readSchema(String path) {
		String[] seq = StringUtils.split(path, ".", 3);
		String schema = seq.length > 1 ? seq[0] : null;
		String element = seq.length > 1 ? seq[1] : null;
		String qualifier = seq.length == 3 ? seq[2] : null;
		if (schema == null || element == null) {
			throw new NotFoundException("schema or element");
		}
		return Tuple3.tuple(schema, element, qualifier);
	}
}
