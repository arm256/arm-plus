package com.arm256.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.springframework.lang.Nullable;
import com.arm256.exception.NotAcceptedException;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.NonNull;
import lombok.experimental.UtilityClass;

@UtilityClass
public class UriUtil {

  /** Captures URI template variable names. */
  private static final Pattern NAMES_PATTERN = Pattern.compile("\\{([^/]+?)\\}");

  public static Map<String, Object> readVars(@Nullable String source, JsonNode node) {

    if (source == null) {
      return Collections.emptyMap();
    }
    if (source.indexOf('{') == -1) {
      return Collections.emptyMap();
    }
    if (source.indexOf(':') != -1) {
      source = sanitizeSource(source);
    }
    Matcher matcher = NAMES_PATTERN.matcher(source);
    final Map<String, Object> uri = new HashMap<>(4);
    while (matcher.find()) {
      String match = matcher.group(1);
      String varName = getVariableName(match);
      uri.put(Matcher.quoteReplacement(varName), validateAndPrepareData(source, node));
    }
    return uri;
  }

  public static List<String> readVars(@Nullable String source) {

    if (source == null) {
      return Collections.emptyList();
    }
    if (source.indexOf('{') == -1) {
      return Collections.emptyList();
    }
    if (source.indexOf(':') != -1) {
      source = sanitizeSource(source);
    }
    Matcher matcher = NAMES_PATTERN.matcher(source);
    final List<String> uri = new ArrayList<>(4);
    while (matcher.find()) {
      String match = matcher.group(1);
      String varName = getVariableName(match);
      uri.add(Matcher.quoteReplacement(varName));
    }
    return uri;
  }

  public static List<String> readVars(@NonNull Collection<String> list) {
    return UriUtil.readVars(list.stream().collect(Collectors.joining("/")));
  }

  public static Map<String, Object> readVars(@NonNull Collection<String> list, JsonNode node) {
    return UriUtil.readVars(list.stream().collect(Collectors.joining("/")), node);
  }

  public static Map<String, String> readVarsString(@NonNull Collection<String> list,
      JsonNode node) {
    return UriUtil.readVars(list.stream().collect(Collectors.joining("/")), node).entrySet()
        .stream().collect(Collectors.toMap(Map.Entry::getKey, e -> (String) e.getValue()));
  }

  public static Map<String, Object> readVarsObject(@NonNull Collection<Object> list,
      JsonNode node) {

    return readVars(list.stream().filter(String.class::isInstance).map(String.class::cast)
        .collect(Collectors.joining("/")), node);
  }

  public static Object validateAndPrepareData(String path, JsonNode node) {
      if (path.isEmpty())
        path = "/";
      if (path.charAt(0) != '/')
        path = '/' + path;
      JsonNode value = node.at(path);
      if (value == null) {
        throw new NotAcceptedException("variable (" + path + ") is missing");
      }

      Boolean empty = value.isTextual() ? value.asText().isEmpty() : value.isEmpty();
      if (Boolean.TRUE.equals(empty)) {
        throw new NotAcceptedException("variable (" + path + ") is missing");
      }
     return value.isTextual() ? value.asText() : value;
  }



  /**
   * Remove nested "{}" such as in URI vars with regular expressions.
   */
  private static String sanitizeSource(String source) {
    int level = 0;
    int lastCharIndex = 0;
    char[] chars = new char[source.length()];
    for (int i = 0; i < source.length(); i++) {
      char c = source.charAt(i);
      if (c == '{') {
        level++;
      }
      if (c == '}') {
        level--;
      }
      if (level > 1 || (level == 1 && c == '}')) {
        continue;
      }
      chars[lastCharIndex++] = c;
    }
    return new String(chars, 0, lastCharIndex);
  }

  private static String getVariableName(String match) {
    int colonIdx = match.indexOf(':');
    return (colonIdx != -1 ? match.substring(0, colonIdx) : match);
  }

  private static String getVariableValueAsString(@Nullable Object variableValue) {
    return (variableValue != null ? variableValue.toString() : "");
  }


}
