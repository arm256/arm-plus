package com.arm256.utils;

import java.util.List;
import java.util.function.Function;
import com.arm256.utils.clazztype.ListBoolean;
import com.arm256.utils.clazztype.ListDouble;
import com.arm256.utils.clazztype.ListInteger;
import com.arm256.utils.clazztype.ListLong;
import com.arm256.utils.clazztype.ListString;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.experimental.UtilityClass;

@UtilityClass
public class SupportClazz {

  @SuppressWarnings("rawtypes")
  public static Class[] clazzs =
      {String.class, Integer.class, Double.class, Long.class, Boolean.class, List.class};

  @SuppressWarnings("rawtypes")
  public static Class[] clazzsList =
      {ListString.class, ListInteger.class, ListDouble.class, ListLong.class, ListBoolean.class};

  public static boolean isClassSupported(String clazzName) {
    try {

      Class<?> clazz = Class.forName(clazzName);
      return isClassSupported(clazz);
    } catch (ClassNotFoundException e) {
    }

    return false;
  }

  /**
   * @param clazz Class
   * @return index of class supported or -1 if not Exits
   */
  public static int readClassIndex(Class<?> clazz) {

    int i;

    for (i = 0; i < clazzs.length; i++) {
      if (clazz == clazzs[i]) {
        return i;
      }
    }

    for (i = 0; i < clazzsList.length; i++) {
      if (clazz == clazzsList[i]) {
        return i;
      }
    }

    return -1;
  }

  public static boolean isClassSupported(Class<?> clazz) {

    for (Class<?> c : clazzs) {
      if (clazz == c) {
        return true;
      }
    }
    for (Class<?> c : clazzsList) {
      if (clazz == c) {
        return true;
      }
    }

    return false;
  }

  public static Function<JsonNode, String> stringFun = e -> {
    return e.asText();
  };

  public static Function<JsonNode, String> integerFun = e -> {
    if (e.isInt()) {
      return e.asText();
    } else {
      if (e.canConvertToInt()) {
        return e.asInt() + "";
      }
      return null;
    }
  };

  public static Function<JsonNode, String> doubleFun = e -> {
    if (e.isDouble()) {
      return e.asText();
    } else {
      if (e.isInt() || e.isLong()) {
        return e.asDouble() + "";
      }
      return null;
    }

  };
  public static Function<JsonNode, String> longFun = e -> {
    if (e.isLong()) {
      return e.asText();
    } else {
      if (e.canConvertToLong()) {
        return e.asLong() + "";
      }
      return null;
    }
  };
  public static Function<JsonNode, String> booleanFun = e -> {
    return e.isBoolean() ? e.asText() : null;
  };
  public static Function<JsonNode, String> arrayFun = e -> {
    return e.isArray() ? "" : null;
  };

  @SuppressWarnings({"unchecked"})
  public static Function<JsonNode, String>[] functions =
      new Function[] {stringFun, integerFun, doubleFun, longFun, booleanFun, arrayFun};

}
