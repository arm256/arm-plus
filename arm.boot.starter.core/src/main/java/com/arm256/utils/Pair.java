package com.arm256.utils;

import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public final class Pair<S, T> {

	private final @NonNull S key;
	private final @NonNull T values;
	private Pair (S key,T values) {
		this.key = key;
		this.values = values;
	}
	/**
	 * Creates a new {@link Pair} for the given elements.
	 *
	 * @param key  must not be {@literal null}.
	 * @param values must not be {@literal null}.
	 * @return
	 */
	public static <S, T> Pair<S, T> of(S key, T values) {
		return new Pair<>(key, values);
	}

	/**
	 * Returns the first element of the {@link Pair}.
	 *
	 * @return
	 */
	public S getKey() {
		return key;
	}

	/**
	 * Returns the second element of the {@link Pair}.
	 *
	 * @return
	 */
	public T getValues() {
		return values;
	}

	/**
	 * A collector to create a {@link Map} from a {@link Stream} of {@link Pair}s.
	 *
	 * @return
	 */
	public static <S, T> Collector<Pair<S, T>, ?, Map<S, T>> toMap() {
		return Collectors.toMap(Pair::getKey, Pair::getValues);
	}
}