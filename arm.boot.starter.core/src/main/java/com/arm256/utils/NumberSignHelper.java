package com.arm256.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.OptionalInt;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.LongToIntFunction;
import java.util.function.ToIntFunction;

public class NumberSignHelper {

	private static final short SHORT_ZERO = (short) 0;

	private static final byte BYTE_ZERO = (byte) 0;

	public static final OptionalInt LESS_THAN = OptionalInt.of(-1);
	public static final OptionalInt FINITE_VALUE = OptionalInt.empty();
	public static final OptionalInt GREATER_THAN = OptionalInt.of(1);

	public static Function<Long,Integer> longFun = Long::signum;
	public static Function<Integer,Integer> intFun = Integer::signum;
	public static Function<Short,Integer> shortFun = e -> e.compareTo(SHORT_ZERO);
	public static Function<Byte,Integer> byteFun = e -> e.compareTo(BYTE_ZERO);
	public static Function<BigInteger,Integer> bigIntFun = e -> e.signum();
	public static Function<BigDecimal,Integer> bigDecimalFun = e -> e.signum();

	public static Function<Float, Integer> floatFunLess = e -> signum(e, LESS_THAN);
	public static Function<Double, Integer> doubleFunLess = e -> signum(e, LESS_THAN);

	public static Function<Float, Integer> floatFunFinite = e -> signum(e, FINITE_VALUE);
	public static Function<Double, Integer> doubleFunFinite = e -> signum(e, FINITE_VALUE);

	public static Function<Float, Integer> floatFunGreate = e -> signum(e, GREATER_THAN);
	public static Function<Double, Integer> doubleFunGreate = e -> signum(e, GREATER_THAN);

	public static Function<Number, Integer> numberFun = e -> Double.compare(e.doubleValue(), 0D);

	public static Class<?>[] clazzs = new Class[] { Long.class, Integer.class, Short.class, Byte.class,
			BigInteger.class, BigDecimal.class, Float.class, Double.class, Number.class };

	@SuppressWarnings("unchecked")
	public static Function<Object, Integer>[] funs = new Function[] { longFun, intFun, shortFun, byteFun,
			bigIntFun, bigDecimalFun, floatFunLess, doubleFunLess, floatFunFinite, doubleFunFinite, floatFunGreate,
			doubleFunGreate, numberFun };


	public static <E extends Number> int signumLess(E e) {
		int index = clazzIndex(e);

		return funs[index].apply(e);
	}

	public static <E extends Number> int signumFinite(E e) {
		int index = clazzIndex(e);

		if (e.getClass().isAssignableFrom(Float.class) || e.getClass().isAssignableFrom(Double.class)) {
			index += 2;
		}
		return funs[index].apply(e);
	}

	public static <E extends Number> int signumGreater(E e) {
		int index = clazzIndex(e);

		if (e.getClass().isAssignableFrom(Float.class) || e.getClass().isAssignableFrom(Double.class)) {
			index += 4;
		}
		return funs[index].apply(e);
	}

	private static <E extends Number> int clazzIndex(E e) {
		int index = -1;

		loop: for (Class<?> clazz : clazzs) {
			index++;
			if (e.getClass().isAssignableFrom(clazz)) {
				break loop;
			}
		}
		return index;
	}

	static int signum(Float number, OptionalInt treatNanAs) {
		OptionalInt infinity = InfinityNumberComparatorHelper.infinityCheck(number, treatNanAs);
		if (infinity.isPresent()) {
			return infinity.getAsInt();
		}
		return number.compareTo(0F);
	}

	static int signum(Double number, OptionalInt treatNanAs) {
		OptionalInt infinity = InfinityNumberComparatorHelper.infinityCheck(number, treatNanAs);
		if (infinity.isPresent()) {
			return infinity.getAsInt();
		}
		return number.compareTo(0D);
	}
}
