package com.arm256.validator;


import java.lang.annotation.Annotation;
import com.arm256.utils.Pair;

public interface ParamsConstraintValidator<A extends Annotation> {
	public static Object[] EMPTY = new Object[] {};
	public static Parameters<?>[] EMPTY_PARAM = new Parameters[] {};

	default Object[] validatorParam(Pair<String, String>[] constraintParameter) {
		return EMPTY;
	}

	@SuppressWarnings("rawtypes")
	public default Parameters[] getParameters() {
		return EMPTY_PARAM;
	}

}
