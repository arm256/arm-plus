package com.arm256.validator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import org.apache.commons.lang3.StringUtils;

public class Parameters<T extends Serializable> implements ParameterInterface<T> {


	@SuppressWarnings("unchecked")
	public Parameters(String name, boolean multi, boolean required, Class<T> clazzType) {
		this.name = name;
		this.clazzType = clazzType;
		this.multi = multi;
		this.required = required;
		this.index = readClassPosition(clazzType);
	}

	public Parameters(String name, boolean multi, boolean required, Class<T> clazzType, T dv) {
		this(name, multi, required, clazzType);
		defalutValue = dv;
	}

	private String name;

	private Class<T> clazzType;

	private boolean multi = false;

	private boolean required = false;

	private T defalutValue;

	private int index;

	public String getName() {
		return name;
	}

	public Class<T> getClazzType() {
		return clazzType;
	}

	public boolean isMulti() {
		return multi;
	}

	public boolean isRequired() {
		return required;
	}

	public T getDefalutValue() {
		return defalutValue;
	}

	@SuppressWarnings("unchecked")
	public List<T> converterValue(String value) {
		if (value == null || value.isEmpty()) {
			return Collections.emptyList();
		}
		if (multi) {
			List<T> arr = new ArrayList<>();
			for (String su : StringUtils.split(value, ',')) {
				arr.add((T) functions[index].apply(su));
			}
		}
		return Collections.singletonList((T) functions[index].apply(value));
	}

	public T converterValueFirst(String value) {
		List<T> list = converterValue(value);
		if (list.isEmpty()) {
			return null;
		}
		return converterValue(value).get(0);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <E extends Enum> Class<E> readEnumClazzType() {
		return (Class<E>) clazzType;
	}

	public int readClassPosition(Class<?> clazz) {
		int position = -1;

		for (Class<?> c : clazzs) {
				position++;
				if (clazz == c) {
					return position;
				}
			}

		return -1;
	}

	private Function<String, String> stringFun = Function.identity();

    private Function<String, Integer> integerFun = Integer::parseInt;

    private Function<String, Double> doubleFun = Double::parseDouble;

    private Function<String, Long> longFun = Long::parseLong;

    private Function<String, Boolean> booleanFun = Boolean::parseBoolean;

	private Function<String, Enum<?>> enumFun = e -> Enum.valueOf(readEnumClazzType(), e);

	@SuppressWarnings("rawtypes")
	private Function[] functions = { stringFun, integerFun, doubleFun, longFun, booleanFun, enumFun };

}
