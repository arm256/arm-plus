package com.arm256.validator;

import com.arm256.utils.Pair;

public interface ConstraintValidator<T> {
	@SuppressWarnings("unchecked")
	default int initialize(Pair<String, String>... constraintParameter) {
		return -1;
	}

	/**
	 * Implements the validation logic. The state of {@code value} must not be
	 * altered.
	 * <p>
	 * This method can be accessed concurrently, thread-safety must be ensured by
	 * the implementation.
	 *
	 * @param value   object to validate
	 * @param context context in which the constraint is evaluated
	 *
	 * @return {@code false} if {@code value} does not pass the constraint
	 */
	boolean isValid(T value, int context);

}
