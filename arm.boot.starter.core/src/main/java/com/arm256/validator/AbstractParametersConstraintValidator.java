package com.arm256.validator;

import java.io.Serializable;
import java.lang.annotation.Annotation;

import com.arm256.utils.Pair;

public abstract class AbstractParametersConstraintValidator<A extends Annotation>
		implements ParamsConstraintValidator<A>
{


	private final Parameters<?>[] parameters;
	private int requiredSize = 0;

	public AbstractParametersConstraintValidator(Parameters<?>... parameters) {

		if (parameters == null || parameters.length == 0) {
			this.parameters = EMPTY_PARAM;
			return;
		}
		for (Parameters<?> para : parameters) {
			if (para.isRequired()) {
				requiredSize++;
			}
		}

		this.parameters = parameters;
	}

	public AbstractParametersConstraintValidator() {
		requiredSize = 0;
		parameters = EMPTY_PARAM;
	}

	protected void setRequiredSize(int requiredSize) {
		this.requiredSize = requiredSize;
	}

	@Override
	public Parameters<?>[] getParameters() {
		return parameters;
	}

	@SuppressWarnings("unchecked")
	protected <E extends Serializable> Parameters<E> getParameters(int index) {
		return (Parameters<E>) parameters[index];
	}

	@SuppressWarnings("unchecked")
	protected <E> E getParameterValue(int index, String paramName,
			Pair<String, String>[] constraintParameter) {

		String v = readParam(paramName, constraintParameter);

		if (v == null) {

			if (parameters[index].isRequired()) {
				throw new IllegalArgumentException("Parameter {" + paramName + "}   Not found  ");
			}
			return (E) parameters[index].getDefalutValue();
		}

		E value = (E) parameters[index].converterValueFirst(v);

		if (value == null) {
			return (E) parameters[index].getDefalutValue();
		}

		return value;
	}


	protected void requiredParam(Pair<String, String>[] constraintParameter) {
		if (requiredSize == 0) {
			return;
		}
		if (constraintParameter == null || constraintParameter.length < requiredSize) {
			throw new IllegalArgumentException("Invalid parameter size");
		}
	}


	private String readParam(String paramName, Pair<String, String>[] paramseters) {
		for (Pair<String, String> pair : paramseters) {
			if (pair.getKey().equals(paramName)) {
				return pair.getValues();
			}
		}
		return null;
	}

}
