package com.arm256.validator;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.arm256.annotation.Variable;
import com.arm256.model.ThirdPartRequest;
import com.arm256.utils.UriUtil;

import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.ConstraintValidator;

public class VariableValidator implements ConstraintValidator<Variable, ThirdPartRequest> {

  @Override
  public boolean isValid(ThirdPartRequest dto, ConstraintValidatorContext context) {
    return validateRequest(readVars(dto), dto.getVars(), context);
  }

  public boolean validateRequest(List<String> variables, Map<String, Object> vars,
      ConstraintValidatorContext context) {
    for (String key : variables) {
      if (!vars.containsKey(key)) {
        customMessageForValidation(context, "variable (" + key + ") is missing");
        return false;
      }
    }
    return true;
  }

  public List<String> readVars(ThirdPartRequest dto) {
    List<String> url = UriUtil.readVars(dto.getUrl());
    List<String> headers = UriUtil.readVars(dto.getHeaders().values());
    List<String> params = UriUtil.readVars(dto.getParams().values());
    List<String> body = UriUtil.readVars(dto.getBody().values().stream()
        .filter(String.class::isInstance).map(String.class::cast).collect(Collectors.joining("/")));

    return Stream.of(headers, params, body, url).flatMap(Collection::stream)
        .collect(Collectors.toList());
  }


  private void customMessageForValidation(ConstraintValidatorContext constraintContext,
      String message) {
    constraintContext.disableDefaultConstraintViolation();
    constraintContext.buildConstraintViolationWithTemplate(message).addConstraintViolation();
  }

}
