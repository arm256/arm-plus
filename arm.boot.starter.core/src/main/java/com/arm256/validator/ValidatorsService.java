package com.arm256.validator;

import java.util.List;
import com.arm256.model.MetadataField;
import com.arm256.utils.Pair;

public interface ValidatorsService {

  void validate(Object value, MetadataField field);

  @SuppressWarnings("rawtypes")
  AbstractConstraintValidator readConstaintValidator(String name);

  List<Validators> readValidators();

  int addValidators(String name, Pair<String, String>[] params);

  int addValidators(String name, Pair<String, String>[] params, Class<?> clazzType);

}
