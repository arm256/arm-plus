package com.arm256.validator.extra;

import java.util.regex.Pattern;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class HexaColorValidator implements ConstraintValidator<HexaColor, String> {
  /*
   * Regix for Hexa Color
   */
  private static final Pattern PATTERN = Pattern.compile("^#[a-fA-F0-9]{6}$|^#[a-fA-F0-9]{3}$");

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return value != null && PATTERN.matcher(value).matches();
  }
}
