package com.arm256.validator.extra;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberType;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber.CountryCodeSource;

public class PhonenumberValidator implements ConstraintValidator<PhoneNumber, String> {

  private static final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
  private CountryCodeSource countryCode;
  private PhoneNumberType phoneType;

  @Override
  public void initialize(PhoneNumber constraintAnnotation) {
    countryCode = constraintAnnotation.countryCode();
    phoneType = constraintAnnotation.phoneType();
    ConstraintValidator.super.initialize(constraintAnnotation);
  }
  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    try {

      return value != null && phoneNumberUtil.isPossibleNumberForType(
          phoneNumberUtil.parse(value, countryCode.name()), phoneType);
    } catch (NumberParseException e) {
      return false;
    }

  }
}
