package com.arm256.validator;

import java.util.List;

import com.arm256.utils.clazztype.ListBoolean;
import com.arm256.utils.clazztype.ListDouble;
import com.arm256.utils.clazztype.ListInteger;
import com.arm256.utils.clazztype.ListLong;
import com.arm256.utils.clazztype.ListString;

public interface ParameterInterface<T> {

	@SuppressWarnings("unchecked")
	public List<T> converterValue(String value);

	public T converterValueFirst(String value);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <E extends Enum> Class<E> readEnumClazzType();

	public int readClassPosition(Class<?> clazz);

	@SuppressWarnings("rawtypes")
	public static Class[] clazzs = { String.class, Integer.class, Double.class, Long.class, Boolean.class, Enum.class };
	@SuppressWarnings("rawtypes")
	public static Class[] clazzsList = { ListString.class, ListInteger.class, ListDouble.class, ListLong.class,
			ListBoolean.class };

}
