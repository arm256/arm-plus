package com.arm256.entity;

import java.io.Serializable;

public interface Persistable<T extends Serializable> extends Loadable<T> {
	
	long getVersion();
	
	boolean isArchive();
	
	String getCreatedBy();
	
	void setArchive(boolean archive);
}
