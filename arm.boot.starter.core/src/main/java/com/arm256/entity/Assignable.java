package com.arm256.entity;

import java.io.Serializable;
import java.util.List;

public interface Assignable<T extends Serializable> extends Persistable<T> {
	
	List<String> assigned();

	List<String> inActiveAssigned();
	
}
