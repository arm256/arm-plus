package com.arm256.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.stereotype.Service;
import com.arm256.exception.NotFoundException;

@Service
public class AbstractStoreMapService<T> {
	private final Map<String, List<T>> serviceMap = new HashMap<>();

	private final Object lock = new Object();

	public Map<String, List<T>> readAll() {
		return serviceMap;
	}

	public List<T> read(String schema) {

		List<T> d = serviceMap.get(schema);

		if (d == null) {
			throw new NotFoundException(schema + " Map ");
		}

		return d;
	}

	public List<T> read(List<String> schema) {

		return schema.stream().flatMap(e -> {

			List<T> listMap = serviceMap.get(e);

			if (listMap == null) {
				return Stream.empty();
			}
			return listMap.stream();

		}).collect(Collectors.toList());

	}

	public T readFirst(String schema) {
		return read(schema).get(0);
	}

    public synchronized void add(String schema, List<? extends T> ds) {
		for (T metadataMap : ds) {
			add(schema, metadataMap);
		}
	}

	private void addElementToList(List<T> elements, T element) {
		for (int i = 0; i < elements.size(); i++) {
			T t = elements.get(i);

			if (t.equals(element)) {
				elements.set(i, element);
				return;
			}
		}
		elements.add(element);

	}

	public synchronized void add(String schema, T d) {
		synchronized (lock) {
			List<T> metadatas = serviceMap.get(schema);
			if (metadatas == null) {
				metadatas = new ArrayList<>();
				serviceMap.put(schema, metadatas);
			}
			addElementToList(metadatas, d);
		}
	}

	public void clearAll() {
		serviceMap.clear();
	}
}
