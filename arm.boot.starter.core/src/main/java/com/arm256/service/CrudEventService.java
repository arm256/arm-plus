package com.arm256.service;

import org.springframework.context.ApplicationEvent;
import com.arm256.model.CrudEvent;

public class CrudEventService extends ApplicationEvent {

  private static final long serialVersionUID = -3492632445754200192L;

  public CrudEventService(CrudEvent<?> source) {
    super(source);
  }

}
