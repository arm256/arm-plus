package com.arm256.service;

import java.util.function.Function;
import com.arm256.model.ThirdPartRequest;

public interface Caller<T> extends Function<ThirdPartRequest, T> {
}
