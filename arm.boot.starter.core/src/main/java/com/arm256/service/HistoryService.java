package com.arm256.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import com.arm256.enumation.ExcuteStatus;
import com.arm256.model.HistoryField;
import com.arm256.model.HistoryRows;

public interface HistoryService {

	void addHistoryField(HistoryField history);

	List<HistoryField> readHistories();

	List<HistoryRows> readHistory(String fieldName);

	List<HistoryRows> readHistory(String fieldName, LocalDateTime after);

	List<HistoryRows> readHistory(String fieldName, ExcuteStatus status);

	List<HistoryRows> readHistory(String fieldName, ExcuteStatus status, LocalDateTime after);

	List<HistoryRows> readHistory(String fieldName, LocalDateTime from, LocalDateTime to);

	void archive(LocalDateTime before);

	void increaseRetryCount(UUID trackId);

	void rowComplete(UUID trackId);

	void rowFailed(UUID trackId);

	void rowStatus(UUID trackId, ExcuteStatus status);

	void addHistoryRows(String fieldName, HistoryRows row);


}
