package com.arm256.service.operation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum OperatorType {
  NONE(new NONEOperator()), EQUAL(new EqualOperator()), GT(new GreaterThanOperator()), GTE(
      new GreaterThanOrEqualOperator()), IN(new InOperator()), LT(new LessThanOperator()), LTE(
          new LessThanOrEqualOperator()), NOT_EQUAL(
              new NotEqualOperator()), NOT_IN(new NotInOperator());


  private final Operator operator;
}
