package com.arm256.service.operation;

public class NotEqualOperator implements Operator {

  private static final NotEqualOperator instance = new NotEqualOperator();

  public boolean calculate(Object val, String ref) {
    return !EqualOperator.instance().calculate(val, ref);
  }

  public static NotEqualOperator instance() {
    return instance;
  }

}
