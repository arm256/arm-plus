package com.arm256.service.operation;

public interface Operator {

	boolean calculate(Object val, String ref);

}
