package com.arm256.service.operation;

public class GreaterThanOrEqualOperator implements Operator {

  private static final GreaterThanOrEqualOperator instance = new GreaterThanOrEqualOperator();

  public boolean calculate(Object val, String ref) {
    return !LessThanOperator.instance().calculate(val, ref);
  }

  public static GreaterThanOrEqualOperator instance() {
    return instance;
  }
}
