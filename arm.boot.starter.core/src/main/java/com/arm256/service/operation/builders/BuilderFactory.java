package com.arm256.service.operation.builders;


import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class BuilderFactory {

  private Map<String, Builder> builderMap;

  @Lazy
  @Autowired
  public void setConditionsMap(List<Builder> convertor) {
    builderMap = convertor.stream().collect(Collectors.toMap(Builder::name, Function.identity()));
  }

  public <T> Builder<T> getOperator(String index) {
    Builder<T> conv = builderMap.get(index);
    if (conv == null) {
      return NONEBuilder.instance();
    }
    return conv;
  }


}
