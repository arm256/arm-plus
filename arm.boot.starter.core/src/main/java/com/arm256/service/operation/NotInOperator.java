package com.arm256.service.operation;

public class NotInOperator implements Operator {
  private static final NotInOperator instance = new NotInOperator();

  public boolean calculate(Object val, String ref) {
    return !InOperator.instance().calculate(val, ref);
  }

  public static NotInOperator instance() {
    return instance;
  }

}
