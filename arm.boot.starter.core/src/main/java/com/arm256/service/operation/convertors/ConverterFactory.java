package com.arm256.service.operation.convertors;


import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
@Component
public class ConverterFactory {

  private Map<String, Converter> converterMap;

    @Lazy
    @Autowired
  public void setConditionsMap(List<Converter> convertor) {
    converterMap =
        convertor.stream().collect(Collectors.toMap(Converter::name, Function.identity()));
    }

    public <T> Converter<T> getOperator(String index) {
      Converter<T> conv = converterMap.get(index);
      if (conv == null) {
        return NONECoverter.instance();
      }
      return conv;
	}


}
