package com.arm256.service.operation.convertors;

import com.arm256.service.operation.OperatorType;

public class NONECoverter implements Converter {

  private static final NONECoverter noneConvert = new NONECoverter();
  @Override
  public OperatorType convert(Object val) {
    return OperatorType.NONE;
  }

  @Override
  public String name() {
    return "NONE";
  }


  public static Converter instance() {
    return noneConvert;
  }

}
