package com.arm256.service.operation;

public class NONEOperator implements Operator {

  @Override
  public boolean calculate(Object val, String ref) {
    return false;
  }

}
