package com.arm256.service.operation;


public class GreaterThanOperator implements Operator {

  private static final GreaterThanOperator instance = new GreaterThanOperator();
	public boolean calculate(Object val, String ref) {
		if(val instanceof Number) {
            return (((Number) val).doubleValue() > Double.valueOf(ref));
		}
		return false;
	}

    public static GreaterThanOperator instance() {
      return instance;
    }
}
