package com.arm256.service.operation.builders;

import com.arm256.service.operation.OperatorType;

public abstract class Builder<T> {

  public final void build(T val, OperatorType type) {
    switch (type) {
      case EQUAL:
        equal(val);
        break;
      case NOT_EQUAL:
        notEqual(val);
        break;
      case IN:
        in(val);
        break;
      case NOT_IN:
        notIn(val);
        break;
      case LT:
        lessThan(val);
        break;
      case LTE:
        lessThanlEqual(val);
        break;
      case GT:
        graterThan(val);
        break;
      case GTE:
        graterThanEqual(val);
        break;
      default:
        defaultMethod(val);
        break;
    }
  }

  abstract void equal(T val);

  abstract void notEqual(T val);

  abstract void in(T val);

  abstract void notIn(T val);

  abstract void lessThan(T val);

  abstract void lessThanlEqual(T val);

  abstract void graterThan(T val);

  abstract void graterThanEqual(T val);

  void defaultMethod(T val) {

  }

  abstract String name();
}
