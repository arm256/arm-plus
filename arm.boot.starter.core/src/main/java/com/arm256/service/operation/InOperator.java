package com.arm256.service.operation;

import java.util.Arrays;
import java.util.List;

public class InOperator implements Operator {
  private static final InOperator instance = new InOperator();
	public boolean calculate(Object val, String ref) {

		List<String> refList = Arrays.asList(ref.split(","));

		if (val instanceof List) {
			for (Object value : ((List<?>) val)) {
				if (refList.contains(String.valueOf(value))) {
					return true;
				}
			}
		}

		if (val instanceof String) {
			for (String element : refList) {
				if (String.valueOf(val).equals(element.trim())) {
					return true;
				}
			}
		}
		return false;
	}

    public static InOperator instance() {
      return instance;
    }
}
