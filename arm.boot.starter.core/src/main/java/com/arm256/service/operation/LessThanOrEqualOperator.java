package com.arm256.service.operation;

public class LessThanOrEqualOperator implements Operator{

  private static final LessThanOrEqualOperator instance = new LessThanOrEqualOperator();

    public boolean calculate(Object val, String ref){
      return !GreaterThanOperator.instance().calculate(val, ref);
    }

    public static LessThanOrEqualOperator instance() {
      return instance;
    }
}


