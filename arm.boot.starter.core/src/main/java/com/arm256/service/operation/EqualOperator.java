package com.arm256.service.operation;

import java.util.Collection;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class EqualOperator implements Operator {

  private static final EqualOperator instance = new EqualOperator();

  public boolean calculate(Object val, String ref) {

    if (val instanceof Collection) {
      final Collection<?> collection = (Collection<?>) val;
      for (Object collectionItem : collection) {
        if (StringUtils.equals(String.valueOf(collectionItem), ref)) {
          return true;
        }
      }
      return false;
    }

    if (val instanceof Map) {
      final Map<?, ?> map = (Map<?, ?>) val;
      for (Object listItem : map.keySet()) {
        if (StringUtils.equals(String.valueOf(listItem), ref)) {
          return true;
        }
      }
      return false;
    }

    if (NumberUtils.isCreatable(ref) && NumberUtils.isCreatable(String.valueOf(val))) {
      return NumberUtils.toDouble(ref) == NumberUtils.toDouble(String.valueOf(val));
    }

    return StringUtils.equals(String.valueOf(val), String.valueOf(ref));
  }

  public static EqualOperator instance() {
    return instance;
  }
}
