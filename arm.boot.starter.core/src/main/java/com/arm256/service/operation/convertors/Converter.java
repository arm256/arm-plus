package com.arm256.service.operation.convertors;

import com.arm256.service.operation.OperatorType;

public interface Converter<T> {

  public OperatorType convert(T val);

  public String name();
}
