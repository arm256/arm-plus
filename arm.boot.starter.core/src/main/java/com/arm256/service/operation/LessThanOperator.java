package com.arm256.service.operation;

public class LessThanOperator implements Operator {
  private static final LessThanOperator instance = new LessThanOperator();

  public boolean calculate(Object val, String ref) {
    if (val instanceof Number) {
      return (((Number) val).doubleValue() < Double.valueOf(ref));
    }
    return false;
  }

  public static LessThanOperator instance() {
    return instance;
  }

}
