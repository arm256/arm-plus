package com.arm256.service.operation.builders;

import javax.management.Query;

public class NONEBuilder extends Builder<Query> {

  private static final NONEBuilder instance = new NONEBuilder();

  @Override
  public String name() {
    return "NONE";
  }


  public static Builder instance() {
    return instance;
  }


  @Override
  public void equal(Query val) {
  }


  @Override
  void notEqual(Query val) {
    // TODO Auto-generated method stub

  }


  @Override
  void in(Query val) {
    // TODO Auto-generated method stub

  }


  @Override
  void notIn(Query val) {
    // TODO Auto-generated method stub

  }


  @Override
  void lessThan(Query val) {
    // TODO Auto-generated method stub

  }


  @Override
  void lessThanlEqual(Query val) {
    // TODO Auto-generated method stub

  }


  @Override
  void graterThan(Query val) {
    // TODO Auto-generated method stub

  }


  @Override
  void graterThanEqual(Query val) {
    // TODO Auto-generated method stub

  }

}
