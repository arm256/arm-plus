package com.arm256.service.operation.convertors;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.arm256.service.operation.OperatorType;

@Service
public class StringConverter implements Converter<String> {

  private static final Map<String, OperatorType> operator = new HashMap<>();
  static {
    operator.put("[]", OperatorType.IN);
    operator.put("![]", OperatorType.NOT_IN);
    operator.put("==", OperatorType.EQUAL);
    operator.put("!=", OperatorType.NOT_EQUAL);
    operator.put(">", OperatorType.GT);
    operator.put("<", OperatorType.LT);
    operator.put(">=", OperatorType.GTE);
    operator.put("<=", OperatorType.LTE);
  }

  @Override
  public OperatorType convert(String val) {
    OperatorType type = operator.get(val);
    if (type == null) {
      return OperatorType.NONE;
    }
   
    return type;
  }

  @Override
  public String name() {
    return "string";
  }

}

