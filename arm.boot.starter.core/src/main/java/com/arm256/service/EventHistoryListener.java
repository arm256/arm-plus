package com.arm256.service;

import java.time.Instant;
import java.time.ZoneId;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.arm256.model.HistoryEvent;
import com.arm256.model.HistoryRows;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class EventHistoryListener implements ApplicationListener<HistoryEvent> {

	@Autowired
	private HistoryService service;

	@Override
	public void onApplicationEvent(HistoryEvent event) {
		log.info("Start event : {} trackingId : {} ", event.getName(), event.getTrackingId());
		switch (event.getEvent()) {
		case CREATE:
			service.addHistoryRows(event.getName(),
					HistoryRows.builder().createdBy(event.getCreatedBy()).createdDate(
							Instant.ofEpochMilli(event.getTimestamp()).atZone(ZoneId.systemDefault()).toLocalDateTime())
							.trackingId(event.getTrackingId()).build());
			break;
		case COMPLETE:
			service.rowComplete(event.getTrackingId());
			break;
		case FAILD:
			service.rowFailed(event.getTrackingId());
			break;
		case RETRY_FAILD:
			service.increaseRetryCount(event.getTrackingId());
			break;
		default:
			break;
		}

	}
}
