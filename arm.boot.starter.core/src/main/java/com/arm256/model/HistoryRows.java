package com.arm256.model;

import java.time.LocalDateTime;
import java.util.UUID;

import com.arm256.enumation.ExcuteStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class HistoryRows extends AbstractModel<UUID> {
	
	@JsonProperty(access = Access.READ_ONLY)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createdDate;
	
	private UUID trackingId;
	
	@JsonProperty(access = Access.READ_ONLY)
	private String createdBy;

	ExcuteStatus status;

	int retryTimes;

}
