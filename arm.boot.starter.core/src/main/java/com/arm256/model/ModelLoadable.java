package com.arm256.model;

import java.io.Serializable;

import com.arm256.entity.Loadable;

public interface ModelLoadable<T extends Serializable> extends Loadable<T>, Serializable {
	void setId(T id);
}
