package com.arm256.model;

import java.net.URI;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import com.arm256.annotation.Variable;
import com.arm256.model.enums.RestMethod;
import com.arm256.utils.UriUtil;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Variable
public class ThirdPartRequest extends AbstractAuditableModel {
  private static final long serialVersionUID = 1L;
  @NotEmpty
  private String url;
  @NotNull
  private Map<String, String> headers;
  @NotNull
  private Map<String, Object> body;
  @NotNull
  private Map<String, String> params;
  @NotNull
  private Map<String, Object> vars;
  @NotNull
  private RestMethod method;


  public void prepareData(JsonNode node) {

    headers.putAll(UriUtil.readVarsString(headers.values(), node));
    params.putAll(UriUtil.readVarsString(params.values(), node));
    body.putAll(UriUtil.readVarsObject(body.values(), node));


    MultiValueMap<String, String> result = new LinkedMultiValueMap<>();
    for (Entry<String, String> entry : params.entrySet()) {
      result.put(entry.getKey(), Arrays.asList(entry.getValue()));
    }
    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
    URI x = builder.queryParams(result).build(vars);
    this.url = x.toString();
  }

}
