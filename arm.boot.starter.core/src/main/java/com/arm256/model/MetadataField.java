package com.arm256.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Builder
public class MetadataField extends AbstractModel<Long> {


  @JsonProperty(access = Access.READ_ONLY)
  @EqualsAndHashCode.Exclude
  @Singular("validator")
  private List<FieldValidators> validators;


}
