package com.arm256.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimestampRequestParam {

	private Date date; // member name doesn't need to be like request parameter

	private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	// must have the same name as the request param
	public Date getDate() {
		return date;
	}

	/**
	 * @param timestamp here we convert timestamp to Date, method name must be same
	 *                  as request parameter
	 */
	public void setDate(String timestamp) {
		long longParse = Long.parseLong(timestamp);
		this.date = new Date(longParse);
	}

	@Override
	public String toString() {
		return "timestamp2date : " + FORMAT.format(date);
	}

}
