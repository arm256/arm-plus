package com.arm256.model;

import java.util.Map;
import lombok.Data;

@Data
public class CrudEvent<T> {
  public interface Type {
  }

  public enum CrudType implements Type {
    CREATE, UPDATE, DELETE, READ, RESET, TRACHED
  }


  private Type type;
  private String name;
  private String createdBy;
  private T sourceId;

  private Map<String, String> attributes;


}
