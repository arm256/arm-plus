package com.arm256.model;

import java.util.ArrayList;
import java.util.List;

import com.arm256.utils.Pair;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FieldValidators extends AbstractModel<Long> {

	private static final long serialVersionUID = 6833317243585599065L;

	private String name;

	private List<ParamValues> params;

	public FieldValidators(String name) {
		this.name = name;
	}

	public FieldValidators addParam(String key, String value) {
		if (params == null) {
			params = new ArrayList<ParamValues>();
		}

		params.add(new ParamValues(key, value));

		return this;
	}

	public Pair<String, String>[] readParams() {
		if (params == null) {
			return null;
		}
		return params.stream().map(e -> Pair.of(e.getParam(), e.getValue())).toArray(Pair[]::new);

	}

}
