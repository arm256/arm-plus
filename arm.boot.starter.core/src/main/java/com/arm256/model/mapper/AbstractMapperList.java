package com.arm256.model.mapper;

import java.util.List;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import com.arm256.entity.Loadable;
import com.arm256.model.ModelLoadable;

public abstract class AbstractMapperList<E extends Loadable<?>, D extends ModelLoadable<?>> extends AbstractMapper<E, D>
		implements Converter<List<E>, List<D>>, Mapper<E, D> {

  protected AbstractMapperList(Class<E> eClazz, Class<D> dClazz) {
		super(eClazz, dClazz);
	}

	@Override
	public List<D> convert(MappingContext<List<E>, List<D>> context) {
		List<E> source = context.getSource();
		return toDto(source);
	}

}
