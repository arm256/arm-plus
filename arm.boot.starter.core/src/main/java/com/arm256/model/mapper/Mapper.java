package com.arm256.model.mapper;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.arm256.entity.Loadable;
import com.arm256.model.ModelLoadable;

public interface Mapper<E extends Loadable<?>, D extends ModelLoadable<?>> {

	E entityUpdate(E entity, D dto);

	D toDto(E entity);

	E toEntity(D dto);

	default Optional<D> toDtoMap(E entity) {
		return Optional.of(toDto(entity));
	}

	default Optional<E> toEntityMap(D dto) {
		return Optional.of(toEntity(dto));
	}

	default Stream<D> toDtoMap(Collection<E> entities) {
		if (entities == null || entities.isEmpty()) {
			return Stream.empty();
		}
		return entities.stream().map(this::toDto);
	}

	default Stream<E> toEntityMap(Collection<D> dto) {
		if (dto == null || dto.isEmpty()) {
			return Stream.empty();
		}
		return dto.stream().map(this::toEntity);
	}

	default List<D> toDto(Collection<E> entities) {
		if (entities == null || entities.isEmpty()) {
			return Collections.emptyList();
		}
		return toDtoMap(entities).collect(Collectors.toList());
	}

	default List<E> toEntity(Collection<D> dto) {
		if (dto == null || dto.isEmpty()) {
			return Collections.emptyList();
		}
		return toEntityMap(dto).collect(Collectors.toList());
	}

	default Stream<D> toDtoMap(Iterable<E> entities) {
		if (entities == null) {
			return Stream.empty();
		}
		return StreamSupport.stream(entities.spliterator(), false).map(this::toDto);
	}

	default Stream<E> toEntityMap(Iterable<D> dto) {
		if (dto == null) {
			return Stream.empty();
		}
		return StreamSupport.stream(dto.spliterator(), false).map(this::toEntity);
	}

	default List<D> toDto(Iterable<E> entities) {
		if (entities == null) {
			return Collections.emptyList();
		}
		return toDtoMap(entities).collect(Collectors.toList());
	}

	default List<E> toEntity(Iterable<D> dto) {
		if (dto == null) {
			return Collections.emptyList();
		}
		return toEntityMap(dto).collect(Collectors.toList());
	}
}
