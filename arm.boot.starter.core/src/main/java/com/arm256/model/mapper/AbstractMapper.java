package com.arm256.model.mapper;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;

import com.arm256.entity.Loadable;
import com.arm256.model.ModelLoadable;

import jakarta.annotation.PostConstruct;

public abstract class AbstractMapper<E extends Loadable<?>, D extends ModelLoadable<?>> implements Mapper<E, D> {

	@Autowired
	protected ModelMapper modelMapper;

	private Class<E> eClazz;
	private Class<D> dClazz;

	protected AbstractMapper(Class<E> eClazz, Class<D> dClazz) {
		this.eClazz = eClazz;
		this.dClazz = dClazz;
	}

	@PostConstruct
	public void init() {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
		modelMapper.getConfiguration().setAmbiguityIgnored(true);
		modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());

	}

	@Override
	public D toDto(E entity) {
		return modelMapper.map(entity, dClazz);
	}

	@Override
	public E toEntity(D dto) {
		return modelMapper.map(dto, eClazz);
	}

	public void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}
