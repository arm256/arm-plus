package com.arm256.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuppressWarnings({ "serial" })
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractAuditableModel extends AbstractPersistableModel implements Serializable {

	@JsonProperty(access = Access.READ_ONLY)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	LocalDateTime  createdDate;
	@JsonProperty(access = Access.READ_ONLY)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	LocalDateTime  lastModifiedDate;
	@JsonProperty(access = Access.READ_ONLY)
	String createdBy;
	@JsonProperty(access = Access.READ_ONLY)
	String lastModifiedBy;
}
