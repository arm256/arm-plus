package com.arm256.model.enums;

import org.springframework.http.HttpMethod;

public enum RestMethod {
	GET(HttpMethod.GET), POST(HttpMethod.POST), PUT(HttpMethod.PUT), PATCH(HttpMethod.PATCH), DELETE(HttpMethod.DELETE);
	private final HttpMethod method;
	
	private RestMethod(HttpMethod method) {
		this.method = method;
	}
	
	public HttpMethod getmethod() {
		return method;
	}
}
