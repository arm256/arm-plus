package com.arm256.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractModel<T extends Serializable> implements ModelLoadable<T> {
	
	private static final long serialVersionUID = 3815718731705692010L;
	
	@JsonProperty(access = Access.READ_ONLY)
	private T id;

}
