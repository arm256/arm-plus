package com.arm256.model;

import java.util.UUID;

import org.springframework.context.ApplicationEvent;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
public class HistoryEvent extends ApplicationEvent {

	private String name;

	private UUID trackingId;

	private String createdBy;

	private Eventz event = Eventz.CREATE;

	public enum Eventz {
		CREATE, RETRY_FAILD, FAILD, COMPLETE
	}

	public HistoryEvent(Object source) {
		super(source);
	}

}
