package com.arm256.reactive.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;

// Thanks to magdy
@Configuration
@Profile("development")
public class DevCorsConfiguration implements WebFluxConfigurer {
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/v1/**").allowedMethods("*").allowedHeaders("*").exposedHeaders("*");
	}
}