package com.arm256.reactive.service;

import java.util.UUID;

import com.arm256.entity.Persistable;
import com.arm256.model.ModelLoadable;

public interface CRUD_UUID<E extends Persistable<UUID>, D extends ModelLoadable<UUID>> extends CRUD<E, D, UUID> {

}
