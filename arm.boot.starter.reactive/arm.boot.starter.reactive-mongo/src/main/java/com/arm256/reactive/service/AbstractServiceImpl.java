package com.arm256.reactive.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import com.arm256.entity.Loadable;
import com.arm256.entity.Persistable;
import com.arm256.exception.JsonException;
import com.arm256.exception.NotAcceptedException;
import com.arm256.exception.NotContentException;
import com.arm256.exception.NotFoundException;
import com.arm256.model.CrudEvent;
import com.arm256.model.CrudEvent.CrudType;
import com.arm256.model.CrudEvent.Type;
import com.arm256.model.ExportTypes;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.Mapper;
import com.arm256.reactive.repository.Generic;
import com.arm256.reactive.service.Validator.Validators;
import com.arm256.reactive.utils.ReactivePredicate;
import com.arm256.service.CrudEventService;
import com.arm256.validator.tags.Crud;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Setter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public abstract class AbstractServiceImpl<E extends Persistable<ID>, D extends ModelLoadable<ID>, R extends Generic<E, ID>, ID extends Serializable>
    implements CRUD<E, D, ID> {

  protected R repository;
  protected Mapper<E, D> mapper;
  protected Class<E> entityClass;
  @Autowired
  protected ReactiveMongoTemplate template;
  protected final Mono<? extends E> notFound = Mono.error(new NotFoundException(name()));
  protected final Mono<? extends E> noContent = Mono.error(new NotContentException());
  protected final Flux<? extends E> noContentFlux = Flux.error(new NotContentException());
  protected final Function<E, D> toDto = e -> mapper.toDto(e);
  protected final Function<D, E> toEntity = d -> mapper.toEntity(d);
  protected final ObjectMapper objectMapper = new ObjectMapper();

  protected static final String ADDED_BY_PROPERTY = "createdBy";
  protected static final String DELETED_PROPERTY = "archive";
  @Autowired
  @Setter
  protected jakarta.validation.Validator validator;
  @Autowired
  @Setter
  protected ApplicationEventPublisher applicationEventPublisher;

  @SuppressWarnings("unchecked")
  protected AbstractServiceImpl(R repository, Mapper<E, D> mapper) {
    this.repository = repository;
    this.mapper = mapper;
    entityClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass())
        .getActualTypeArguments()[0];
  }

  public Mono<Long> countAll(String userId, boolean deleted) {
    return count(Criteria.where(DELETED_PROPERTY).is(deleted),
        Criteria.where(ADDED_BY_PROPERTY).is(userId));
  }

  public Mono<Long> countAllByAdmin(boolean deleted) {
    return count(Criteria.where(DELETED_PROPERTY).is(deleted));
  }

  public ReactivePredicate<D> validatorX() {
    return validatorX(null);
  }

  public ReactivePredicate<D> validatorX(Class<?> group) {
    return d -> {
      Set<ConstraintViolation<D>> validate;
      if (group != null) {
        validate = validator.validate(d, Crud.class, group);
      } else {
        validate = validator.validate(d);
      }
      if (!validate.isEmpty()) {
        return Mono.error(new ConstraintViolationException(validate));
      }
      return Mono.just(true);
    };
  }

  @Override
  public Mono<Long> count(Criteria... criterias) {
    Query query = new Query();

    for (Criteria criteria : criterias) {
      query.addCriteria(criteria);
    }
    return template.count(query, entityClass);
  }
  // CrudEventService


  protected CrudEvent<ID> addEvent(E e, Type type) {
    CrudEvent<ID> event = new CrudEvent<ID>();
    event.setType(type);
    event.setCreatedBy(e.getCreatedBy());
    event.setSourceId(e.getId());
    event.setName(name());
    return event;
  }

  public void pushEvent(CrudEvent<ID> e) {
    e.setName(name());
    applicationEventPublisher.publishEvent(getCrudEventService(e));
  }

  public CrudEventService getCrudEventService(CrudEvent<ID> e) {
    return new CrudEventService(e);
  }

  @Override
  public Flux<E> query(Criteria... criterias) {
    return query(Pageable.unpaged(), criterias);
  }

  @Override
  public Mono<E> querySingle(Criteria... criterias) {
    return querySingle(Pageable.unpaged(), criterias);
  }

  @Override
  public Flux<E> query(Pageable pageable, Criteria... criterias) {
    Query query = new Query();

    for (Criteria criteria : criterias) {
      query.addCriteria(criteria);
    }
    return template.find(query, entityClass);
  }

  @Override
  public Mono<E> querySingle(Pageable pageable, Criteria... criterias) {
    Query query = new Query();

    for (Criteria criteria : criterias) {
      query.addCriteria(criteria);
    }
    return template.findOne(query, entityClass);
  }



  @Override
  public Mono<D> create(D d) {
    return create(d, Validators.of(validatorX(), "validate"));
  }

  public Mono<D> create(D d, @SuppressWarnings("unchecked") Validators<D>... v) {
    return Validator.of(Mono.just(d)).getMono(Arrays.asList(v))
        .flatMap(e -> create(mapper.toEntity(e)))
        .map(mapper::toDto);
  }


  @Override
  public Mono<Void> update(D d) {
    return update(d, Validators.of(validatorX(), "validate"));
  }

  public Mono<Void> update(D d, @SuppressWarnings("unchecked") Validators<D>... v) {
    
    return Validator.of(Mono.just(d)).getMono(Arrays.asList(v))
        .flatMap(e -> 
        read(e).flatMap(entity -> repository.save(mapper.entityUpdate(entity, e))
            .doOnSuccess(x -> pushEvent(addEvent(x, CrudType.UPDATE)))
        ))
        .then();
  }

  @Override
  public Mono<E> create(E ee) {
    return repository.save(ee).doOnSuccess(e -> pushEvent(addEvent(e, CrudType.CREATE)));
  }

  @Override
  public Flux<E> reads(Query query) {
    return template.find(query, entityClass).switchIfEmpty(notFound);
  }

  @Override
  public Flux<D> retrieves(Pageable pageable) {
    return reads(pageable).map(mapper::toDto);
  }

  @Override
  public Flux<D> retrieves(Pageable pageable, String createdBy) {
    return reads(pageable, createdBy).map(mapper::toDto);
  }

  @Override
  public Flux<D> retrieves(Pageable pageable, List<String> createdBy) {
    return reads(pageable, createdBy).map(mapper::toDto);
  }


  @Override
  public Flux<D> retrievesTrashed(Pageable pageable) {
    return readsTrashed(pageable).map(mapper::toDto);
  }

  @Override
  public Flux<D> retrievesTrashed(Pageable pageable, String createdBy) {
    return readsTrashed(pageable, createdBy).map(mapper::toDto);
  }



  @Override
  public Mono<D> retrieve(Loadable<ID> id) {
    return read(id).map(mapper::toDto);
  }

  protected Mono<E> read(Query query) {
    return template.findOne(query, entityClass).switchIfEmpty(notFound);
  }



  @Override
  public Mono<Void> delete(Loadable<ID> id) {
    return readTrashed(id)
        .flatMap(
            ef -> repository.delete(ef).doOnSuccess(e -> pushEvent(addEvent(ef, CrudType.DELETE))))
        .then();
  }

  @Override
  public Mono<Void> deleteTrashed(String createdBy) {
    return readCreatedBy(createdBy)
        .flatMap(
            ef -> repository.delete(ef).doOnSuccess(e -> pushEvent(addEvent(ef, CrudType.DELETE))))
        .then();
  }

  protected Flux<E> readCreatedBy(String createdBy) {
    return repository.findByCreatedByAndArchiveTrue(createdBy).switchIfEmpty(noContentFlux);
  }

  @Override
  public <X> Mono<Void> trashed(Loadable<ID> id,
      Function<? super E, ? extends Mono<? extends X>> onNext) {
    return read(id).flatMap(e -> {
      return onNext.apply(e).flatMap(x -> {
        if (e.isArchive()) {
          return Mono.error(new NotAcceptedException());
        }
        e.setArchive(true);
        return repository.save(e);
      });
    }).doOnSuccess(e -> pushEvent(addEvent(e, CrudType.TRACHED))).then();
  }

  @Override
  public Mono<Void> trashed(Loadable<ID> id) {
    return trashed(id, Mono::just);
  }



  @Override
  public Mono<Void> reset(Loadable<ID> id) {
    return repository.findByIdAndArchiveTrue(id.getId()).switchIfEmpty(notFound).flatMap(e -> {
      e.setArchive(false);
      return repository.save(e);
    }).doOnSuccess(e -> pushEvent(addEvent(e, CrudType.RESET))).then();
  }

  @Override
  public Mono<Void> resetAll(String createdBy) {
    return readCreatedBy(createdBy).flatMap(ef -> {
      ef.setArchive(false);
      return repository.save(ef);
    }).doOnNext(e -> pushEvent(addEvent(e, CrudType.RESET))).then();

  }


  @Override
  public Mono<E> read(Loadable<ID> id) {
    return repository.findByIdAndArchiveFalse(id.getId()).switchIfEmpty(notFound);
  }

  @Override
  public Flux<E> readsTrashed(Pageable pageable) {
    if (pageable == null) {
      pageable = PageRequest.of(0, 20);
    }
    return repository.findByArchiveTrue(pageable);
  }

  @Override
  public Flux<E> readsTrashed(Pageable pageable, String createdBy) {
    if (pageable == null) {
      pageable = PageRequest.of(0, 20);
    }
    return repository.findByCreatedByAndArchiveTrue(pageable, createdBy);
  }


  @Override
  public Mono<E> readTrashed(Loadable<ID> id) {
    return repository.findByIdAndArchiveTrue(id.getId()).switchIfEmpty(notFound);
  }


  @Override
  public Flux<E> reads(Pageable pageable, List<String> createdBy) {
    if (pageable == null) {
      pageable = PageRequest.of(0, 20);
    }
    return repository.findByArchiveFalseAndCreatedByIn(pageable, createdBy);
  }

  @Override
  public Flux<E> reads(Pageable pageable) {
    if (pageable == null) {
      pageable = PageRequest.of(0, 20);
    }
    return repository.findAll(pageable);
  }

  @Override
  public Flux<E> reads(Pageable pageable, String createdBy) {
    if (pageable == null) {
      pageable = PageRequest.of(0, 20);
    }
    return repository.findByCreatedByAndArchiveFalse(pageable, createdBy);
  }

  @Override
  public Mono<InputStream> export(ExportTypes type, Pageable pageable) throws IOException {

    return retrieves(pageable).switchIfEmpty(Flux.error(new NotContentException())).collectList()
        .map(t -> {
          try {
            return objectMapper.writeValueAsBytes(t);
          } catch (JsonProcessingException e) {
            throw new JsonException().setArgs("error exporting");
          }
        }).map(ByteArrayInputStream::new);
  }



  @Override
  public Flux<D> imports(ExportTypes type, InputStream stream, Class<D> model) throws IOException {

    JavaType typemap = objectMapper.getTypeFactory().constructCollectionType(List.class, model);


    List<D> list = objectMapper.readValue(stream, typemap);

    return Flux.fromIterable(list).flatMap(this::create);

  }

  @SuppressWarnings("unchecked")
  @Override
  public R getRepository() {
    return repository;
  }

  @Override
  public Mapper<E, D> getMapper() {
    return mapper;
  }

  public abstract String name();

  // protected List<E> notEmpty(List<E> entities) {
  // if (entities.isEmpty()) {
  // throw notFound.get();
  // }
  // return entities;
  // }
}
