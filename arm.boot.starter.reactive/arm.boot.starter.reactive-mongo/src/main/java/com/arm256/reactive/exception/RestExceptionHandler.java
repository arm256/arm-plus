package com.arm256.reactive.exception;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.codec.DecodingException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.lang.Nullable;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ServerWebInputException;
import org.springframework.web.util.WebUtils;
import com.arm256.exception.AbstractArmException;
import com.arm256.exception.InvalidDataException;
import com.arm256.exception.JsonException;
import com.arm256.exception.MissingParamsException;
import com.arm256.exception.NoMoreCapacityException;
import com.arm256.exception.NotAcceptedException;
import com.arm256.exception.NotFoundException;
import com.arm256.exception.UnAuthorizedException;
import com.arm256.exception.ValidationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {

  private static final String X_ERROR_CODE = "X-error-code";
  private static final String X_ERROR_ID = "X-error-id";
  private static final String X_ERROR_TIMESTAMP = "X-error-timestamp";
  private static final String X_ERROR_MESSAGE = "X-error-message";
  private static final String ERROR_CODE = "ent-nv10";
  private static final Pattern ENUM_MSG = Pattern.compile("String.*\\[(.*)\\];", Pattern.MULTILINE);


  private final MessageSource messageSource;

  @Autowired
  public RestExceptionHandler(@Qualifier("exception_message") MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  /**
   * Log category to use when no mapped handler is found for a request.
   * 
   * @see #pageNotFoundLogger
   */
  public static final String PAGE_NOT_FOUND_LOG_CATEGORY =
      "org.springframework.web.servlet.PageNotFound";

  /**
   * Specific logger to use when no mapped handler is found for a request.
   * 
   * @see #PAGE_NOT_FOUND_LOG_CATEGORY
   */
  protected static final Log pageNotFoundLogger = LogFactory.getLog(PAGE_NOT_FOUND_LOG_CATEGORY);



  @ExceptionHandler(value = {ConversionNotSupportedException.class})
  public ResponseEntity<Object> conversionNotSupportedException(ConversionNotSupportedException ex
      ) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    return handleExceptionInternal(ex, null, headers, status,  "SYS_REQ_0",
        "internal error");
  }

  @ExceptionHandler(value = {TypeMismatchException.class})
  public ResponseEntity<Object> typeMismatchException(TypeMismatchException ex
      ) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    return handleExceptionInternal(ex, null, headers, status,  "SYS_REQ_0", "Bad bean");
  }

  @ExceptionHandler(value = {HttpMessageNotReadableException.class})
  public ResponseEntity<Object> httpMessageNotReadableException(HttpMessageNotReadableException ex
      ) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.BAD_REQUEST;
    return handleExceptionInternal(ex, null, headers, status,  "SYS_REQ_0", "Bad bean");
  }

  @ExceptionHandler(value = {HttpMessageNotWritableException.class})
  public ResponseEntity<Object> httpMessageNotWritableException(HttpMessageNotWritableException ex
      ) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    return handleExceptionInternal(ex, null, headers, status,  "SYS_REQ_0", "Bad bean");
  }

  @ExceptionHandler(value = {MethodArgumentNotValidException.class})
  public ResponseEntity<Object> methodArgumentNotValidException(MethodArgumentNotValidException ex
      ) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.BAD_REQUEST;
    return handleExceptionInternal(ex, null, headers, status,  "SYS_REQ_0",
        "Bad validation");
  }

  @ExceptionHandler(value = {BindException.class})
  public ResponseEntity<Object> bindException(BindException ex) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.BAD_REQUEST;
    return handleExceptionInternal(ex, null, headers, status,  "SYS_REQ_0", "Bad bean");
  }



  @ExceptionHandler(value = {AsyncRequestTimeoutException.class})
  public ResponseEntity<Object> asyncRequestTimeoutException(AsyncRequestTimeoutException ex
      ) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE;

    return handleExceptionInternal(ex, null, headers, status,  "SYS_REQ_0",
        "aysnc timeout bean");
  }

  @ExceptionHandler(NoSuchFieldException.class)
  public final ResponseEntity<Object> handleNoSuchFieldException(NoSuchFieldException ex
      ) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST,  "SYS_REQ_0",
        "File Not found bean");

  }

  @ExceptionHandler(DuplicateKeyException.class)
  public final ResponseEntity<Object> handleNoSuchFieldException(DuplicateKeyException ex) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, "SYS_REQ_0",
        ex.getMessage());

  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {DecodingException.class})
  public ResponseEntity<Object> jsonDecodingException(DecodingException ex) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST,  "SYS_REQ_0",
        "Bad Json");

  }

  @ExceptionHandler(ServerWebInputException.class)
  public final ResponseEntity<Object> handleServerWebInputException(
      ServerWebInputException exception) {
    HttpHeaders headers = new HttpHeaders();
    if (exception.getCause() instanceof DecodingException) {
      String message = exception.getCause().getMessage();
      Matcher match = ENUM_MSG.matcher(message);
      if (match.find()) {
        String cleanMessage = cleanMessage(match.group());
        return handleExceptionInternal(exception, null, headers, HttpStatus.BAD_REQUEST,
            ERROR_CODE, cleanMessage);
      }
    }
    return handleExceptionInternal(exception, null, headers, HttpStatus.BAD_REQUEST,
        ERROR_CODE, exception.getReason());
  }

  private String cleanMessage(String group) {
    String cleanMessage = group.replace('[', ' ');
    cleanMessage = cleanMessage.replace(']', ' ');
    cleanMessage = cleanMessage.replace(';', ' ').trim();
    return cleanMessage;
  }


  @ExceptionHandler(value = {JsonProcessingException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<Object> jsonProcessingException(JsonProcessingException ex
      ) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, "SYS_REQ_0",
        "Bad Json");
  }

  @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<Object> jsonProcessingException(MethodArgumentTypeMismatchException ex) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, "SYS_REQ_0",
        "Bad parameter");
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {DataIntegrityViolationException.class})
  public ResponseEntity<Object> validationExceptionException(DataIntegrityViolationException ex) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, "SYS_REQ_0",
        "Bad parameter");

  }

  @ExceptionHandler(value = {IllegalArgumentException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<Object> illegalArgumentException(IllegalArgumentException ex) {

    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, "SYS_REQ_0",
        "Bad parameter");
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {jakarta.validation.ValidationException.class})
  public ResponseEntity<Object> validationExceptionException(jakarta.validation.ValidationException ex
      ) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, "SYS_REQ_0",
        "non valide parameter");

  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {ValidationException.class})
  public ResponseEntity<Object> armValidationExceptionException(
      ValidationException ex, Locale local) {

    return handlingExeption(ex, HttpStatus.BAD_REQUEST, local);
  }



  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(ConstraintViolationException.class)
  protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex) {

    String errors = ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage)
        .collect(Collectors.joining());

    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, "SYS_REQ_0",
        errors);
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {IllegalStateException.class})
  public ResponseEntity<Object> illegalStatusException(IllegalStateException ex) {

    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, "SYS_REQ_0",
        ex.getMessage());


  }


  @ExceptionHandler(value = {RuntimeException.class})
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ResponseEntity<Object> notRunTimeException(RuntimeException ex) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.INTERNAL_SERVER_ERROR,
        "SYS_REQ_0", ex.getMessage());

  }


  @ExceptionHandler(
      value = {JsonException.class, MissingParamsException.class, InvalidDataException.class})
  public ResponseEntity<Object> jsonProcessingException(AbstractArmException ex, 
      Locale locale) {

    return handlingExeption(ex, HttpStatus.BAD_REQUEST, locale);
  }



  @ExceptionHandler(value = {NotFoundException.class})
  public ResponseEntity<Object> notFountId(NotFoundException ex,
      Locale locale) {
    return handlingExeption(ex, HttpStatus.NOT_FOUND, locale);

  }



  @ExceptionHandler(value = {NoMoreCapacityException.class, NotAcceptedException.class})
  public ResponseEntity<Object> notRunTimeException(AbstractArmException ex, 
      Locale locale) {
    return handlingExeption(ex, HttpStatus.NOT_ACCEPTABLE, locale);

  }


  @ExceptionHandler(value = {UnAuthorizedException.class})
  public ResponseEntity<Object> unAuthorizedException(UnAuthorizedException ex,
      Locale locale) {
    return handlingExeption(ex, HttpStatus.UNAUTHORIZED, locale);

  }



  @ExceptionHandler(value = {Exception.class})
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ResponseEntity<Object> notRunTimeException(Exception ex) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.INTERNAL_SERVER_ERROR);

  }

  /**
   * A single place to customize the response body of all exception types.
   * <p>
   * The default implementation sets the {@link WebUtils#ERROR_EXCEPTION_ATTRIBUTE} request
   * attribute and creates a {@link ResponseEntity} from the given body, headers, and status.
   * 
   * @param ex the exception
   * @param body the body for the response
   * @param headers the headers for the response
   * @param status the response status
   * @param request the current request
   */
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body,
      HttpHeaders headers, HttpStatus status, String code, String message) {

    UUID id = UUID.randomUUID();
    headers.add(X_ERROR_ID, id.toString());
    headers.add(X_ERROR_TIMESTAMP, LocalDateTime.now().toString());
    headers.add(X_ERROR_CODE, code);
    headers.add(X_ERROR_MESSAGE, message);

    log.info("id: {} , code: {} , message {}", id.toString(), code, message);
    log.error(message, ex);
    return new ResponseEntity<>(body, headers, status);
  }


  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body,
      HttpHeaders headers, HttpStatus status) {

    UUID id = UUID.randomUUID();
    headers.add(X_ERROR_ID, id.toString());
    headers.add(X_ERROR_TIMESTAMP, LocalDateTime.now().toString());
    log.error(id.toString(), ex);

    return new ResponseEntity<>(body, headers, status);
  }

  private ResponseEntity<Object> handlingExeption(AbstractArmException ex, HttpStatus status,
      Locale locale) {
    if (locale == null) {
      locale = Locale.getDefault();
    }
    HttpHeaders headers = new HttpHeaders();

    UUID id = UUID.randomUUID();
    headers.add(X_ERROR_ID, id.toString());
    headers.add(X_ERROR_TIMESTAMP, LocalDateTime.now().toString());
    headers.add(X_ERROR_CODE, ex.getCode());
    String message;
    Object[] args = ex.getArgs();
    try {
    if (args == null || args.length == 0)
      message = messageSource.getMessage(ex.getMessage(), new Object[] {id}, locale);
    else
      message = messageSource.getMessage(ex.getMessageCode(), args, locale);
  } catch (Exception e) {
    log.error("Error in retrevie message", e);
    log.error("error handle ", ex);
    message = ex.getMessage();
  }
    headers.add(X_ERROR_MESSAGE, message);
    if (ex.isCatched()) {
      log.debug("Catched Exception with id :{} , code :{} and message :{}", id, ex.getCode(),
          message);

    } else {
      log.error("Uncatched Exception with id :{} , code :{} , message :{} ,and exeption: {}", id,
          ex.getCode(), message, ex);
    }
    return new ResponseEntity<>(null, headers, status);
  }



}
