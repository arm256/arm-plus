package com.arm256.reactive.controller;

import static org.springframework.http.ResponseEntity.created;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.Set;
import jakarta.validation.constraints.NotEmpty;
import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.util.UriComponentsBuilder;
import com.arm256.model.ExportTypes;
import com.arm256.model.ModelLoadable;
import com.arm256.reactive.service.CRUD;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequestMapping(consumes = "application/json", produces = "application/json")
public abstract class AbstractCRUDController<T extends ModelLoadable<E>, E extends Serializable> {

  protected final CRUD<?, T, E> service;
  private final Class<T> modelClazz;
  protected static final String COUNT_HEADER = "X_TOTAL_COUNT";

  @Autowired
  protected AbstractCRUDController(CRUD<?, T, E> service, Class<T> type) {
    this.service = service;
    modelClazz = type;
  }

  @Autowired
  protected AbstractCRUDController(CRUD<?, T, E> service) {
    this.service = service;
    this.modelClazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
        .getActualTypeArguments()[0];
  }

  @GetMapping()
  public Flux<T> all(@RequestHeader(name = "userId", required = true) String userid,
      @RequestParam(name = "page", defaultValue = "0", required = false) int pageNo,
      @RequestParam(name = "size", defaultValue = "20", required = false) int pageSize,
      @RequestParam(value = "sortBy", defaultValue = "name") String sortBy,
      @RequestParam(value = "direction", defaultValue = "Desc") String direction) {
    isNotSupported("all");
    Pageable page =
        PageRequest.of(pageNo, pageSize, Sort.by(Sort.Direction.fromString(direction), sortBy));
    return service.retrieves(page, userid);

  }

  @GetMapping("/admin")
  public Flux<T> all(@RequestParam(name = "page", defaultValue = "0", required = false) int pageNo,
      @RequestParam(name = "size", defaultValue = "20", required = false) int pageSize

  // @RequestParam(required = false) Pageable page
  ) {
    isNotSupported("all/admin");
    Pageable page = PageRequest.of(pageNo, pageSize);
    return service.retrieves(page);

    // if (read.isEmpty()) {
    // return noContent().build();
    // }
    // HttpHeaders responseHeader = new HttpHeaders();
    //
    // responseHeader.add("X-content-total", "" + read.getTotalElements());
    // responseHeader.add("X-page-total", "" + read.getTotalPages());
    // responseHeader.add("X-content-size", "" + read.getNumberOfElements());
    //
    // return new ResponseEntity<List<T>>(read.getContent(), responseHeader, HttpStatus.OK);
  }

  @GetMapping("/{id}")
  @Operation(parameters = {@Parameter(name = "id", in = ParameterIn.PATH, required = true,
      description = "uuid of element need to select",
      schema = @Schema(type = "string", format = "uuid",
          pattern = "/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"),
      example = "dd58a026-5d7e-4c42-b2f0-83081938903f")},
      responses = @ApiResponse(responseCode = "200", description = "ok", headers = {}))
  public Mono<T> get(@PathVariable("id") E queriesId) {
    isNotSupported("get");
    return this.service.retrieve(() -> queriesId);
  }

  @PostMapping
  @Operation(responses = @ApiResponse(responseCode = "201", description = "Created", headers = {
      @Header(name = "location", required = true, description = "url of new data created")}))
  public Mono<ResponseEntity<Void>> save(@RequestHeader(required = false) String userId,
      @RequestBody T form, ServerHttpRequest request) {
    isNotSupported("create");
    return service.create(form)
        .map(dto -> created(UriComponentsBuilder.fromPath(request.getURI().getPath())
            .path(controllerPath() + "/{id}").buildAndExpand(dto.getId()).toUri()).build());
  }

  @PutMapping("/{id}")
  @Operation(parameters = {@Parameter(name = "id", in = ParameterIn.PATH, required = true,
      description = "uuid of element need to select",
      schema = @Schema(type = "string", format = "uuid",
          pattern = "/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"),
      example = "dd58a026-5d7e-4c42-b2f0-83081938903f")},
      responses = @ApiResponse(responseCode = "204", description = "updated"))
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Mono<Void> update(@PathVariable("id") E id, @RequestBody T form) {
    isNotSupported("update");
    form.setId(id);
    return service.update(form);
  }

  @DeleteMapping("/{id}")
  @Operation(responses = @ApiResponse(responseCode = "204", description = "deleted"))
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Mono<Void> delete(@PathVariable("id") E id) {
    isNotSupported("delete");
    return service.delete(() -> id);
  }

  @GetMapping("/trashed")
  public Flux<T> trashed(@RequestHeader(name = "userId", required = true) String userid,
      @RequestParam(name = "page", defaultValue = "0", required = false) int pageNo,
      @RequestParam(name = "size", defaultValue = "20", required = false) int pageSize

  // @RequestParam(required = false) Pageable page
  ) {
    isNotSupported("all/trashed");
    Pageable page = PageRequest.of(pageNo, pageSize);
    return service.retrievesTrashed(page, userid);
    //
    // if (read.isEmpty()) {
    // return noContent().build();
    // }
    // HttpHeaders responseHeader = new HttpHeaders();
    //
    // responseHeader.add("X-content-total", "" + read.getTotalElements());
    // responseHeader.add("X-page-total", "" + read.getTotalPages());
    // responseHeader.add("X-content-size", "" + read.getNumberOfElements());
    //
    // return new ResponseEntity<List<T>>(read.getContent(), responseHeader, HttpStatus.OK);
  }

  @GetMapping("/trashed/admin")
  public Flux<T> trashed(
      @RequestParam(name = "page", defaultValue = "0", required = false) int pageNo,
      @RequestParam(name = "size", defaultValue = "20", required = false) int pageSize

  // @RequestParam(required = false) Pageable page
  ) {
    isNotSupported("all/trashed/admin");
    Pageable page = PageRequest.of(pageNo, pageSize);
    return service.retrievesTrashed(page);

    // if (read.isEmpty()) {
    // return noContent().build();
    // }
    // HttpHeaders responseHeader = new HttpHeaders();
    //
    // responseHeader.add("X-content-total", "" + read.getTotalElements());
    // responseHeader.add("X-page-total", "" + read.getTotalPages());
    // responseHeader.add("X-content-size", "" + read.getNumberOfElements());
    //
    // return new ResponseEntity<List<T>>(read.getContent(), responseHeader, HttpStatus.OK);
  }

  @DeleteMapping("/trashed/{id}")
  @Operation(responses = @ApiResponse(responseCode = "204", description = "deleted"))
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Mono<Void> trashed(@PathVariable("id") E id) {
    isNotSupported("get/trashed");
    return service.trashed(() -> id);
  }

  @DeleteMapping("/trashed")
  @Operation(responses = @ApiResponse(responseCode = "204", description = "deleted"))
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Mono<Void> trashedAll(@RequestHeader(name = "userId", required = true) String userid) {
    isNotSupported("delete/all/trashed");
    return service.deleteTrashed(userid);
  }

  @PatchMapping("/reset/{id}")
  @Operation(responses = @ApiResponse(responseCode = "204", description = "deleted"))
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Mono<Void> reset(@PathVariable("id") E id) {
    isNotSupported("reset");
    return service.reset(() -> id);
  }

  @PatchMapping("/reset")
  @Operation(responses = @ApiResponse(responseCode = "204", description = "deleted"))
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Mono<Void> resetAll(@RequestHeader(name = "userId", required = true) String userid) {
    isNotSupported("all/reset");
    return service.resetAll(userid);
  }


  @GetMapping("count")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Mono<ResponseEntity<Void>> countLocations(@RequestHeader(name = "userId") @NotEmpty(
      message = "User id header should not be empty") String userId) {
    isNotSupported("count");
    return service.countAll(userId, false)
        .map(counter -> ResponseEntity.ok().header(COUNT_HEADER, String.valueOf(counter)).build());
  }

  @GetMapping("count/admin")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Mono<ResponseEntity<Void>> countLocationsByAdmin() {
    isNotSupported("count/admin");
    return service.countAllByAdmin(false)
        .map(counter -> ResponseEntity.ok().header(COUNT_HEADER, String.valueOf(counter)).build());
  }

  @GetMapping("trashed/count")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Mono<ResponseEntity<Void>> getDeletedLocationsCount(@RequestHeader(
      name = "userId") @NotEmpty(message = "User id header should not be empty") String userId) {
    isNotSupported("trashed/count");
    return service.countAll(userId, true)
        .map(counter -> ResponseEntity.ok().header(COUNT_HEADER, String.valueOf(counter)).build());
  }

  @GetMapping("trashed/count/admin")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public Mono<ResponseEntity<Void>> getDeletedLocationsCountByAdmin() {
    isNotSupported("trashed/count/admin");
    return service.countAllByAdmin(true)
        .map(counter -> ResponseEntity.ok().header(COUNT_HEADER, String.valueOf(counter)).build());
  }

  @GetMapping("export")
  public Mono<ResponseEntity<Resource>> export(
      @RequestParam(name = "type", defaultValue = "JSON") ExportTypes types,
      @RequestParam(name = "page", defaultValue = "0", required = false) int pageNo,
      @RequestParam(name = "size", defaultValue = "20", required = false) int pageSize

  ) throws IOException {

    Pageable page = PageRequest.of(pageNo, pageSize);

    HttpHeaders responseHeader = new HttpHeaders();

    responseHeader.add(HttpHeaders.CONTENT_DISPOSITION,
        "attachment;filename=\"export." + types.name() + "\"");

    return service.export(types, page).map(InputStreamResource::new).map(e -> ResponseEntity.ok()
        .headers(responseHeader).contentType(MediaType.APPLICATION_OCTET_STREAM).body(e)
    );

  }

	@PostMapping(path = "/import", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public Mono<ResponseEntity<Object>> uploadFile(
			@RequestParam(name = "type", defaultValue = "JSON") ExportTypes types, @RequestPart Mono<FilePart> file) {
		return file.map(FilePart::content).flatMap(this::mergeDataBuffers).map(ByteArrayInputStream::new).flatMap(e -> {
			try {
				return service.imports(types, e, modelClazz).then();
			} catch (IOException e1) {
				return Mono.error(new FileUploadException("please contact author"));
			}
		}).map(e -> ResponseEntity.noContent().build()).onErrorReturn(ResponseEntity.unprocessableEntity().build());

	}


  private Mono<byte[]> mergeDataBuffers(Flux<DataBuffer> dataBufferFlux) {
    return DataBufferUtils.join(dataBufferFlux).map(dataBuffer -> {
      byte[] bytes = new byte[dataBuffer.readableByteCount()];
      dataBuffer.read(bytes);
      DataBufferUtils.release(dataBuffer);
      return bytes;
    });
  }

  @SuppressWarnings("unchecked")
  public <S extends CRUD<?, T, E>> S getService() {
    return (S) service;
  }

  public abstract String controllerPath();

  private void isNotSupported(String method) {
    if (notSupportedOperations().contains(method)) {
      throw new UnsupportedOperationException(method);
    }
  }
  protected Set<String> notSupportedOperations() {
    return Collections.emptySet();
  }
}
