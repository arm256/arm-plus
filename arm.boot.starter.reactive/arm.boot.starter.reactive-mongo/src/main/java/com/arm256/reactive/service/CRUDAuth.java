package com.arm256.reactive.service;

import java.io.Serializable;
import org.springframework.transaction.annotation.Transactional;
import com.arm256.entity.Loadable;
import com.arm256.entity.Persistable;
import com.arm256.model.ModelLoadable;
import reactor.core.publisher.Mono;

public interface CRUDAuth<E extends Persistable<ID>, D extends ModelLoadable<ID>, ID extends Serializable> {

  Mono<D> retrieve(Loadable<ID> id, String createdBy);

	@Transactional
  Mono<Void> update(D d, String createdBy);

	@Transactional
  Mono<Void> trashed(Loadable<ID> id, String createdBy);
	
	@Transactional
  Mono<Void> delete(Loadable<ID> id, String createdBy);

    // InputStream export(ExportTypes type, Pageable pageable, String createdBy) throws IOException;

  Mono<E> read(Loadable<ID> id, String createdBy);

}
