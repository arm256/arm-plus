package com.arm256.reactive.config;

import org.springframework.http.server.reactive.ServerHttpRequest;
import lombok.experimental.UtilityClass;
import reactor.core.publisher.Mono;

@UtilityClass
public class ReactiveRequestContextHolder {
  public static final Class<ServerHttpRequest> CONTEXT_KEY = ServerHttpRequest.class;

  /**
   * Gets the {@code Mono<ServerHttpRequest>} from Reactor {@link Context}
   * 
   * @return the {@code Mono<ServerHttpRequest>}
   */
  public static Mono<ServerHttpRequest> getRequest() {
    return Mono.deferContextual(ctx -> Mono.just(ctx.get(CONTEXT_KEY)));
  }

}
