package com.arm256.reactive.config;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.mongodb.config.EnableReactiveMongoAuditing;
import lombok.Setter;

@Configuration
@EnableReactiveMongoAuditing
public class DataMongoConfig {


	@Autowired
	@Setter
	protected ModelMapper modelMapper;

    @Bean
    public PersistentEntityCallback persistentEntityCallback() {
      return new PersistentEntityCallback();
    }

    @Bean("exception_message")
    public MessageSource messageSource() {
      ReloadableResourceBundleMessageSource messageSource =
          new ReloadableResourceBundleMessageSource();

      messageSource.setBasename("classpath:messages");
      messageSource.setDefaultEncoding("UTF-8");
      return messageSource;
    }
    // @Autowired
    // protected EntityManagerFactory emFactory;
    //
    // @Autowired
    // @Setter
    // private MetadataValueMapper metadataValueMapper;
    //
    // @PostConstruct
    // public void init() {
    // modelMapper.addMappings(new PropertyMap<ArmObjectEntity, ArmObject>() {
    //
    // @Override
    // protected void configure() {
    //
    // using(metadataValueMapper).map(source.getMetadata()).setMetadata(null);
    // }
    // });
    // modelMapper.getConfiguration()
    // .setPropertyCondition(context ->
    // emFactory.getPersistenceUnitUtil().isLoaded(context.getSource()));
    // }

}
