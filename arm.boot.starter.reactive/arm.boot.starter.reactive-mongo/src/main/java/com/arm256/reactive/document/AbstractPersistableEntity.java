package com.arm256.reactive.document;

import java.io.Serializable;
import java.util.UUID;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import com.arm256.entity.Persistable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuppressWarnings("serial")
@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractPersistableEntity implements Persistable<UUID>, Serializable {

	@Id
	protected UUID id;

	@Version
	private long version;
	
	@CreatedBy
	private String createdBy;

	private boolean archive;

}
