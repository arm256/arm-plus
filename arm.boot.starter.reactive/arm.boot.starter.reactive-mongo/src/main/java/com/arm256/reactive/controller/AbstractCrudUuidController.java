package com.arm256.reactive.controller;

import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import com.arm256.model.ModelLoadable;
import com.arm256.reactive.service.CRUD;

public abstract class AbstractCrudUuidController<T extends ModelLoadable<UUID>>
		extends AbstractCRUDController<T, UUID> {

	@Autowired
    protected AbstractCrudUuidController(CRUD<?, T, UUID> service, Class<T> modelClazz) {
		super(service,modelClazz);
	}

    @Autowired
    protected AbstractCrudUuidController(CRUD<?, T, UUID> service) {
      super(service);
    }

	public abstract String controllerPath();
}
