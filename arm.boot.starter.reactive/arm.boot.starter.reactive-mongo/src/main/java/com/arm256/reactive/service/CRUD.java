package com.arm256.reactive.service;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.transaction.annotation.Transactional;
import com.arm256.entity.Loadable;
import com.arm256.entity.Persistable;
import com.arm256.model.ModelLoadable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CRUD<E extends Persistable<ID>, D extends ModelLoadable<ID>, ID extends Serializable>
    extends CRUDService<E, D, ID> {


  default Flux<E> createAllEntity(Flux<E> d) {
    return d.flatMap(this::create);
	}


	@Transactional
    Mono<E> create(E e);

    Mono<Long> count(Criteria... criterias);

    Flux<E> query(Criteria... criterias);

    Mono<E> querySingle(Criteria... criterias);

    Flux<E> query(Pageable pageable, Criteria... criterias);

    Mono<E> querySingle(Pageable pageable, Criteria... criterias);

    // InputStream export(ExportTypes type, Pageable pageable) throws IOException;
    //
    // List<D> imports(ExportTypes type, InputStream stream, Class<D> model) throws IOException;

    default Mono<D> toDto(E e) {
      return Mono.just(getMapper().toDto(e));
	}

	default E toEntity(D d) {
		return getMapper().toEntity(d);
	}

	default List<D> toDto(List<E> e) {
		return getMapper().toDto(e);
	}

	default List<E> toEntity(List<D> d) {
		return getMapper().toEntity(d);
	}

    Mono<E> read(Loadable<ID> id);

    Mono<E> readTrashed(Loadable<ID> id);

    Flux<E> reads(Query query);

    Flux<E> reads(Pageable pageable, String createdBy);

    Flux<E> reads(Pageable pageable);

    Flux<E> reads(Pageable pageable, List<String> createdBy);

    Flux<E> readsTrashed(Pageable pageable, String createdBy);

    Flux<E> readsTrashed(Pageable pageable);



}
