package com.arm256.reactive.service;

import java.util.Map.Entry;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestBodySpec;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;
import com.arm256.model.ThirdPartRequest;
import com.arm256.service.Caller;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@Primary
@Service
public class HttpCallerMono implements Caller<Mono<?>> {


  private final WebClient webclient;

  @Override
  public Mono<?> apply(ThirdPartRequest dto) {
    HttpHeaders headers = new HttpHeaders();
    for (Entry<String, String> entry : dto.getHeaders().entrySet()) {
      headers.add(entry.getKey(), entry.getValue());
    }

    RequestBodySpec wb = webclient.method(dto.getMethod().getmethod()).uri(dto.getUrl())
        .headers(h -> h.addAll(headers));

    RequestHeadersSpec<?> client = wb;
    if (!dto.getBody().isEmpty()) {
      client = wb.bodyValue(dto.getBody());
    }

    return client.retrieve().bodyToMono(Object.class);
  }

}
