package com.arm256.reactive.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.function.Function;
import org.springframework.data.domain.Pageable;
import com.arm256.entity.Loadable;
import com.arm256.entity.Persistable;
import com.arm256.model.ExportTypes;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.Mapper;
import com.arm256.reactive.repository.Generic;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CRUDService<E extends Persistable<ID>, D extends ModelLoadable<ID>, ID extends Serializable> {

  default Flux<D> createAll(Flux<D> d) {
    return d.flatMap(this::create);
	}

    Mono<D> create(D d);

    Mono<Long> countAll(String userId, boolean deleted);

    Mono<Long> countAllByAdmin(boolean deleted);

    Flux<D> retrieves(Pageable pageable);

    Flux<D> retrieves(Pageable pageable, String createdBy);

    Flux<D> retrievesTrashed(Pageable pageable);

    Flux<D> retrieves(Pageable pageable, List<String> createdBy);

    Flux<D> retrievesTrashed(Pageable pageable, String createdBy);
	
    Mono<D> retrieve(Loadable<ID> id);


    Mono<Void> update(D d);

    Mono<Void> delete(Loadable<ID> id);
	
    Mono<Void> trashed(Loadable<ID> id);

    <R> Mono<Void> trashed(Loadable<ID> id,
        Function<? super E, ? extends Mono<? extends R>> onNext);

    Mono<Void> reset(Loadable<ID> id);


	public <R extends Generic<E, ID>> R getRepository();

	public Mapper<E, D> getMapper();

    Mono<Void> resetAll(String createdBy);

    Mono<Void> deleteTrashed(String createdBy);

    Mono<InputStream> export(ExportTypes type, Pageable pageable) throws IOException;

    Flux<D> imports(ExportTypes type, InputStream stream, Class<D> model) throws IOException;

}
