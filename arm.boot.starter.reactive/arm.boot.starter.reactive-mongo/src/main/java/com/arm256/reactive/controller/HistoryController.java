package com.arm256.reactive.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arm256.model.HistoryField;
import com.arm256.model.HistoryRows;
import com.arm256.service.HistoryService;

@RestController
@RequestMapping("/v1/hisotry")
public class HistoryController {
	@Autowired
	private HistoryService historyService;

	@GetMapping("/fields")
	public List<HistoryField> readAll() {
		return historyService.readHistories().stream().
				peek(e -> e.setHistories(null)).collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public List<HistoryRows> read(@PathVariable(value = "id") String name) {
		return historyService.readHistory(name);
	}

}
