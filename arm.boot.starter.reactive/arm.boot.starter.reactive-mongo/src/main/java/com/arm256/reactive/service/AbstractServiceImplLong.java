package com.arm256.reactive.service;

import com.arm256.entity.Persistable;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.Mapper;
import com.arm256.reactive.repository.Generic;

public abstract class AbstractServiceImplLong<E extends Persistable<Long>, D extends ModelLoadable<Long>, R extends Generic<E, Long>>
    extends AbstractServiceImpl<E, D, R, Long> implements CRUD_LONG<E, D> {

  protected AbstractServiceImplLong(R repository, Mapper<E, D> mapper) {
    super(repository, mapper);
  }

}
