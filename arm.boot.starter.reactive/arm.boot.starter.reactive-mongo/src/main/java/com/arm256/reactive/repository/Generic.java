package com.arm256.reactive.repository;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.NoRepositoryBean;
import com.arm256.entity.Persistable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@NoRepositoryBean
public interface Generic<T extends Persistable<ID>, ID extends Serializable>
    extends ReactiveMongoRepository<T, ID> {



  @Query("{ id: { $exists: true }, archive: {$ne: true}}")
  public Flux<T> findAll(final Pageable page);

  public Mono<T> findByIdAndArchiveFalse(ID id);

  public Mono<T> findByIdAndArchiveTrue(ID id);
	
  public Flux<T> findByCreatedByAndArchiveFalse(Pageable page, String createdBy);
	
  public Flux<T> findByCreatedByAndArchiveTrue(Pageable page, String createdBy);
	
  public Flux<T> findByArchiveTrue(Pageable page);
	
  public Flux<T> findByArchiveFalseAndCreatedByIn(Pageable page, List<String> createdByList);
	
  public Flux<T> findByArchiveTrueAndCreatedByIn(Pageable page, List<String> createdByList);

  public Flux<T> findByCreatedByAndArchiveTrue(String createdBy);
	
  public Flux<T> findByCreatedByAndArchiveFalse(String createdBy);

  public Flux<T> findByCreatedBy(String addedBy);

  public Mono<T> findByIdAndArchiveFalse(UUID id);

  public Flux<T> findByIdInAndArchiveFalse(List<UUID> ids);

  public Mono<Long> countByArchiveFalseAndCreatedBy(String userId);

  public Mono<T> findByIdAndArchive(ID id, boolean deleted);

  public Flux<T> findByArchive(boolean deleted);

  public Mono<T> findByIdAndArchive(UUID id, boolean deleted);

  public Mono<Long> countByArchive(boolean deleted);

  public Mono<Long> countByArchiveAndCreatedBy(boolean deleted, String createdBy);



}
