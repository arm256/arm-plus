package com.arm256.reactive.service;

import java.util.UUID;
import com.arm256.entity.Persistable;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.Mapper;
import com.arm256.reactive.repository.Generic;
import com.arm256.reactive.service.Validator.Validators;
import reactor.core.publisher.Mono;

public abstract class AbstractServiceImplUUID<E extends Persistable<UUID>, D extends ModelLoadable<UUID>, R extends Generic<E, UUID>>
		extends AbstractServiceImpl<E, D, R, UUID> implements CRUD_UUID<E, D> {

  protected AbstractServiceImplUUID(R repository, Mapper<E, D> mapper) {
		super(repository, mapper);
	}

    @Override
    public Mono<D> create(D d) {
      if (d.getId() == null) {
        d.setId(generateUUID());
      }
      return super.create(d);
    }

    @Override
    public Mono<D> create(D d, @SuppressWarnings("unchecked") Validators<D>... v) {
      if (d.getId() == null) {
        d.setId(generateUUID());
      }
      return super.create(d, v);
    }

    protected UUID generateUUID() {
      return UUID.randomUUID();
    }

}
