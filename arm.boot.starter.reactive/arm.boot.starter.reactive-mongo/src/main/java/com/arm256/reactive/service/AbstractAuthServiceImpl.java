package com.arm256.reactive.service;

import java.io.Serializable;
import com.arm256.entity.Loadable;
import com.arm256.entity.Persistable;
import com.arm256.exception.UnAuthorizedException;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.Mapper;
import com.arm256.reactive.repository.Generic;
import reactor.core.publisher.Mono;

public abstract class AbstractAuthServiceImpl<E extends Persistable<ID>, D extends ModelLoadable<ID>, R extends Generic<E, ID>, ID extends Serializable>
    extends AbstractServiceImpl<E, D, R, ID> implements CRUDAuth<E, D, ID> {


  protected final Mono<? extends E> unAuthorized = Mono.error(new UnAuthorizedException());

  protected AbstractAuthServiceImpl(R repository, Mapper<E, D> mapper) {
    super(repository, mapper);
  }

  @Override
  public Mono<D> retrieve(Loadable<ID> id, String createdBy) {
    return read(id, createdBy).map(mapper::toDto);
  }

  @Override
  public Mono<Void> update(D d, String createdBy) {

    return read(d).filter(e -> e.getCreatedBy().equals(createdBy)).switchIfEmpty(unAuthorized)
        .flatMap(e -> repository.save(mapper.entityUpdate(e, d))).then();

  }

  @Override
  public Mono<Void> delete(Loadable<ID> id, String createdBy) {
    return read(id, createdBy).flatMap(repository::delete).then();
  }

  @Override
  public Mono<Void> trashed(Loadable<ID> id, String createdBy) {

    return read(id, createdBy).flatMap(e -> {
      e.setArchive(true);
      return repository.save(e);
    }).then();
  }

  @Override
  public Mono<E> read(Loadable<ID> id, String createdBy) {
    return read(id).filter(e -> e.getCreatedBy().equals(createdBy)).switchIfEmpty(unAuthorized);
  }

  // @Override
  // public InputStream export(ExportTypes type, Pageable pageable, String createdBy) throws
  // IOException {
  // Page<D> read = retrieves(pageable,createdBy);
  // if (read.isEmpty()) {
  // throw new NotContentException();
  // }
  // return new ByteArrayInputStream(objectMapper.writeValueAsBytes(read.getContent()));
  // }
}
