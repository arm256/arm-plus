package com.arm256.reactive.config;

import java.time.LocalDateTime;
import org.reactivestreams.Publisher;
import org.springframework.data.mongodb.core.mapping.event.ReactiveBeforeConvertCallback;
import org.springframework.http.server.reactive.ServerHttpRequest;
import com.arm256.reactive.document.AbstractAuditableEntity;
import reactor.core.publisher.Mono;

public class PersistentEntityCallback
    implements ReactiveBeforeConvertCallback<AbstractAuditableEntity> {

  @Override
  public Publisher<AbstractAuditableEntity> onBeforeConvert(AbstractAuditableEntity entity,
      String collection) {
    Mono<ServerHttpRequest> user = ReactiveRequestContextHolder.getRequest();

    LocalDateTime currentTime = LocalDateTime.now();

    if (entity.getId() == null) {
      entity.setCreatedDate(currentTime);
    }
    entity.setLastModifiedDate(currentTime);

    return user.flatMap(r -> {
      String u = r.getHeaders().getFirst("userId");
      if (entity.getCreatedBy() == null) {
        entity.setCreatedBy(u);
      }
      entity.setLastModifiedBy(u);

      return Mono.just(entity);
    }).defaultIfEmpty(entity);
  }
}