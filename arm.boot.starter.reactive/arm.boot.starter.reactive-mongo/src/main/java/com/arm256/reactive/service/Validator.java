package com.arm256.reactive.service;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import org.reactivestreams.Publisher;
import com.arm256.exception.ValidationException;
import com.arm256.reactive.utils.ReactivePredicate;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Validator<T> {
  /**
   * Object that is validated.
   */
  private final Publisher<T> obj;

  /**
   * List of exception thrown during validation.
   */
  private final ValidationException exceptions = new ValidationException();

  /**
   * Creates a monadic value of given object.
   *
   * @param obj object to be validated
   */
  private Validator(Publisher<T> obj) {
    this.obj = obj;
  }

  /**
   * Creates validator against given object.
   *
   * @param t object to be validated
   * @param <T> object's type
   * @return new instance of a validator
   */
  public static <T> Validator<T> of(Publisher<T> t) {
    return new Validator<>(t);
  }

  /**
   * Checks if the validation is successful.
   *
   * @param validation one argument boolean-valued function that represents one step of validation.
   *        Adds exception to main validation exception list when single step validation ends with
   *        failure.
   * @param message error message when object is invalid
   * @return this
   */
  private <X> Mono<Boolean> validate(ReactivePredicate<X> validation, String message, X obj) {

    return validation.test(obj).doOnNext(e -> {
      if (!Boolean.TRUE.equals(e)) {
        exceptions.addValidation(message);
      }
    }).onErrorResume(e -> {
      exceptions.addValidation(e.getMessage());
      return Mono.just(false);
    });
  }

  /**
   * Extension for the {@link Validator#validate(Predicate, String)} method, dedicated for objects,
   * that need to be projected before requested validation.
   *
   * @param projection function that gets an objects, and returns projection representing element to
   *        be validated.
   * @param validation see {@link Validator#validate(Predicate, String)}
   * @param message see {@link Validator#validate(Predicate, String)}
   * @param <U> see {@link Validator#validate(Predicate, String)}
   * @return this
   */
  private <U> Mono<Boolean> validate(Function<T, U> projection, ReactivePredicate<U> validation,
      String message, T obj) {
    return validate(projection.andThen(validation::test)::apply, message, obj);
  }



  private <U> Function<? super T, ? extends Publisher<Boolean>> validate(
      List<Validators<T>> validators) {
    return e -> Flux.fromIterable(validators).flatMap(v -> {
      if (v instanceof ValidatorsProjection) {
        ValidatorsProjection<T, U> xo = ((ValidatorsProjection<T, U>) v);
        return validate(xo.projection, xo.getConvertValidation(), v.message, e);
      } else {
        return validate(v.getValidation(), v.message, e);
      }
    }).all(result -> result);
  }


  /**
   * Receives validated object or throws exception when invalid.
   *
   * @return object that was validated
   * @throws IllegalStateException when any validation step results with failure
   */
  public Flux<T> getFlux(List<Validators<T>> validators) throws IllegalStateException {

    Flux<T> mono = (Flux<T>) obj;

    Flux<T> m = mono.filterWhen(validate(validators));

    return m.switchIfEmpty(Mono.error(() -> exceptions));

  }

  /**
   * Receives validated object or throws exception when invalid.
   *
   * @return object that was validated
   * @throws IllegalStateException when any validation step results with failure
   */
  public Mono<T> getMono(List<Validators<T>> validators) throws IllegalStateException {

    Mono<T> mono = (Mono<T>) obj;

    Mono<T> m = mono.filterWhen(validate(validators));

    return m.switchIfEmpty(Mono.error(() -> exceptions));

  }

  @AllArgsConstructor
  public static class Validators<T> {
    final ReactivePredicate<T> validation;
    String message;
    boolean negation = false;

    protected Validators(ReactivePredicate<T> validation, String message) {
      this.validation = validation;
      this.message = message;
    }


    ReactivePredicate<T> getValidation() {
      if (negation) {
        return validation.negate();
      }
      return validation;
    }

    public static <X> Validators<X> of(ReactivePredicate<X> validation, String message) {
      return new Validators<>(validation, message);
    }

    public static <X, R> Validators<R> of(Function<R, X> projection,
        ReactivePredicate<X> validation, String message) {
      return new ValidatorsProjection<>(projection, validation, message);
    }

    public Validators<T> reverse() {
      this.negation = !negation;
      return this;
    }
  }


  public static class ValidatorsProjection<T, U> extends Validators<T> {
    final ReactivePredicate<U> convertValidation;

    public ReactivePredicate<U> getConvertValidation() {
      if (negation) {
        return convertValidation.negate();
      }
      return convertValidation;
    }

    protected ValidatorsProjection(Function<T, U> projection, ReactivePredicate<U> validation,
        String message) {
      super(null, message);
      this.projection = projection;
      this.convertValidation = validation;
    }

    final Function<T, U> projection;
  }

}
