package com.arm256.reactive.service;

import com.arm256.entity.Persistable;
import com.arm256.model.ModelLoadable;

public interface CRUD_LONG<E extends Persistable<Long>, D extends ModelLoadable<Long>> extends CRUD<E, D, Long> {

}
