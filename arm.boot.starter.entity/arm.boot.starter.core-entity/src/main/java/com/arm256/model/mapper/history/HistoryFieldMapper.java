package com.arm256.model.mapper.history;

import jakarta.annotation.PostConstruct;

import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.arm256.entity.history.HistoryFieldEntity;
import com.arm256.model.HistoryField;
import com.arm256.model.mapper.AbstractMapper;

@Component
@Qualifier("history-field-mapper")
public class HistoryFieldMapper extends AbstractMapper<HistoryFieldEntity, HistoryField> {

	@Autowired
	private HistoryRowsMapper historyRow;

	public HistoryFieldMapper() {
		super(HistoryFieldEntity.class, HistoryField.class);
	}

	@PostConstruct
	@Override
	public void init() {
		super.init();
		modelMapper.addMappings(new PropertyMap<HistoryFieldEntity, HistoryField>() {
			@Override
			protected void configure() {
				using(historyRow).map(source.getHistoryRows()).setHistories(null);
			}
		});
	}

	@Override
	public HistoryFieldEntity entityUpdate(HistoryFieldEntity entity, HistoryField dto) {
		return entity;
	}


}
