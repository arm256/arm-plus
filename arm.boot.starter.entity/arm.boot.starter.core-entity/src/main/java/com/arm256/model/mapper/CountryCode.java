package com.arm256.model.mapper;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CountryCode {

    AD("Andorra", "AND", 16),

    AE("United Arab Emirates", "ARE", 784),

    AF("Afghanistan", "AFG", 4),

    AG("Antigua and Barbuda", "ATG", 28),

    AI("Anguilla", "AIA", 660),

    AL("Albania", "ALB", 8),

    AM("Armenia", "ARM", 51),

    AN("Netherlands Antilles", "ANT", 530),

    AO("Angola", "AGO", 24),

    AQ("Antarctica", "ATA", 10),

    AR("Argentina", "ARG", 32),

    AS("American Samoa", "ASM", 16),

    AT("Austria", "AUT", 40),

    AU("Australia", "AUS", 36),

    AW("Aruba", "ABW", 533),

    AX("\u212Bland Islands", "ALA", 248),

    AZ("Azerbaijan", "AZE", 31),

    BA("Bosnia and Herzegovina", "BIH", 70),

    BB("Barbados", "BRB", 52),

    BD("Bangladesh", "BGD", 50),

    BE("Belgium", "BEL", 56),

    BF("Burkina Faso", "BFA", 854),

    BG("Bulgaria", "BGR", 100),

    BH("Bahrain", "BHR", 48),

    BI("Burundi", "BDI", 108),

    BJ("Benin", "BEN", 204),

    BL("Saint Barth\u00E9lemy", "BLM", 652),

    BM("Bermuda", "BMU", 60),

    BN("Brunei Darussalam", "BRN", 96),

    BO("Plurinational State of Bolivia", "BOL", 68),

    BQ("Bonaire, Sint Eustatius and Saba", "BES", 535),

    BR("Brazil", "BRA", 76),

    BS("Bahamas", "BHS", 44),

    BT("Bhutan", "BTN", 64),

    BV("Bouvet Island", "BVT", 74),

    BW("Botswana", "BWA", 72),

    BY("Belarus", "BLR", 112),

    BZ("Belize", "BLZ", 84),

    CA("Canada", "CAN", 124),

    CC("Cocos Islands", "CCK", 166),

    CD("The Democratic Republic of the Congo", "COD", 180),

    CF("Central African Republic", "CAF", 140),

    CG("Congo", "COG", 178),

    CH("Switzerland", "CHE", 756),

    CI("C\u00F4te d'Ivoire", "CIV", 384),

    CK("Cook Islands", "COK", 184),

    CL("Chile", "CHL", 152),

    CM("Cameroon", "CMR", 120),

    CN("China", "CHN", 156),

    CO("Colombia", "COL", 170),

    CR("Costa Rica", "CRI", 188),

    CU("Cuba", "CUB", 192),

    CV("Cape Verde", "CPV", 132),

    CW("Cura/u00E7ao", "CUW", 531),

    CX("Christmas Island", "CXR", 162),

    CY("Cyprus", "CYP", 196),

    CZ("Czech Republic", "CZE", 203),

    DE("Germany", "DEU", 276),

    DJ("Djibouti", "DJI", 262),

    DK("Denmark", "DNK", 208),

    DM("Dominica", "DMA", 212),

    DO("Dominican Republic", "DOM", 214),

    DZ("Algeria", "DZA", 12),

    EC("Ecuador", "ECU", 218),

    EE("Estonia", "EST", 233),

    EG("Egypt", "EGY", 818),

    EH("Western Sahara", "ESH", 732),

    ER("Eritrea", "ERI", 232),

    ES("Spain", "ESP", 724),

    ET("Ethiopia", "ETH", 231),

    FI("Finland", "FIN", 246),

    FJ("Fiji", "FJI", 242),

    FK("Falkland Islands", "FLK", 238),

    /**
     * <a href="http://en.wikipedia.org/wiki/Federated_States_of_Micronesia">Federated States of Micronesia</a>
     */
    FM("Federated States of Micronesia", "FSM", 583),

    /**
     * <a href="http://en.wikipedia.org/wiki/Faroe_Islands">Faroe Islands</a>
     */
    FO("Faroe Islands", "FRO", 234),

    /**
     * <a href="http://en.wikipedia.org/wiki/France">France</a>
     */
    FR("France", "FRA", 250),

    /**
     * <a href="http://en.wikipedia.org/wiki/Gabon">Gabon </a>
     */
    GA("Gabon", "GAB", 266),

    /**
     * <a href="http://en.wikipedia.org/wiki/United_Kingdom">United Kingdom</a>
     */
    GB("United Kingdom", "GBR", 826),

    /**
     * <a href="http://en.wikipedia.org/wiki/Grenada">Grenada</a>
     */
    GD("Grenada", "GRD", 308),

    /**
     * <a href="http://en.wikipedia.org/wiki/Georgia_(country)">Georgia</a>
     */
    GE("Georgia", "GEO", 268),

    /**
     * <a href="http://en.wikipedia.org/wiki/French_Guiana">French Guiana</a>
     */
    GF("French Guiana", "GUF", 254),

    /**
     * <a href="http://en.wikipedia.org/wiki/Guernsey">Guemsey</a>
     */
    GG("Guemsey", "GGY", 831),

    /**
     * <a href="http://en.wikipedia.org/wiki/Ghana">Ghana</a>
     */
    GH("Ghana", "GHA", 288),

    /**
     * <a href="http://en.wikipedia.org/wiki/Gibraltar">Gibraltar</a>
     */
    GI("Gibraltar", "GIB", 292),

    /**
     * <a href="http://en.wikipedia.org/wiki/Greenland">Greenland</a>
     */
    GL("Greenland", "GRL", 304),

    /**
     * <a href="http://en.wikipedia.org/wiki/The_Gambia">Gambia</a>
     */
    GM("Gambia", "GMB", 270),

    /**
     * <a href="http://en.wikipedia.org/wiki/Guinea">Guinea</a>
     */
    GN("Guinea", "GIN", 324),

    /**
     * <a href="http://en.wikipedia.org/wiki/Guadeloupe">Guadeloupe</a>
     */
    GP("Guadeloupe", "GLP", 312),

    /**
     * <a href="http://en.wikipedia.org/wiki/Equatorial_Guinea">Equatorial Guinea</a>
     */
    GQ("Equatorial Guinea", "GNQ", 226),

    /**
     * <a href="http://en.wikipedia.org/wiki/Greece">Greece</a>
     */
    GR("Greece", "GRC", 300),

    /**
     * <a href="http://en.wikipedia.org/wiki/South_Georgia_and_the_South_Sandwich_Islands">South Georgia and the South Sandwich Islands</a>
     */
    GS("South Georgia and the South Sandwich Islands", "SGS", 239),

    /**
     * <a href="http://en.wikipedia.org/wiki/Guatemala">Guatemala</a>
     */
    GT("Guatemala", "GTM", 320),

    /**
     * <a href="http://en.wikipedia.org/wiki/Guam">Guam</a>
     */
    GU("Guam", "GUM", 316),

    /**
     * <a href="http://en.wikipedia.org/wiki/Guinea-Bissau">Guinea-Bissau</a>
     */
    GW("Guinea-Bissau", "GNB", 624),

    /**
     * <a href="http://en.wikipedia.org/wiki/Guyana">Guyana</a>
     */
    GY("Guyana", "GUY", 328),

    /**
     * <a href="http://en.wikipedia.org/wiki/Hong_Kong">Hong Kong</a>
     */
    HK("Hong Kong", "HKG", 344),

    /**
     * <a href="http://en.wikipedia.org/wiki/Heard_Island_and_McDonald_Islands">Heard Island and McDonald Islands</a>
     */
    HM("Heard Island and McDonald Islands", "HMD", 334),

    /**
     * <a href="http://en.wikipedia.org/wiki/Honduras">Honduras</a>
     */
    HN("Honduras", "HND", 340),

    /**
     * <a href="http://en.wikipedia.org/wiki/Croatia">Croatia</a>
     */
    HR("Croatia", "HRV", 191),

    /**
     * <a href="http://en.wikipedia.org/wiki/Haiti">Haiti</a>
     */
    HT("Haiti", "HTI", 332),

    /**
     * <a href="http://en.wikipedia.org/wiki/Hungary">Hungary</a>
     */
    HU("Hungary", "HUN", 348),

    /**
     * <a href="http://en.wikipedia.org/wiki/Indonesia">Indonesia</a>
     */
    ID("Indonesia", "IDN", 360),

    /**
     * <a href="http://en.wikipedia.org/wiki/Republic_of_Ireland">Ireland</a>
     */
    IE("Ireland", "IRL", 372),

    /**
     * <a href="http://en.wikipedia.org/wiki/Israel">Israel</a>
     */
    IL("Israel", "ISR", 376),

    /**
     * <a href="http://en.wikipedia.org/wiki/Isle_of_Man">Isle of Man</a>
     */
    IM("Isle of Man", "IMN", 833),

    /**
     * <a href="http://en.wikipedia.org/wiki/India">India</a>
     */
    IN("India", "IND", 356),

    /**
     * <a href="http://en.wikipedia.org/wiki/British_Indian_Ocean_Territory">British Indian Ocean Territory</a>
     */
    IO("British Indian Ocean Territory", "IOT", 86),

    /**
     * <a href="http://en.wikipedia.org/wiki/Iraq">Iraq</a>
     */
    IQ("Iraq", "IRQ", 368),

    /**
     * <a href="http://en.wikipedia.org/wiki/Iran">Islamic Republic of Iran</a>
     */
    IR("Islamic Republic of Iran", "IRN", 364),

    /**
     * <a href="http://en.wikipedia.org/wiki/Iceland">Iceland</a>
     */
    IS("Iceland", "ISL", 352),

    /**
     * <a href="http://en.wikipedia.org/wiki/Italy">Italy</a>
     */
    IT("Italy", "ITA", 380),

    /**
     * <a href="http://en.wikipedia.org/wiki/Jersey">Jersey</a>
     */
    JE("Jersey", "JEY", 832),

    /**
     * <a href="http://en.wikipedia.org/wiki/Jamaica">Jamaica</a>
     */
    JM("Jamaica", "JAM", 388),

    /**
     * <a href="http://en.wikipedia.org/wiki/Jordan">Jordan</a>
     */
    JO("Jordan", "JOR", 400),

    /**
     * <a href="http://en.wikipedia.org/wiki/Japan">Japan</a>
     */
    JP("Japan", "JPN", 392),

    /**
     * <a href="http://en.wikipedia.org/wiki/Kenya">Kenya</a>
     */
    KE("Kenya", "KEN", 404),

    /**
     * <a href="http://en.wikipedia.org/wiki/Kyrgyzstan">Kyrgyzstan</a>
     */
    KG("Kyrgyzstan", "KGZ", 417),

    /**
     * <a href="http://en.wikipedia.org/wiki/Cambodia">Cambodia</a>
     */
    KH("Cambodia", "KHM", 116),

    /**
     * <a href="http://en.wikipedia.org/wiki/Kiribati">Kiribati</a>
     */
    KI("Kiribati", "KIR", 296),

    /**
     * <a href="http://en.wikipedia.org/wiki/Comoros">Comoros</a>
     */
    KM("Comoros", "COM", 174),

    /**
     * <a href="http://en.wikipedia.org/wiki/Saint_Kitts_and_Nevis">Saint Kitts and Nevis</a>
     */
    KN("Saint Kitts and Nevis", "KNA", 659),

    /**
     * <a href="http://en.wikipedia.org/wiki/North_Korea">Democratic People's Republic of Korea</a>
     */
    KP("Democratic People's Republic of Korea", "PRK", 408),

    /**
     * <a href="http://en.wikipedia.org/wiki/South_Korea">Republic of Korea</a>
     */
    KR("Republic of Korea", "KOR", 410),

    /**
     * <a href="http://en.wikipedia.org/wiki/Kuwait">Kuwait</a>
     */
    KW("Kuwait", "KWT", 414),

    /**
     * <a href="http://en.wikipedia.org/wiki/Cayman_Islands">Cayman Islands</a>
     */
    KY("Cayman Islands", "CYM", 136),

    /**
     * <a href="http://en.wikipedia.org/wiki/Kazakhstan">Kazakhstan</a>
     */
    KZ("Kazakhstan", "KAZ", 398),

    /**
     * <a href="http://en.wikipedia.org/wiki/Laos">Lao People's Democratic Republic</a>
     */
    LA("Lao People's Democratic Republic", "LAO", 418),

    /**
     * <a href="http://en.wikipedia.org/wiki/Lebanon">Lebanon</a>
     */
    LB("Lebanon", "LBN", 422),

    /**
     * <a href="http://en.wikipedia.org/wiki/Saint_Lucia">Saint Lucia</a>
     */
    LC("Saint Lucia", "LCA", 662),

    /**
     * <a href="http://en.wikipedia.org/wiki/Liechtenstein">Liechtenstein</a>
     */
    LI("Liechtenstein", "LIE", 438),

    /**
     * <a href="http://en.wikipedia.org/wiki/Sri_Lanka">Sri Lanka</a>
     */
    LK("Sri Lanka", "LKA", 144),

    /**
     * <a href="http://en.wikipedia.org/wiki/Liberia">Liberia</a>
     */
    LR("Liberia", "LBR", 430),

    /**
     * <a href="http://en.wikipedia.org/wiki/Lesotho">Lesotho</a>
     */
    LS("Lesotho", "LSO", 426),

    /**
     * <a href="http://en.wikipedia.org/wiki/Lithuania">Lithuania</a>
     */
    LT("Lithuania", "LTU", 440),

    /**
     * <a href="http://en.wikipedia.org/wiki/Luxembourg">Luxembourg</a>
     */
    LU("Luxembourg", "LUX", 442),

    /**
     * <a href="http://en.wikipedia.org/wiki/Latvia">Latvia</a>
     */
    LV("Latvia", "LVA", 428),

    /**
     * <a href="http://en.wikipedia.org/wiki/Libya">Libya</a>
     */
    LY("Libya", "LBY", 434),

    /**
     * <a href="http://en.wikipedia.org/wiki/Morocco">Morocco</a>
     */
    MA("Morocco", "MAR", 504),

    /**
     * <a href="http://en.wikipedia.org/wiki/Monaco">Monaco</a>
     */
    MC("Monaco", "MCO", 492),

    /**
     * <a href="http://en.wikipedia.org/wiki/Moldova">Republic of Moldova</a>
     */
    MD("Republic of Moldova", "MDA", 498),

    /**
     * <a href="http://en.wikipedia.org/wiki/Montenegro">Montenegro</a>
     */
    ME("Montenegro", "MNE", 499),

    /**
     * <a href="http://en.wikipedia.org/wiki/Collectivity_of_Saint_Martin">Saint Martin (French part)</a>
     */
    MF("Saint Martin", "MAF", 663),

    /**
     * <a href="http://en.wikipedia.org/wiki/Madagascar">Madagascar</a>
     */
    MG("Madagascar", "MDG", 450),

    /**
     * <a href="http://en.wikipedia.org/wiki/Marshall_Islands">Marshall Islands</a>
     */
    MH("Marshall Islands", "MHL", 584),

    /**
     * <a href="http://en.wikipedia.org/wiki/Republic_of_Macedonia">The former Yugoslav Republic of Macedonia</a>
     */
    MK("The former Yugoslav Republic of Macedonia", "MKD", 807),

    /**
     * <a href="http://en.wikipedia.org/wiki/Mali">Mali</a>
     */
    ML("Mali", "MLI", 466),

    /**
     * <a href="http://en.wikipedia.org/wiki/Myanmar">Myanmar</a>
     */
    MM("Myanmar", "MMR", 104),

    /**
     * <a href="http://en.wikipedia.org/wiki/Mongolia">Mongolia</a>
     */
    MN("Mongolia", "MNG", 496),

    /**
     * <a href="http://en.wikipedia.org/wiki/Macau">Macao</a>
     */
    MO("Macao", "MAC", 446),

    /**
     * <a href="http://en.wikipedia.org/wiki/Northern_Mariana_Islands">Northern Mariana Islands</a>
     */
    MP("Northern Mariana Islands", "MNP", 580),

    /**
     * <a href="http://en.wikipedia.org/wiki/Martinique">Martinique</a>
     */
    MQ("Martinique", "MTQ", 474),

    /**
     * <a href="http://en.wikipedia.org/wiki/Mauritania">Mauritania</a>
     */
    MR("Mauritania", "MRT", 478),

    /**
     * <a href="http://en.wikipedia.org/wiki/Montserrat">Montserrat</a>
     */
    MS("Montserrat", "MSR", 500),

    /**
     * <a href="http://en.wikipedia.org/wiki/Malta">Malta</a>
     */
    MT("Malta", "MLT", 470),

    /**
     * <a href="http://en.wikipedia.org/wiki/Mauritius">Mauritius</a>
     */
    MU("Mauritius", "MUS", 480),

    /**
     * <a href="http://en.wikipedia.org/wiki/Maldives">Maldives</a>
     */
    MV("Maldives", "MDV", 462),

    /**
     * <a href="http://en.wikipedia.org/wiki/Malawi">Malawi</a>
     */
    MW("Malawi", "MWI", 454),

    /**
     * <a href="http://en.wikipedia.org/wiki/Mexico">Mexico</a>
     */
    MX("Mexico", "MEX", 484),

    /**
     * <a href="http://en.wikipedia.org/wiki/Malaysia">Malaysia</a>
     */
    MY("Malaysia", "MYS", 458),

    /**
     * <a href="http://en.wikipedia.org/wiki/Mozambique">Mozambique</a>
     */
    MZ("Mozambique", "MOZ", 508),

    /**
     * <a href="http://en.wikipedia.org/wiki/Namibia">Namibia</a>
     */
    NA("Namibia", "NAM", 516),

    /**
     * <a href="http://en.wikipedia.org/wiki/New_Caledonia">New Caledonia</a>
     */
    NC("New Caledonia", "NCL", 540),

    /**
     * <a href="http://en.wikipedia.org/wiki/Niger">Niger</a>
     */
    NE("Niger", "NER", 562),

    /**
     * <a href="http://en.wikipedia.org/wiki/Norfolk_Island">Norfolk Island</a>
     */
    NF("Norfolk Island", "NFK", 574),

    /**
     * <a href="http://en.wikipedia.org/wiki/Nigeria">Nigeria</a>
     */
    NG("Nigeria", "NGA", 566),

    /**
     * <a href="http://en.wikipedia.org/wiki/Nicaragua">Nicaragua</a>
     */
    NI("Nicaragua", "NIC", 558),

    /**
     * <a href="http://en.wikipedia.org/wiki/Netherlands">Netherlands</a>
     */
    NL("Netherlands", "NLD", 528),

    /**
     * <a href="http://en.wikipedia.org/wiki/Norway">Norway</a>
     */
    NO("Norway", "NOR", 578),

    /**
     * <a href="http://en.wikipedia.org/wiki/Nepal">Nepal</a>
     */
    NP("Nepal", "NPL", 524),

    /**
     * <a href="http://en.wikipedia.org/wiki/Nauru">Nauru</a>
     */
    NR("Nauru", "NRU", 520),

    /**
     * <a href="http://en.wikipedia.org/wiki/Niue">Niue</a>
     */
    NU("Niue", "NIU", 570),

    /**
     * <a href="http://en.wikipedia.org/wiki/New_Zealand">New Zealand</a>
     */
    NZ("New Zealand", "NZL", 554),

    /**
     * <a href=http://en.wikipedia.org/wiki/Oman"">Oman</a>
     */
    OM("Oman", "OMN", 512),

    /**
     * <a href="http://en.wikipedia.org/wiki/Panama">Panama</a>
     */
    PA("Panama", "PAN", 591),

    PE("Peru", "PER", 604),

    PF("French Polynesia", "PYF", 258),

    PG("Papua New Guinea", "PNG", 598),

    PH("Philippines", "PHL", 608),

    PK("Pakistan", "PAK", 586),

    PL("Poland", "POL", 616),

    PM("Saint Pierre and Miquelon", "SPM", 666),

    PN("Pitcairn", "PCN", 612),

    PR("Puerto Rico", "PRI", 630),

    PS("Occupied Palestinian Territory", "PSE", 275),

    PT("Portugal", "PRT", 620),

    PW("Palau", "PLW", 585),

    PY("Paraguay", "PRY", 600),

    QA("Qatar", "QAT", 634),

    RE("R\u00E9union", "REU", 638),

    RO("Romania", "ROU", 642),

    RS("Serbia", "SRB", 688),

    RU("Russian Federation", "RUS", 643),

    RW("Rwanda", "RWA", 646),

    SA("Saudi Arabia", "SAU", 682),

    SB("Solomon Islands", "SLB", 90),

    SC("Seychelles", "SYC", 690),

    SD("Sudan", "SDN", 729),

    SE("Sweden", "SWE", 752),

    SG("Singapore", "SGP", 702),

    SH("Saint Helena, Ascension and Tristan da Cunha", "SHN", 654),

    SI("Slovenia", "SVN", 705),

    SJ("Svalbard and Jan Mayen", "SJM", 744),

    SK("Slovakia", "SVK", 703),

    SL("Sierra Leone", "SLE", 694),

    SM("San Marino", "SMR", 674),

    SN("Senegal", "SEN", 686),

    SO("Somalia", "SOM", 706),

    SR("Suriname", "SUR", 740),

    SS("South Sudan", "SSD", 728),

    ST("Sao Tome and Principe", "STP", 678),

    SV("El Salvador", "SLV", 222),

    SX("Sint Maarten", "SXM", 534),

    SY("Syrian Arab Republic", "SYR", 760),

    SZ("Swaziland", "SWZ", 748),

    TC("Turks and Caicos Islands", "TCA", 796),

    TD("Chad", "TCD", 148),

    TF("French Southern Territories", "ATF", 260),

    TG("Togo", "TGO", 768),

    TH("Thailand", "THA", 764),

    TJ("Tajikistan", "TJK", 762),

    TK("Tokelau", "TKL", 772),

    TL("Timor-Leste", "TLS", 626),

    TM("Turkmenistan", "TKM", 795),

    TN("Tunisia", "TUN", 788),

    TO("Tonga", "TON", 776),

    TR("Turkey", "TUR", 792),

    TT("Trinidad and Tobago", "TTO", 780),

    TV("Tuvalu", "TUV", 798),

    TW("Taiwan, Province of China", "TWN", 158),

    TZ("United Republic of Tanzania", "TZA", 834),

    UA("Ukraine", "UKR", 804),

    UG("Uganda", "UGA", 800),

    UM("United States Minor Outlying Islands", "UMI", 581),

    US("United States", "USA", 840),

    UY("Uruguay", "URY", 858),

    UZ("Uzbekistan", "UZB", 860),

    VA("Holy See", "VAT", 336),

    VC("Saint Vincent and the Grenadines", "VCT", 670),

    VE("Bolivarian Republic of Venezuela", "VEN", 862),

    VG("British Virgin Islands", "VGB", 92),

    VI("Virgin Islands, U.S.", "VIR", 850),

    VN("Viet Nam", "VNM", 704),

    VU("Vanuatu", "VUT", 548),

    WF("Wallis and Futuna", "WLF", 876),

    WS("Samoa", "WSM", 882),

    YE("Yemen", "YEM", 887),

    YT("Mayotte", "MYT", 175),

    ZA("South Africa", "ZAF", 710),

    ZM("Zambia", "ZMB", 894),

    ZW("Zimbabwe", "ZWE", 716);

    private final String name;
    private final String alpha3;
    private final int numeric;



}
