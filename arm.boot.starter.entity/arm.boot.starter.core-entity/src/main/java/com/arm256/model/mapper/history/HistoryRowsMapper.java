package com.arm256.model.mapper.history;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.arm256.entity.history.HistoryRowsEntity;
import com.arm256.model.HistoryRows;
import com.arm256.model.mapper.AbstractMapperList;

@Component
@Qualifier("history-rows-mapper")
public class HistoryRowsMapper extends AbstractMapperList<HistoryRowsEntity, HistoryRows> {

	public HistoryRowsMapper() {
		super(HistoryRowsEntity.class, HistoryRows.class);
	}

	@Override
	public HistoryRowsEntity entityUpdate(HistoryRowsEntity entity, HistoryRows dto) {
		return entity;
	}

}
