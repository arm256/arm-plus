package com.arm256.model.mapper;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ExportTypes {
	JSON("export.json"),CVS("export.cvs");
	
	
	private final String fileName;
	
	public String getFileName() {
		return fileName;
	}
	
}
