package com.arm256.exception;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.ValidationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.codec.DecodingException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.server.ServerWebInputException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.util.WebUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class RestExceptionHandler {

  private static final String X_ERROR_CODE = "X-error-code";
  private static final String X_ERROR_ID = "X-error-id";
  private static final String X_ERROR_TIMESTAMP = "X-error-timestamp";
  private static final String X_ERROR_MESSAGE = "X-error-message";
  private static final String ERROR_CODE = "ent-nv10";
  private static final Pattern ENUM_MSG = Pattern.compile("String.*\\[(.*)\\];", Pattern.MULTILINE);


  private final MessageSource messageSource;

  @Autowired
  public RestExceptionHandler(@Qualifier("exception_message") MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  /**
   * Log category to use when no mapped handler is found for a request.
   * 
   * @see #pageNotFoundLogger
   */
  public static final String PAGE_NOT_FOUND_LOG_CATEGORY =
      "org.springframework.web.servlet.PageNotFound";

  /**
   * Specific logger to use when no mapped handler is found for a request.
   * 
   * @see #PAGE_NOT_FOUND_LOG_CATEGORY
   */
  protected static final Log pageNotFoundLogger = LogFactory.getLog(PAGE_NOT_FOUND_LOG_CATEGORY);



  @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
  public ResponseEntity<Object> httpRequestMethodNotSupportedException(
      HttpRequestMethodNotSupportedException ex, WebRequest request) {

    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.METHOD_NOT_ALLOWED;

    pageNotFoundLogger.warn(ex.getMessage());

    Set<HttpMethod> supportedMethods = ex.getSupportedHttpMethods();
    if (!CollectionUtils.isEmpty(supportedMethods)) {
      headers.setAllow(supportedMethods);
    }
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0",
        "Not Supported");

  }

  @ExceptionHandler(value = {HttpMediaTypeNotSupportedException.class})
  public ResponseEntity<Object> httpMediaTypeNotSupportedException(
      HttpMediaTypeNotSupportedException ex, WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.UNSUPPORTED_MEDIA_TYPE;

    List<MediaType> mediaTypes = ex.getSupportedMediaTypes();
    if (!CollectionUtils.isEmpty(mediaTypes)) {
      headers.setAccept(mediaTypes);
    }

    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0",
        "Media not support");
  }


  @ExceptionHandler(value = {HttpMediaTypeNotAcceptableException.class})
  public ResponseEntity<Object> httpMediaTypeNotAcceptableException(
      HttpMediaTypeNotAcceptableException ex, WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.NOT_ACCEPTABLE;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0",
        "HTTP Media Type not supported");
  }

  @ExceptionHandler(value = {MissingPathVariableException.class})
  public ResponseEntity<Object> missingPathVariableException(MissingPathVariableException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0",
        "missing path variable");
  }


  @ExceptionHandler(value = {MissingServletRequestParameterException.class})
  public ResponseEntity<Object> missingServletRequestParameterException(
      MissingServletRequestParameterException ex, WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.BAD_REQUEST;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0",
        "missing param");
  }

  @ExceptionHandler(value = {ServletRequestBindingException.class})
  public ResponseEntity<Object> servletRequestBindingException(ServletRequestBindingException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.BAD_REQUEST;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0", "Bad binding");
  }

  @ExceptionHandler(value = {ConversionNotSupportedException.class})
  public ResponseEntity<Object> conversionNotSupportedException(ConversionNotSupportedException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0",
        "internal error");
  }

  @ExceptionHandler(value = {TypeMismatchException.class})
  public ResponseEntity<Object> typeMismatchException(TypeMismatchException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0", "Bad bean");
  }

  @ExceptionHandler(value = {HttpMessageNotReadableException.class})
  public ResponseEntity<Object> httpMessageNotReadableException(HttpMessageNotReadableException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.BAD_REQUEST;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0", "Bad bean");
  }

  @ExceptionHandler(value = {HttpMessageNotWritableException.class})
  public ResponseEntity<Object> httpMessageNotWritableException(HttpMessageNotWritableException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0", "Bad bean");
  }

  @ExceptionHandler(value = {MethodArgumentNotValidException.class})
  public ResponseEntity<Object> methodArgumentNotValidException(MethodArgumentNotValidException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.BAD_REQUEST;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0",
        "Bad validation");
  }

  @ExceptionHandler(value = {MissingServletRequestPartException.class})
  public ResponseEntity<Object> missingServletRequestPartException(
      MissingServletRequestPartException ex, WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.BAD_REQUEST;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0",
        "Bad multi part");
  }

  @ExceptionHandler(value = {BindException.class})
  public ResponseEntity<Object> bindException(BindException ex, WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.BAD_REQUEST;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0", "Bad bean");
  }

  @ExceptionHandler(value = {NoHandlerFoundException.class})
  public ResponseEntity<Object> noHandlerFoundException(NoHandlerFoundException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.NOT_FOUND;
    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0",
        ex.getMessage());
  }



  @ExceptionHandler(value = {AsyncRequestTimeoutException.class})
  public ResponseEntity<Object> asyncRequestTimeoutException(AsyncRequestTimeoutException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE;

    if (request instanceof ServletWebRequest) {
      ServletWebRequest servletWebRequest = (ServletWebRequest) request;
      HttpServletResponse response = servletWebRequest.getResponse();
      if (response != null && response.isCommitted()) {
        log.warn("Async request timed out");
        return null;
      }
    }

    return handleExceptionInternal(ex, null, headers, status, request, "SYS_REQ_0",
        "aysnc timeout bean");
  }

  @ExceptionHandler(NoSuchFieldException.class)
  public final ResponseEntity<Object> handleNoSuchFieldException(NoSuchFieldException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request, "SYS_REQ_0",
        "File Not found bean");

  }




  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {DecodingException.class})
  public ResponseEntity<Object> jsonDecodingException(DecodingException ex, WebRequest request) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request, "SYS_REQ_0",
        "Bad Json");

  }
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {ServerWebInputException.class})
  public ResponseEntity<Object> invalidFormatDecodingException(ServerWebInputException ex,
      WebRequest request) {

    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request, "SYS_REQ_0",
        "Bad parameter");

  }


  @ExceptionHandler(value = {JsonProcessingException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<Object> jsonProcessingException(JsonProcessingException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request, "SYS_REQ_0",
        "Bad Json");
  }

  @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<Object> jsonProcessingException(MethodArgumentTypeMismatchException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request, "SYS_REQ_0",
        "Bad parameter");
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {DataIntegrityViolationException.class})
  public ResponseEntity<Object> validationExceptionException(DataIntegrityViolationException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request, "SYS_REQ_0",
        "Bad parameter");

  }

  @ExceptionHandler(value = {IllegalArgumentException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<Object> illegalArgumentException(IllegalArgumentException ex,
      WebRequest request) {

    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request, "SYS_REQ_0",
        "Bad parameter");
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {ValidationException.class})
  public ResponseEntity<Object> validationExceptionException(ValidationException ex,
      WebRequest request) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request, "SYS_REQ_0",
        "non valide parameter");

  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(ConstraintViolationException.class)
  protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex,
      WebRequest request) {

    String errors = ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage)
        .collect(Collectors.joining());

    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request, "SYS_REQ_0",
        errors);
  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = {IllegalStateException.class})
  public ResponseEntity<Object> illegalStatusException(IllegalStateException ex,
      WebRequest request) {

    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.BAD_REQUEST, request, "SYS_REQ_0",
        ex.getMessage());


  }


  @ExceptionHandler(value = {RuntimeException.class})
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ResponseEntity<Object> notRunTimeException(RuntimeException ex, WebRequest request) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.INTERNAL_SERVER_ERROR, request,
        "SYS_REQ_0", ex.getMessage());

  }


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(
      value = {JsonException.class, MissingParamsException.class, InvalidDataException.class})
  public void jsonProcessingException(AbstractArmException ex, WebRequest request,
      HttpServletResponse response, Locale locale) {

    handlingExeption(ex, response, locale);
  }



  @ExceptionHandler(value = {NotFoundException.class})
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public void notFountId(NotFoundException ex, WebRequest request,
      HttpServletResponse response, Locale locale) {
    handlingExeption(ex, response, locale);

  }

  @ExceptionHandler(value = {NotContentException.class})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void notFountId(NotContentException ex, WebRequest request, HttpServletResponse response,
      Locale locale) {
    handlingExeption(ex, response, locale);

  }



  @ExceptionHandler(value = {NoMoreCapacityException.class, NotAcceptedException.class})
  @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
  public void notRunTimeException(AbstractArmException ex, WebRequest request,
      HttpServletResponse response, Locale locale) {
    handlingExeption(ex, response, locale);

  }


  @ExceptionHandler(value = {UnAuthorizedException.class})
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  public void unAuthorizedException(UnAuthorizedException ex, WebRequest request,
      HttpServletResponse response, Locale locale) {
    handlingExeption(ex, response, locale);

  }




  @ExceptionHandler(value = {Exception.class})
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ResponseEntity<Object> notRunTimeException(Exception ex, WebRequest request) {
    HttpHeaders headers = new HttpHeaders();

    return handleExceptionInternal(ex, null, headers, HttpStatus.INTERNAL_SERVER_ERROR, request,
        "SYS_REQ_0", null);

  }

  /**
   * A single place to customize the response body of all exception types.
   * <p>
   * The default implementation sets the {@link WebUtils#ERROR_EXCEPTION_ATTRIBUTE} request
   * attribute and creates a {@link ResponseEntity} from the given body, headers, and status.
   * 
   * @param ex the exception
   * @param body the body for the response
   * @param headers the headers for the response
   * @param status the response status
   * @param request the current request
   */
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body,
      HttpHeaders headers, HttpStatus status, WebRequest request, String code, String message) {

    UUID id = UUID.randomUUID();
    headers.add(X_ERROR_ID, id.toString());
    headers.add(X_ERROR_TIMESTAMP, LocalDateTime.now().toString());
    headers.add(X_ERROR_CODE, code);
    headers.add(X_ERROR_MESSAGE, message);
    log.error("non Excepect exception", ex);
    if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
      request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
    }
    return new ResponseEntity<>(body, headers, status);
  }


  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body,
      HttpHeaders headers, HttpStatus status, WebRequest request) {

    UUID id = UUID.randomUUID();
    headers.add(X_ERROR_ID, id.toString());
    headers.add(X_ERROR_TIMESTAMP, LocalDateTime.now().toString());

    if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
      request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
    }
    return new ResponseEntity<>(body, headers, status);
  }

  private void handlingExeption(AbstractArmException ex, HttpServletResponse response,
      Locale locale) {
    UUID id = UUID.randomUUID();
    response.addHeader(X_ERROR_ID, id.toString());
    response.addHeader(X_ERROR_TIMESTAMP, LocalDateTime.now().toString());
    response.addHeader(X_ERROR_CODE, ex.getCode());
    String message;
    if (ex.getArgs().length == 0)
      message = messageSource.getMessage(ex.getMessage(), new Object[] {id}, locale);
    else
      message = messageSource.getMessage(ex.getMessageCode(), ex.getArgs(), locale);

    response.addHeader(X_ERROR_MESSAGE, message);
    if (ex.isCatched()) {
      log.debug("Catched Exception with id :{} , code :{} and message :{}", id, ex.getCode(),
          message);

    } else {
      log.error("Uncatched Exception with id :{} , code :{} , message :{} ,and exeption: {}", id,
          ex.getCode(), message, ex);
    }

  }



}
