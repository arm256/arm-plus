package com.arm256.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.arm256.entity.Loadable;
import com.arm256.entity.Persistable;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.ExportTypes;

public interface CRUDAuth<E extends Persistable<ID>, D extends ModelLoadable<ID>, ID extends Serializable> {

	D retrieve(Loadable<ID> id, String createdBy);

	@Transactional
	void update(D d, String createdBy);

	@Transactional
	void trashed(Loadable<ID> id, String createdBy);
	
	@Transactional
	void delete(Loadable<ID> id, String createdBy);

	InputStream export(ExportTypes type, Pageable pageable, String createdBy) throws IOException;

	E read(Loadable<ID> id, String createdBy);

}
