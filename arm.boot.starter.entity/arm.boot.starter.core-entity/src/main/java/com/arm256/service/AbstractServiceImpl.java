package com.arm256.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import com.arm256.entity.Loadable;
import com.arm256.entity.Persistable;
import com.arm256.exception.NotAcceptedException;
import com.arm256.exception.NotContentException;
import com.arm256.exception.NotFoundException;
import com.arm256.model.CrudEvent;
import com.arm256.model.CrudEvent.CrudType;
import com.arm256.model.CrudEvent.Type;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.ExportTypes;
import com.arm256.model.mapper.Mapper;
import com.arm256.repository.Generic;
import com.arm256.validator.tags.Crud;
import com.arm256.validator.tags.Crud.Post;
import com.arm256.validator.tags.Crud.Put;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Setter;

public abstract class AbstractServiceImpl<E extends Persistable<ID>, D extends ModelLoadable<ID>, R extends Generic<E, ID>, ID extends Serializable>
    implements CRUD<E, D, ID> {

  protected R repository;
  protected Mapper<E, D> mapper;

  protected final Supplier<NotFoundException> notFound = () -> new NotFoundException(name());
  protected final Function<E, D> toDto = e -> mapper.toDto(e);
  protected final Function<D, E> toEntity = d -> mapper.toEntity(d);
  protected final ObjectMapper objectMapper = new ObjectMapper();
  @Autowired
  @Setter
  protected Validator validator;

  @Autowired
  @Setter
  protected ApplicationEventPublisher applicationEventPublisher;

  protected AbstractServiceImpl(R repository, Mapper<E, D> mapper) {
    this.repository = repository;
    this.mapper = mapper;
    Class<E> persistentClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass())
        .getActualTypeArguments()[0];

  }

  @Override
  public void validate(D d, Class<?> group) {
    Set<ConstraintViolation<D>> validate;
    if (group != null) {
      validate = validator.validate(d, Crud.class, group);
    } else {
      validate = validator.validate(d, Crud.class);
    }
    if (!validate.isEmpty()) {
      throw new ConstraintViolationException(validate);
    }
  }

  @Override
  public D create(D d) {
    validate(d, Post.class);
   return mapper
    .toEntityMap(d)
    .map(e -> create(e))
    .flatMap(e -> mapper.toDtoMap(e))
    .get();
//    return mapper.toDto(create(mapper.toEntity(d)));
  }

  @Override
  public E create(E e) {
     E x = repository.save(e);
     pushEvent(addEvent(x, CrudType.CREATE));
     return x;
  }

  protected CrudEvent<ID> addEvent(E e, Type type) {
    CrudEvent<ID> event = new CrudEvent<ID>();
    event.setType(type);
    event.setCreatedBy(e.getCreatedBy());
    event.setSourceId(e.getId());
    event.setName(name());
    return event;
  }

  public void pushEvent(CrudEvent<ID> e) {
    e.setName(name());
    applicationEventPublisher.publishEvent(getCrudEventService(e));
  }

  public CrudEventService getCrudEventService(CrudEvent<ID> e) {
    return new CrudEventService(e);
  }

  @Override
  public Page<D> retrieves(Pageable pageable) {
    if (pageable == null) {
      pageable = PageRequest.of(0, 20);
    }
    return repository.findAll(pageable).map(mapper::toDto);
  }

  @Override
  public Page<D> retrieves(Pageable pageable, String createdBy) {
    if (pageable == null) {
      pageable = PageRequest.of(0, 20);
    }
    return repository.findByCreatedByAndArchiveFalse(pageable, createdBy).map(mapper::toDto);
  }

  @Override
  public Page<D> retrieves(Pageable pageable, List<String> createdBy) {
    if (pageable == null) {
      pageable = PageRequest.of(0, 20);
    }
    return repository.findByArchiveFalseAndCreatedByIn(pageable, createdBy).map(mapper::toDto);
  }

  @Override
  public Page<D> retrieves(Specification<E> spec, Pageable pageable) {
    return repository.findAll(spec, pageable).map(mapper::toDto);
  }

  @Override
  public Page<D> retrievesTrashed(Pageable pageable) {
    if (pageable == null) {
      pageable = PageRequest.of(0, 20);
    }
    return repository.findByArchiveTrue(pageable).map(mapper::toDto);
  }

  @Override
  public Page<D> retrievesTrashed(Pageable pageable, String createdBy) {
    if (pageable == null) {
      pageable = PageRequest.of(0, 20);
    }
    return repository.findByCreatedByAndArchiveTrue(pageable, createdBy).map(mapper::toDto);
  }



  @Override
  public D retrieve(Loadable<ID> id) {
    return toDto(read(id));
  }


  @Override
  public void update(D d) {
    validate(d, Put.class);
    E entity = read(d);
    E x = repository.save(mapper.entityUpdate(entity, d));

    pushEvent(addEvent(x, CrudType.UPDATE));
  }

  @Override
  public void delete(Loadable<ID> id) {
    E e = repository.findById(id.getId()).orElseThrow(notFound);
    delete(e);
  }

  @Override
  public void delete(E e) {
    repository.delete(e);
    pushEvent(addEvent(e, CrudType.DELETE));
  }

  @Override
  public void deleteTrashed(String createdBy) {
    List<E> e = repository.findByCreatedByAndArchiveTrue(createdBy);
    if (e == null || e.isEmpty()) {
      throw new NotContentException();
    }

    repository.deleteAll(e);
    for (E e2 : e) {
      pushEvent(addEvent(e2, CrudType.DELETE));
    }
  }



  @Override
  public void trashed(Loadable<ID> id) {
    E e = read(id);
    if (e.isArchive()) {
      throw new NotAcceptedException();
    }
    e.setArchive(true);
    repository.save(e);
    pushEvent(addEvent(e, CrudType.TRACHED));

  }



  @Override
  public void reset(Loadable<ID> id) {
    E e = repository.findByIdAndArchiveTrue(id.getId()).orElseThrow(notFound);

    e.setArchive(false);
    repository.save(e);
    pushEvent(addEvent(e, CrudType.RESET));

  }

  @Override
  public void resetAll(String createdBy) {
    List<E> e = repository.findByCreatedByAndArchiveTrue(createdBy);
    if (e == null || e.isEmpty()) {
      throw new NotContentException();
    }

    e.forEach(x -> x.setArchive(false));
    repository.saveAll(e);
    e.forEach(x -> pushEvent(addEvent(x, CrudType.RESET)));
  }


  @Override
  public E read(Loadable<ID> id) {
    return repository.findByIdAndArchiveFalse(id.getId()).orElseThrow(notFound);
  }



  @Override
  public InputStream export(ExportTypes type, Pageable pageable) throws IOException {

    Page<D> read = retrieves(pageable);
    if (read.isEmpty()) {
      throw new NotContentException();
    }
    return new ByteArrayInputStream(objectMapper.writeValueAsBytes(read.getContent()));
  }



  @Override
  public List<D> imports(ExportTypes type, InputStream stream, Class<D> model) throws IOException {

    JavaType typemap = objectMapper.getTypeFactory().constructCollectionType(List.class, model);


    List<D> list = objectMapper.readValue(stream, typemap);

    List<D> createdList = new ArrayList<>();

    for (D d : list) {
      try {
        createdList.add(create(d));
      } catch (Exception e) {
        // TODO: handle exception
      }

    }
    return createdList;

  }

  @SuppressWarnings("unchecked")
  @Override
  public R getRepository() {
    return repository;
  }

  @Override
  public Mapper<E, D> getMapper() {
    return mapper;
  }

  public abstract String name();

  protected List<E> notEmpty(List<E> entities) {
    if (entities.isEmpty()) {
      throw notFound.get();
    }
    return entities;
  }
}
