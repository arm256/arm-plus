package com.arm256.service.history;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arm256.entity.history.HistoryFieldEntity;
import com.arm256.entity.history.HistoryRowsEntity;
import com.arm256.enumation.ExcuteStatus;
import com.arm256.exception.NotFoundException;
import com.arm256.model.HistoryField;
import com.arm256.model.HistoryRows;
import com.arm256.model.mapper.history.HistoryFieldMapper;
import com.arm256.model.mapper.history.HistoryRowsMapper;
import com.arm256.repository.history.HistoryFieldRepo;
import com.arm256.repository.history.HistoryRowsRepo;
import com.arm256.service.HistoryService;

@Service
public class HistoryServiceImpl implements HistoryService {

	@Autowired
	private HistoryFieldRepo fieldRepo;
	@Autowired
	private HistoryFieldMapper fieldMapper;
	@Autowired
	private HistoryRowsRepo rowHistory;
	@Autowired
	private HistoryRowsMapper rowMapper;

	@Override
	public List<HistoryField> readHistories() {
		return fieldMapper.toDto(fieldRepo.findAll());
	}

	@Override
	public void addHistoryField(HistoryField history) {
		fieldRepo.save(fieldMapper.toEntity(history));
	}

	@Override
	public List<HistoryRows> readHistory(String fieldName) {
		return rowMapper.toDto(rowHistory.findByFieldObjectAndArchiveFalse(read(fieldName)));
	}

	private HistoryFieldEntity read(String name) {
		return fieldRepo.findByName(name).orElseThrow(() -> new NotFoundException("History Field"));
	}
	@Override
	public List<HistoryRows> readHistory(String fieldName, LocalDateTime after) {
		return rowMapper.toDto(rowHistory.findByFieldObjectAndCreatedDateAfter(read(fieldName), after));
	}

	@Override
	public List<HistoryRows> readHistory(String fieldName, ExcuteStatus status) {
		return rowMapper.toDto(rowHistory.findByFieldObjectAndStatusAndArchiveFalse(read(fieldName), status));
	}

	@Override
	public List<HistoryRows> readHistory(String fieldName, ExcuteStatus status, LocalDateTime after) {
		return rowMapper
				.toDto(rowHistory.findByFieldObjectAndStatusAndCreatedDateAfter(read(fieldName), status, after));
	}

	@Override
	public List<HistoryRows> readHistory(String fieldName, LocalDateTime from, LocalDateTime to) {
		return rowMapper.toDto(rowHistory.findByFieldObjectAndCreatedDateBetween(read(fieldName), from, to));
	}

	@Override
	public void archive(LocalDateTime before) {
		rowHistory.archiveAll(before);

	}

	@Override
	public void increaseRetryCount(UUID trackId) {
		rowHistory.increaseRetryCount(trackId);
	}

	@Override
	public void rowComplete(UUID trackId) {
		rowHistory.changeStatus(trackId, ExcuteStatus.COMPLETE);

	}

	@Override
	public void rowFailed(UUID trackId) {
		rowHistory.changeStatus(trackId, ExcuteStatus.FAILD);
	}

	@Override
	public void rowStatus(UUID trackId, ExcuteStatus status) {
		rowHistory.changeStatus(trackId, status);
	}

	@Override
	public void addHistoryRows(String fieldName, HistoryRows row) {
		HistoryFieldEntity entity = read(fieldName);
		HistoryRowsEntity historyRow = rowMapper.toEntity(row);
		historyRow.setFieldObject(entity);
		entity.addHistoryRows(historyRow);

		fieldRepo.save(entity);

	}

}
