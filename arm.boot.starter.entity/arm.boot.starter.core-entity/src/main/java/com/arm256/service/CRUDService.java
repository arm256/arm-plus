package com.arm256.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.arm256.entity.Loadable;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.ExportTypes;

public interface CRUDService<D extends ModelLoadable<ID>, ID extends Serializable> {

	default void createAll(Iterable<D> d) {
		d.forEach(this::create);
	}

	D create(D d);


    void validate(D d, Class<?> group);

	Page<D> retrieves(Pageable pageable);

	Page<D> retrieves(Pageable pageable, String createdBy);

	Page<D> retrievesTrashed(Pageable pageable);

	Page<D> retrieves(Pageable pageable, List<String> createdBy);

	Page<D> retrievesTrashed(Pageable pageable, String createdBy);
	
	D retrieve(Loadable<ID> id);

	void update(D d);

	void delete(Loadable<ID> id);
	
	void trashed(Loadable<ID> id);

	void reset(Loadable<ID> id);

	InputStream export(ExportTypes type, Pageable pageable) throws IOException;

	List<D> imports(ExportTypes type, InputStream stream, Class<D> model) throws IOException;

	void resetAll(String createdBy);

	void deleteTrashed(String createdBy);

}
