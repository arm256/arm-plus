package com.arm256.service;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;
import com.arm256.entity.Loadable;
import com.arm256.entity.Persistable;
import com.arm256.model.CrudEvent;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.Mapper;
import com.arm256.repository.Generic;

public interface CRUD<E extends Persistable<ID>, D extends ModelLoadable<ID>, ID extends Serializable>
    extends CRUDService<D, ID> {

	default void createAllEntity(Iterable<E> d) {
		d.forEach(this::create);
	}

	@Transactional
	E create(E e);


	Page<D> retrieves(Specification<E> spec, Pageable pageable);
	

	E read(Loadable<ID> id);

	@Transactional
    void delete(E e);

	public <R extends Generic<E, ID>> R getRepository();

	public Mapper<E, D> getMapper();

	default D toDto(E e) {
		return getMapper().toDto(e);
	}

	default E toEntity(D d) {
		return getMapper().toEntity(d);
	}

	default List<D> toDto(List<E> e) {
		return getMapper().toDto(e);
	}

	default List<E> toEntity(List<D> d) {
		return getMapper().toEntity(d);
	}

    void pushEvent(CrudEvent<ID> e);

}
