package com.arm256.service;

import java.util.UUID;

import com.arm256.entity.Persistable;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.Mapper;
import com.arm256.repository.Generic;

public abstract class AbstractServiceImplUUID<E extends Persistable<UUID>, D extends ModelLoadable<UUID>, R extends Generic<E, UUID>>
		extends AbstractServiceImpl<E, D, R, UUID> implements CRUD_UUID<E, D> {

	public AbstractServiceImplUUID(R repository, Mapper<E, D> mapper) {
		super(repository, mapper);
	}

}
