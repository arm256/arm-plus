package com.arm256.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.arm256.entity.Loadable;
import com.arm256.entity.Persistable;
import com.arm256.exception.NotContentException;
import com.arm256.exception.UnAuthorizedException;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.ExportTypes;
import com.arm256.model.mapper.Mapper;
import com.arm256.repository.Generic;

public abstract class AbstractAuthServiceImpl<E extends Persistable<ID>, D extends ModelLoadable<ID>, R extends Generic<E, ID>, ID extends Serializable>
		extends AbstractServiceImpl<E, D, R, ID> implements CRUDAuth<E, D, ID> {


	
	public AbstractAuthServiceImpl(R repository, Mapper<E, D> mapper) {
		super(repository, mapper);
	}
	@Override
	public D retrieve(Loadable<ID> id, String createdBy) {

		E e = read(id);
		
		if (e.getCreatedBy().equals(createdBy)) {
			throw new UnAuthorizedException();
		}
		return toDto(e);
	}
	@Override
	public void update(D d, String createdBy) {
		E e = read(d);
		if (e.getCreatedBy().equals(createdBy)) {
			throw new UnAuthorizedException();
		}
		repository.save(mapper.entityUpdate(e, d));
	}
	
	@Override
	public void delete(Loadable<ID> id, String createdBy) {
		E e = repository.findById(id.getId()).orElseThrow(notFound);
		if (e.getCreatedBy().equals(createdBy)) {
			throw new UnAuthorizedException();
		}
		repository.delete(e);
	}

	@Override
	public void trashed(Loadable<ID> id, String createdBy) {
		E e = read(id);
		if (e.getCreatedBy().equals(createdBy)) {
			throw new UnAuthorizedException();
		}
		e.setArchive(true);
		repository.save(e);
	}
	
	@Override
	public E read(Loadable<ID> id, String createdBy) {
		E e = read(id);
		
		if (e.getCreatedBy().equals(createdBy)) {
			throw new UnAuthorizedException();
		}
		
		return e;
	}
	
	@Override
	public InputStream export(ExportTypes type, Pageable pageable, String createdBy) throws IOException {
	    Page<D> read = retrieves(pageable,createdBy);
		if (read.isEmpty()) {
			throw new NotContentException();  
		}
		return new ByteArrayInputStream(objectMapper.writeValueAsBytes(read.getContent()));
	}
}
