package com.arm256.controller;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.unprocessableEntity;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.arm256.model.ModelLoadable;
import com.arm256.model.mapper.ExportTypes;
import com.arm256.service.CRUD;
import com.arm256.service.CRUDService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RequestMapping(consumes = "application/json", produces = "application/json")
public abstract class AbstractReadCRUDController<T extends ModelLoadable<E>, E extends Serializable> {

  protected final CRUDService<T, E> service;
	
  protected AbstractReadCRUDController(CRUDService<T, E> service) {
		this.service = service;
	}

	@GetMapping()
	public ResponseEntity<List<T>> all(@RequestHeader(name = "userId", required = true) String userid,
			@RequestParam(name = "page", defaultValue = "0", required = false) int pageNo,
			@RequestParam(name = "size", defaultValue = "20", required = false) int pageSize,
			HttpServletResponse response

//			@RequestParam(required = false) Pageable page
	) {

		Pageable page = PageRequest.of(pageNo, pageSize);
		Page<T> read = service.retrieves(page, userid);

		if (read.isEmpty()) {
			return noContent().build();
		}
		HttpHeaders responseHeader = new HttpHeaders();

		responseHeader.add("X-content-total", "" + read.getTotalElements());
		responseHeader.add("X-page-total", "" + read.getTotalPages());
		responseHeader.add("X-content-size", "" + read.getNumberOfElements());

		return new ResponseEntity<List<T>>(read.getContent(), responseHeader, HttpStatus.OK);
	}

	@GetMapping("/admin")
	public ResponseEntity<List<T>> all(@RequestParam(name = "page", defaultValue = "0", required = false) int pageNo,
			@RequestParam(name = "size", defaultValue = "20", required = false) int pageSize,
			HttpServletResponse response

//			@RequestParam(required = false) Pageable page
	) {

		Pageable page = PageRequest.of(pageNo, pageSize);
		Page<T> read = service.retrieves(page);

		if (read.isEmpty()) {
			return noContent().build();
		}
		HttpHeaders responseHeader = new HttpHeaders();

		responseHeader.add("X-content-total", "" + read.getTotalElements());
		responseHeader.add("X-page-total", "" + read.getTotalPages());
		responseHeader.add("X-content-size", "" + read.getNumberOfElements());

		return new ResponseEntity<List<T>>(read.getContent(), responseHeader, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	@Operation(parameters = {
			@Parameter(name = "id", in = ParameterIn.PATH, required = true, description = "uuid of element need to select", schema = @Schema(type = "string", format = "uuid", pattern = "/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"), example = "dd58a026-5d7e-4c42-b2f0-83081938903f") }, responses = @ApiResponse(responseCode = "200", description = "ok", headers = {}))
	public T get(@PathVariable("id") E queriesId) {
		return this.service.retrieve(() -> queriesId);
	}


	@GetMapping("export")
	public ResponseEntity<Resource> export(@RequestParam(name = "type",defaultValue = "JSON") ExportTypes types,  @RequestParam(name = "page", defaultValue = "0", required = false) int pageNo,
			@RequestParam(name = "size", defaultValue = "20", required = false) int pageSize,
			HttpServletResponse response

//			@RequestParam(required = false) Pageable page
	) {
    
		Pageable page = PageRequest.of(pageNo, pageSize);

		HttpHeaders responseHeader = new HttpHeaders();

		responseHeader.add(
			        HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"export."+types.name()+"\"");

	    InputStreamResource resource = null;
        try {
         	InputStream stream = service.export(types, page);
			 resource = new InputStreamResource(stream);
    	} catch (IOException e) {
    		return unprocessableEntity().build();
		}
        
        return ResponseEntity.ok()
                .headers(responseHeader)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
	}
	
	@SuppressWarnings("unchecked")
	public <S extends CRUD<?, T, E>> S getService() {
		return (S) service;
	}

	public abstract String controllerPath();
}
