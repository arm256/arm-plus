package com.arm256.controller;

import java.util.UUID;
import com.arm256.model.ModelLoadable;
import com.arm256.service.CRUDService;

public abstract class AbstractCRUD_UUIDController<T extends ModelLoadable<UUID>>
		extends AbstractCRUDController<T, UUID> {

	protected AbstractCRUD_UUIDController(CRUDService<T, UUID> service, Class<T> modelClazz) {
		super(service,modelClazz);
	}

	public abstract String controllerPath();
}
