package com.arm256.entity.history;

import java.time.LocalDateTime;
import java.util.UUID;

import jakarta.persistence.Cacheable;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedBy;

import com.arm256.entity.Persistable;
import com.arm256.enumation.ExcuteStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "historyrow")
public class HistoryRowsEntity implements Persistable<Long> {

	@Id
	@Column(name = "history_rows_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "history_rows_seq")
	@SequenceGenerator(name = "history_rows_seq", sequenceName = "history_rows_seq", allocationSize = 1, initialValue = 1)
	private Long id;

	@Version
	private long version;
	
	@CreatedBy
	private String createdBy;
	
	@Column(unique = true)
	private UUID trackingId;

	private LocalDateTime createdDate;

	private ExcuteStatus status = ExcuteStatus.PROGRESS;

	private int retryTimes;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "field_id")
	private HistoryFieldEntity fieldObject;

	private boolean archive;

}
