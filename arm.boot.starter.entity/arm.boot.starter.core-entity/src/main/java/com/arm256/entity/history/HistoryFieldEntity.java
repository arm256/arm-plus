package com.arm256.entity.history;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Cacheable;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedBy;

import com.arm256.entity.Persistable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HistoryFieldEntity implements Persistable<Long> {

    @Id
	@Column(name = "history_field_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "historyfieldregistry_seq")
	@SequenceGenerator(name = "historyfieldregistry_seq", sequenceName = "historyfieldregistry_seq", allocationSize
        = 1, initialValue = 1)
	private Long id;
    
	@Version
	private long version;
	
	@CreatedBy
	private String createdBy;

	@Column(name = "name", length = 64, unique = true)
	private String name;

	private int maxRetry;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fieldObject", cascade = CascadeType.ALL, orphanRemoval = true)
	@Builder.Default
	private List<HistoryRowsEntity> historyRows = new ArrayList<>();

	private boolean archive;

	public void addHistoryRows(HistoryRowsEntity add) {
		historyRows.add(add);
	}

}
