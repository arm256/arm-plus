package com.arm256.entity;

import java.time.LocalDateTime;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.SequenceGenerator;
import org.springframework.data.annotation.CreatedDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@Entity(name = "event_history")
public class EventEntity implements Loadable<Long> {

	@Id
	@Column(name = "event_history_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_history_seq")
	@SequenceGenerator(name = "event_history_seq", sequenceName = "event_history_seq", allocationSize = 1, initialValue = 1)
	private Long id;

	@CreatedDate
	private LocalDateTime createdDate;

	@Lob
	@Column(name = "dataBefore")
	private String data;


	@Column(name = "scope_note", columnDefinition = "text")
	private String scopeNote;

}
