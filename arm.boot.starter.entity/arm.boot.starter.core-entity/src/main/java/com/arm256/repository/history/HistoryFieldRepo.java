package com.arm256.repository.history;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.arm256.entity.history.HistoryFieldEntity;
import com.arm256.repository.GenericLong;

@Repository
public interface HistoryFieldRepo extends GenericLong<HistoryFieldEntity> {

	public Optional<HistoryFieldEntity> findByName(String name);
}
