package com.arm256.repository;

import org.springframework.data.repository.NoRepositoryBean;

import com.arm256.entity.Persistable;

@NoRepositoryBean
public interface GenericLong<T extends Persistable<Long>> extends Generic<T, Long> {

}
