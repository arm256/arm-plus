package com.arm256.repository;

import java.util.UUID;

import org.springframework.data.repository.NoRepositoryBean;

import com.arm256.entity.Persistable;

@NoRepositoryBean
public interface GenericUUID<T extends Persistable<UUID>> extends Generic<T, UUID> {

}
