package com.arm256.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import jakarta.persistence.LockModeType;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.NoRepositoryBean;
import com.arm256.entity.Persistable;

@NoRepositoryBean
@Transactional
public interface Generic<T extends Persistable<ID>, ID extends Serializable>
		extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public Optional<T> findByIdAndArchiveFalse(ID id);

	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public Optional<T> findByIdAndArchiveTrue(ID id);
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public Page<T> findByCreatedByAndArchiveFalse(Pageable page, String createdBy);
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public Page<T> findByCreatedByAndArchiveTrue(Pageable page,String createdBy);
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public Page<T> findByArchiveTrue(Pageable page);
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public Page<T> findByArchiveFalseAndCreatedByIn(Pageable page,List<String> createdByList);
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public Page<T> findByArchiveTrueAndCreatedByIn(Pageable page,List<String> createdByList);

	public List<T> findByCreatedByAndArchiveTrue(String createdBy);
	
	public List<T> findByCreatedByAndArchiveFalse(String createdBy);

}
