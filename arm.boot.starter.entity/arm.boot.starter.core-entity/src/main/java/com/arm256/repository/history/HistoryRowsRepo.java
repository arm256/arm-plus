package com.arm256.repository.history;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.arm256.entity.history.HistoryFieldEntity;
import com.arm256.entity.history.HistoryRowsEntity;
import com.arm256.enumation.ExcuteStatus;
import com.arm256.repository.GenericLong;

@Repository
public interface HistoryRowsRepo extends GenericLong<HistoryRowsEntity> {

	public List<HistoryRowsEntity> findByFieldObjectAndArchiveFalse(HistoryFieldEntity fieldObject);

	public List<HistoryRowsEntity> findByFieldObjectAndCreatedDateAfter(HistoryFieldEntity fieldObject,
			LocalDateTime createdDate);

	public List<HistoryRowsEntity> findByFieldObjectAndStatusAndArchiveFalse(HistoryFieldEntity fieldObject,
			ExcuteStatus status);

	public List<HistoryRowsEntity> findByFieldObjectAndStatusAndCreatedDateAfter(HistoryFieldEntity fieldObject,
			ExcuteStatus status, LocalDateTime createdDate);

	public List<HistoryRowsEntity> findByFieldObjectAndCreatedDateBetween(HistoryFieldEntity fieldObject,
			LocalDateTime from, LocalDateTime to);

	@Modifying
	@Query("update HistoryRowsEntity h set h.archive = true where h.createdDate > ?1")
	int archiveAll(LocalDateTime createdDate);

	@Modifying
	@Query("update HistoryRowsEntity h set h.retryTimes = h.retryTimes+1 where h.trackingId > ?1")
	int increaseRetryCount(UUID trackId);

	@Modifying
	@Query("update HistoryRowsEntity h set h.status = ?2 where h.trackingId > ?1")
	int changeStatus(UUID trackId, ExcuteStatus status);

}
