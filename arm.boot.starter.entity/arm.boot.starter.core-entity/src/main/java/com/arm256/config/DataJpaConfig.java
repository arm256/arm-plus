package com.arm256.config;

import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityManagerFactory;
import lombok.Setter;

@EnableSpringDataWebSupport
@Configuration
// @EntityScan(basePackageClasses = { EventEntity.class })
@EnableJpaAuditing
public class DataJpaConfig {

	@Bean
	AuditorAware<String> auditor() {
		return () -> {
			try {
				ServletRequestAttributes curentRequest = (ServletRequestAttributes) RequestContextHolder
						.currentRequestAttributes();
				return Optional.ofNullable(curentRequest).map(ServletRequestAttributes::getRequest)
                .map(e -> e.getHeader("userId"));
			} catch (IllegalStateException e) {
				return Optional.of("SYSTEM");
			}

		};

	}

	@Autowired
	@Setter
	protected ModelMapper modelMapper;

	@Autowired
	protected EntityManagerFactory emFactory;

	@PostConstruct
	public void init() {
		modelMapper.getConfiguration()
				.setPropertyCondition(context -> emFactory.getPersistenceUnitUtil().isLoaded(context.getSource()));
	}

    @Bean("exception_message")
     MessageSource messageSource() {
      ReloadableResourceBundleMessageSource messageSource =
          new ReloadableResourceBundleMessageSource();

      messageSource.setBasename("classpath:messages");
      messageSource.setDefaultEncoding("UTF-8");
      return messageSource;
    }

}
