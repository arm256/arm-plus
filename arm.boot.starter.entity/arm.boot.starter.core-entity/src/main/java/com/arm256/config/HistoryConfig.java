package com.arm256.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.arm256.controller.HistoryController;
import com.arm256.entity.history.HistoryFieldEntity;
import com.arm256.entity.history.HistoryRowsEntity;
import com.arm256.model.mapper.history.HistoryFieldMapper;
import com.arm256.model.mapper.history.HistoryRowsMapper;
import com.arm256.repository.history.HistoryFieldRepo;
import com.arm256.repository.history.HistoryRowsRepo;
import com.arm256.service.EventHistoryListener;
import com.arm256.service.history.HistoryServiceImpl;

@Configuration
@EntityScan(basePackageClasses = { HistoryFieldEntity.class, HistoryRowsEntity.class })
@EnableJpaRepositories(basePackageClasses = { HistoryFieldRepo.class, HistoryRowsRepo.class })
@Import({ HistoryFieldMapper.class, HistoryRowsMapper.class, HistoryServiceImpl.class, HistoryController.class,
		EventHistoryListener.class })
public class HistoryConfig {

}
