package com.arm256.validate.service;

import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import com.arm256.exception.NotAcceptedException;
import com.arm256.exception.NotFoundException;
import com.arm256.model.FieldValidators;
import com.arm256.model.MetadataField;
import com.arm256.utils.Pair;
import com.arm256.validator.AbstractConstraintValidator;
import com.arm256.validator.ParamsConstraintValidator;
import com.arm256.validator.Validators;
import com.arm256.validator.ValidatorsService;

import jakarta.annotation.PostConstruct;

@Service
@SuppressWarnings("rawtypes")
public class ValidatorsServiceImpl implements ValidatorsService {

	@Autowired
	private ApplicationContext context;
	private Map<String, AbstractConstraintValidator> maps;

	protected Supplier<NotFoundException> notFound = () -> new NotFoundException("Validator");

	@PostConstruct
	private void init() {
		maps = context.getBeansOfType(AbstractConstraintValidator.class);
	}

	@Override
	public int addValidators(String name, Pair<String, String>[] params) {
		AbstractConstraintValidator<?> constaint = readConstaintValidator(name);
		return constaint.initialize(params);
	}

	@Override
	public int addValidators(String name, Pair<String, String>[] params, Class<?> clazzType) {
		AbstractConstraintValidator<?> constaint = readConstaintValidator(name);
		if (!constaint.getClazzType().isAssignableFrom(clazzType)) {
			throw new NotAcceptedException();
		}

		return constaint.initialize(params);
	}

	@Override
	public List<Validators> readValidators() {
		return maps.entrySet().stream().map(this::mapValidator).collect(Collectors.toList());
	}

	@Override
	public AbstractConstraintValidator readConstaintValidator(String name) {
		AbstractConstraintValidator constaint = maps.get(name);

		if (constaint == null) {
			throw notFound.get();
		}

		return constaint;
	}

	private Validators mapValidator(Map.Entry<String, AbstractConstraintValidator> map) {

		AbstractConstraintValidator constraint = map.getValue();
		Validators v = new Validators();
		v.setName(map.getKey());
		v.setAcceptClazz(constraint.getClazzType());
		ParamsConstraintValidator conPar = constraint.getParamValidator();
		v.setParams(conPar != null ? conPar.getParameters() : null);
		return v;

	}


	private static Map<MetadataField, Map<AbstractConstraintValidator, Integer>> fieldKeeper = new IdentityHashMap<>();

	private static synchronized void addField(MetadataField field, AbstractConstraintValidator constraint, int context) {
		Map<AbstractConstraintValidator, Integer> constraintmap = fieldKeeper.get(field);
		if (constraintmap == null) {
			constraintmap = new IdentityHashMap<>();
			fieldKeeper.put(field, constraintmap);
		}
		constraintmap.put(constraint, context);
	}

	@SuppressWarnings("unchecked")
	private synchronized void validate(Object value, FieldValidators validator, MetadataField fieldId) {

		AbstractConstraintValidator constrain = readConstaintValidator(validator.getName());

		Map<AbstractConstraintValidator, Integer> constraintmap = fieldKeeper.get(fieldId);

		int contextId;

		Integer saveContext;

		if (constraintmap == null || (saveContext = constraintmap.get(constrain)) == null) {
			contextId = constrain.initialize(validator.readParams());
			addField(fieldId, constrain, contextId);
		} else {
			contextId = saveContext;
		}

		if (!constrain.isValid(value, contextId)) {
			throw new NotAcceptedException();
		}

	}

	@Override
	public void validate(Object value, MetadataField field) {
		if (field.getValidators() == null) {
			return;
		}
		for (FieldValidators fieldValidators : field.getValidators()) {
			validate(value, fieldValidators, field);
		}
	}

}
