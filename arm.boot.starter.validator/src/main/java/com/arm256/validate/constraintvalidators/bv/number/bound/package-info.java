

/**
 * Contains {@link javax.validation.ConstraintValidator} implementations for
 * min and max constraints for numbers.
 *
 * @author Marko Bekhta
 */
package com.arm256.validate.constraintvalidators.bv.number.bound;
