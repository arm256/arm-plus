package com.arm256.validate.constraintvalidators.params;

import org.springframework.stereotype.Service;

import com.arm256.validator.AbstractParametersConstraintValidator;

@Service
public class NoparamConstraint extends AbstractParametersConstraintValidator<Nothing> {

}
