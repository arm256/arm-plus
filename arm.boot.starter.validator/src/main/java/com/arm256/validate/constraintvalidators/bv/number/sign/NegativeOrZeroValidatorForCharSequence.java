
package com.arm256.validate.constraintvalidators.bv.number.sign;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.arm256.validator.AbstractConstraintValidator;


@Service("NegativeOrZeroString")
public class NegativeOrZeroValidatorForCharSequence extends AbstractConstraintValidator<CharSequence> {

	@Override
	public boolean isValid(CharSequence value, int context) {
		// null values are valid
		if ( value == null ) {
			return false;
		}

		try {
			return NumberSignHelper.signum(new BigDecimal(value.toString()), NumberSignHelper.GREATER_THAN) <= 0;
		}
		catch (NumberFormatException nfe) {
			return false;
		}
	}
}
