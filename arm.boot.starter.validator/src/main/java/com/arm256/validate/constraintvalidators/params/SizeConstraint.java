package com.arm256.validate.constraintvalidators.params;

import jakarta.validation.constraints.Size;

import org.springframework.stereotype.Service;

import com.arm256.utils.Pair;
import com.arm256.validator.AbstractParametersConstraintValidator;
import com.arm256.validator.Parameters;

@Service
public class SizeConstraint extends AbstractParametersConstraintValidator<Size> {

	public SizeConstraint() {
		super(new Parameters<Integer>("min", false, true, Integer.class),
				new Parameters<Integer>("max", false, true, Integer.class));
	}
	@Override
	public Object[] validatorParam(Pair<String, String>[] constraintParameter) {

		requiredParam(constraintParameter);

		int min = getParameterValue(0, "min", constraintParameter);
		int max = getParameterValue(1, "max", constraintParameter);

		if (min < 0) {
			throw new IllegalArgumentException("The min parameter cannot be negative.");
		}
		if (max < 0) {
			throw new IllegalArgumentException("The max parameter cannot be negative.");
		}
		if (max < min) {
			throw new IllegalArgumentException("The length cannot be negative.");
		}


		return new Object[] { min, max };
	}

}
