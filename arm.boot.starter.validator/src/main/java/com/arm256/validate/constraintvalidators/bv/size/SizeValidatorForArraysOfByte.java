
package com.arm256.validate.constraintvalidators.bv.size;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;

@Service("SizeArrayByte")
public class SizeValidatorForArraysOfByte extends SizeValidatorForArray<Byte> {

	public SizeValidatorForArraysOfByte(SizeConstraint p) {
		super(p);
	}

}