package com.arm256.validate.constraintvalidators.bv;

import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.AbstractEmailValidator;
import com.arm256.validate.constraintvalidators.params.EmailConstraint;

@Service("Email")
public class EmailValidator extends AbstractEmailValidator {


	public EmailValidator(EmailConstraint paramValidator) {
		super(paramValidator);
	}

	@Override
	public boolean isValid(CharSequence value, int context) {
		if (value == null) {
			return false;
		}
		Pattern pattern = getParams(context, 0);

		boolean isValid = super.isValid( value, context );
		if ( pattern == null || !isValid ) {
			return isValid;
		}



		return pattern.matcher(value).matches();
	}

}
