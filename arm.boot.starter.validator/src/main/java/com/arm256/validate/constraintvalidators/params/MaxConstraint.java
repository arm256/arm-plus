package com.arm256.validate.constraintvalidators.params;

import jakarta.validation.constraints.Max;

import org.springframework.stereotype.Service;

import com.arm256.utils.Pair;
import com.arm256.validator.AbstractParametersConstraintValidator;
import com.arm256.validator.Parameters;

@Service
public class MaxConstraint extends AbstractParametersConstraintValidator<Max> {

	public MaxConstraint() {
		super(new Parameters<Long>("value", false, true, Long.class));
	}
	@Override
	public Object[] validatorParam(Pair<String, String>[] constraintParameter) {

		requiredParam(constraintParameter);

		long max = getParameterValue(0, "value", constraintParameter);

		return new Object[] { max };
	}

}
