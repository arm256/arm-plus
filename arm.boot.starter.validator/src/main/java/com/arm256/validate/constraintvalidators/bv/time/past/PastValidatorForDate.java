
package com.arm256.validate.constraintvalidators.bv.time.past;

import java.time.Instant;
import java.util.Date;

/**
 * Check that the {@code java.util.Date} passed to be validated is in the
 * past.
 *
 * @author Alaa Nassef
 * @author Guillaume Smet
 */
public class PastValidatorForDate extends AbstractPastInstantBasedValidator<Date> {

	@Override
	protected Instant getInstant(Date value) {
		return value.toInstant();
	}

}
