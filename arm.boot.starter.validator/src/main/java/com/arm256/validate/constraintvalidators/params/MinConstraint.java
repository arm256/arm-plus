package com.arm256.validate.constraintvalidators.params;

import jakarta.validation.constraints.Min;

import org.springframework.stereotype.Service;

import com.arm256.utils.Pair;
import com.arm256.validator.AbstractParametersConstraintValidator;
import com.arm256.validator.Parameters;

@Service
public class MinConstraint extends AbstractParametersConstraintValidator<Min> {

	public MinConstraint() {
		super(new Parameters<Long>("value", false, true, Long.class));
	}
	@Override
	public Object[] validatorParam(Pair<String, String>[] constraintParameter) {

		requiredParam(constraintParameter);

		long min = getParameterValue(0, "value", constraintParameter);

		return new Object[] { min };
	}

}
