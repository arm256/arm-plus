
package com.arm256.validate.constraintvalidators.bv.notempty;

import org.springframework.stereotype.Service;

import com.arm256.validator.AbstractConstraintValidator;


@Service("NotEmptyArray")
public class NotEmptyValidatorForArray<T> extends AbstractConstraintValidator<T[]> {

	/**
	 * Checks the array is not {@code null} and not empty.
	 *
	 * @param array the array to validate
	 * @param constraintValidatorContext context in which the constraint is evaluated
	 * @return returns {@code true} if the array is not {@code null} and the array is not empty
	 */

	@Override
	public boolean isValid(T[] array, int context) {
		if ( array == null ) {
			return false;
		}
		return array.length > 0;
	}

}
