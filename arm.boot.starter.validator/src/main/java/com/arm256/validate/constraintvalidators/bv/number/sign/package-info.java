

/**
 * Contains {@link javax.validation.ConstraintValidator} implementations for
 * {@link javax.validation.constraints.Positive}, {@link javax.validation.constraints.PositiveOrZero},
 * {@link javax.validation.constraints.Negative} and {@link javax.validation.constraints.NegativeOrZero}
 * constraints.
 *
 * @author Marko Bekhta
 */
package com.arm256.validate.constraintvalidators.bv.number.sign;
