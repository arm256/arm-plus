
package com.arm256.validate.constraintvalidators.bv.time.future;

import java.time.Clock;
import java.time.YearMonth;

/**
 * Check that the {@code java.time.YearMonth} passed is in the future.
 *
 * @author Guillaume Smet
 */
public class FutureValidatorForYearMonth extends AbstractFutureJavaTimeValidator<YearMonth> {

	@Override
	protected YearMonth getReferenceValue(Clock reference) {
		return YearMonth.now( reference );
	}

}
