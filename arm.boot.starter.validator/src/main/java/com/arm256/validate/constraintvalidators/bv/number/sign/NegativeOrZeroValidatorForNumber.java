
package com.arm256.validate.constraintvalidators.bv.number.sign;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.bv.number.InfinityNumberComparatorHelper;
import com.arm256.validator.AbstractConstraintValidator;

@Service("NegativeOrZero")
public class NegativeOrZeroValidatorForNumber extends AbstractConstraintValidator<Number> {

	@Override
	public boolean isValid(Number value, int context) {
		// null values are valid
		if ( value == null ) {
			return false;
		}

		return NumberSignHelper.signum(value, InfinityNumberComparatorHelper.GREATER_THAN) <= 0;
	}

}
