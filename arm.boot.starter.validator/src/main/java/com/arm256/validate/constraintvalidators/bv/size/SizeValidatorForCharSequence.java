
package com.arm256.validate.constraintvalidators.bv.size;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("SizeChar")
public class SizeValidatorForCharSequence extends AbstractConstraintValidator<CharSequence> {

	public SizeValidatorForCharSequence(SizeConstraint p) {
		super(p);
	}

	@Override
	public boolean isValid(CharSequence charSequence, int context) {
		if ( charSequence == null ) {
			return true;
		}

		int min = getParams(context, 0);
		int max = getParams(context, 1);

		int length = charSequence.length();
		return length >= min && length <= max;
	}

}
