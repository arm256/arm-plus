package com.arm256.validate.constraintvalidators.params;

import jakarta.validation.constraints.Digits;

import org.springframework.stereotype.Service;

import com.arm256.utils.Pair;
import com.arm256.validator.AbstractParametersConstraintValidator;
import com.arm256.validator.Parameters;

@Service
public class DigitsConstraint extends AbstractParametersConstraintValidator<Digits> {
	private final static Parameters<Integer> integer = new Parameters<Integer>("integer", false, true, Integer.class);
	private final static Parameters<Integer> fraction = new Parameters<Integer>("fraction", false, true, Integer.class);

	public DigitsConstraint() {
		super(integer, fraction);
	}

	@Override
	public Object[] validatorParam(Pair<String, String>[] constraintParameter) {

		requiredParam(constraintParameter);

		int maxIntegerLength = getParameterValue(0, "integer", constraintParameter);
		int maxFractionLength = getParameterValue(1, "fraction", constraintParameter);

		if (maxIntegerLength < 0) {
			throw new IllegalArgumentException("The length of the integer part cannot be negative.");
		}

		if (maxFractionLength < 0) {
			throw new IllegalArgumentException("The length of the fraction part cannot be negative.");
		}

		return new Object[] { maxIntegerLength, maxFractionLength };
	}

}
