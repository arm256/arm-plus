
package com.arm256.validate.constraintvalidators.bv.time.past;

import java.time.Clock;
import java.time.chrono.ThaiBuddhistDate;

/**
 * Check that the {@code java.time.chrono.ThaiBuddhistDate} passed is in the past.
 *
 * @author Guillaume Smet
 */
public class PastValidatorForThaiBuddhistDate extends AbstractPastJavaTimeValidator<ThaiBuddhistDate> {

	@Override
	protected ThaiBuddhistDate getReferenceValue(Clock reference) {
		return ThaiBuddhistDate.now( reference );
	}

}
