
package com.arm256.validate.constraintvalidators.bv.number.bound;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.OptionalInt;
import java.util.function.ToIntBiFunction;

import com.arm256.validate.constraintvalidators.bv.number.InfinityNumberComparatorHelper;

final class NumberComparatorHelper {

	private NumberComparatorHelper() {
	}

	public static final OptionalInt LESS_THAN = OptionalInt.of(-1);
	public static final OptionalInt GREATER_THAN = OptionalInt.of(1);

	public static ToIntBiFunction<Long, Long> longFun = (e, v) -> e.compareTo(v);
	public static ToIntBiFunction<Integer, Long> intFun = (e, v) -> compare(e.longValue(), v);
	public static ToIntBiFunction<Short, Long> shortFun = (e, v) -> compare(e.longValue(), v);
	public static ToIntBiFunction<Byte, Long> byteFun = (e, v) -> compare(e.longValue(), v);
	public static ToIntBiFunction<BigInteger, Long> bigIntFun = (e, v) -> e.compareTo(BigInteger.valueOf(v));
	public static ToIntBiFunction<BigDecimal, Long> bigDecimalFun = (e, v) -> e.compareTo(BigDecimal.valueOf(v));

	public static ToIntBiFunction<Float, Long> floatFunLess = (e, v) -> compare(e, v, LESS_THAN);
	public static ToIntBiFunction<Double, Long> doubleFunLess = (e, v) -> compare(e, v, LESS_THAN);

	public static ToIntBiFunction<Float, Long> floatFunGreate = (e, v) -> compare(e, v, GREATER_THAN);
	public static ToIntBiFunction<Double, Long> doubleFunGreate = (e, v) -> compare(e, v, GREATER_THAN);

	public static ToIntBiFunction<Number, Long> numberFun = (e, v) -> Double.compare(e.doubleValue(), 0D);

	public static Class<?>[] clazzs = new Class[] { Long.class, Integer.class, Short.class, Byte.class,
			BigInteger.class, BigDecimal.class, Float.class, Double.class, Number.class };

	@SuppressWarnings("unchecked")
	public static ToIntBiFunction<Object, Long>[] funs = new ToIntBiFunction[] { longFun, intFun, shortFun, byteFun,
			bigIntFun,
			bigDecimalFun, floatFunLess, doubleFunLess, floatFunGreate,
			doubleFunGreate, numberFun };


	public static int compare(Long va1, Long va2) {
		return va1.compareTo(va2);
	}

	public static <E extends Number> int compare(E e, long value, OptionalInt treatNanAs) {
		int index = -1;

		loop: for (Class<?> clazz : clazzs) {
			index++;
			if (e.getClass().isAssignableFrom(clazz)) {
				break loop;
			}
		}

		if ((treatNanAs == GREATER_THAN)
				&& (e.getClass().isAssignableFrom(Float.class) || e.getClass().isAssignableFrom(Double.class))) {
			index += 2;
		}

		return funs[index].applyAsInt(e, value);
	}

	public static int compare(Double number, long value, OptionalInt treatNanAs) {
		OptionalInt infinity = InfinityNumberComparatorHelper.infinityCheck( number, treatNanAs );
		if ( infinity.isPresent() ) {
			return infinity.getAsInt();
		}
		return Double.compare( number, value );
	}

	public static int compare(Float number, long value, OptionalInt treatNanAs) {
		OptionalInt infinity = InfinityNumberComparatorHelper.infinityCheck( number, treatNanAs );
		if ( infinity.isPresent() ) {
			return infinity.getAsInt();
		}
		return Float.compare( number, value );
	}
}
