
package com.arm256.validate.constraintvalidators.bv.number.bound.decimal;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.OptionalInt;
import java.util.function.ToIntBiFunction;

import com.arm256.validate.constraintvalidators.bv.number.InfinityNumberComparatorHelper;

final class DecimalNumberComparatorHelper {

	private DecimalNumberComparatorHelper() {
	}

	public static final OptionalInt LESS_THAN = OptionalInt.of(-1);
	public static final OptionalInt GREATER_THAN = OptionalInt.of(1);

	public static ToIntBiFunction<Long, BigDecimal> longFun = (e, v) -> compare(e, v);
	public static ToIntBiFunction<Integer, BigDecimal> intFun = (e, v) -> compare(e.longValue(), v);
	public static ToIntBiFunction<Short, BigDecimal> shortFun = (e, v) -> compare(e.longValue(), v);
	public static ToIntBiFunction<Byte, BigDecimal> byteFun = (e, v) -> compare(e.longValue(), v);
	public static ToIntBiFunction<BigInteger, BigDecimal> bigIntFun = (e, v) -> compare(e, v);
	public static ToIntBiFunction<BigDecimal, BigDecimal> bigDecimalFun = (e, v) -> compare(e, v);

	public static ToIntBiFunction<Float, BigDecimal> floatFunLess = (e, v) -> compare(e, v, LESS_THAN);
	public static ToIntBiFunction<Double, BigDecimal> doubleFunLess = (e, v) -> compare(e, v, LESS_THAN);

	public static ToIntBiFunction<Float, BigDecimal> floatFunGreate = (e, v) -> compare(e, v, GREATER_THAN);
	public static ToIntBiFunction<Double, BigDecimal> doubleFunGreate = (e, v) -> compare(e, v, GREATER_THAN);

	public static ToIntBiFunction<Number, BigDecimal> numberFun = (e, v) -> Double.compare(e.doubleValue(), 0D);

	public static Class<?>[] clazzs = new Class[] { Long.class, Integer.class, Short.class, Byte.class,
			BigInteger.class, BigDecimal.class, Float.class, Double.class, Number.class };

	@SuppressWarnings("unchecked")
	public static ToIntBiFunction<Object, BigDecimal>[] funs = new ToIntBiFunction[] { longFun, intFun, shortFun,
			byteFun,
			bigIntFun, bigDecimalFun, floatFunLess, doubleFunLess, floatFunGreate, doubleFunGreate, numberFun };


	public static int compare(BigDecimal number, BigDecimal value) {
		return number.compareTo( value );
	}

	public static int compare(BigInteger number, BigDecimal value) {
		return new BigDecimal( number ).compareTo( value );
	}

	public static int compare(Long number, BigDecimal value) {
		return BigDecimal.valueOf( number ).compareTo( value );
	}

	public static <E extends Number> int compare(E e, BigDecimal value, OptionalInt treatNanAs) {
		int index = -1;

		loop: for (Class<?> clazz : clazzs) {
			index++;
			if (e.getClass().isAssignableFrom(clazz)) {
				break loop;
			}
		}

		if ((treatNanAs == GREATER_THAN)
				&& (e.getClass().isAssignableFrom(Float.class) || e.getClass().isAssignableFrom(Double.class))) {
			index += 2;
		}

		return funs[index].applyAsInt(e, value);
	}

	public static int compare(Double number, BigDecimal value, OptionalInt treatNanAs) {
		OptionalInt infinity = InfinityNumberComparatorHelper.infinityCheck( number, treatNanAs );
		if ( infinity.isPresent() ) {
			return infinity.getAsInt();
		}
		return BigDecimal.valueOf( number ).compareTo( value );
	}

	public static int compare(Float number, BigDecimal value, OptionalInt treatNanAs) {
		OptionalInt infinity = InfinityNumberComparatorHelper.infinityCheck( number, treatNanAs );
		if ( infinity.isPresent() ) {
			return infinity.getAsInt();
		}
		return BigDecimal.valueOf( number ).compareTo( value );
	}


}
