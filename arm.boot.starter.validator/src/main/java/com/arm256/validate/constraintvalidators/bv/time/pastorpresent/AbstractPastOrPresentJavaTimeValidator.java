
package com.arm256.validate.constraintvalidators.bv.time.pastorpresent;

import java.time.Duration;
import java.time.temporal.TemporalAccessor;

import jakarta.validation.constraints.PastOrPresent;

import com.arm256.validate.constraintvalidators.bv.time.AbstractJavaTimeValidator;


public abstract class AbstractPastOrPresentJavaTimeValidator<T extends TemporalAccessor & Comparable<? super T>>
		extends AbstractJavaTimeValidator<PastOrPresent, T> {

	@Override
	protected boolean isValid(int result) {
		return result <= 0;
	}

	@Override
	protected Duration getEffectiveTemporalValidationTolerance(Duration absoluteTemporalValidationTolerance) {
		return absoluteTemporalValidationTolerance;
	}
}
