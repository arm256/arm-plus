
package com.arm256.validate.constraintvalidators.bv.size;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;

@Service("SizeArrayShort")
public class SizeValidatorForArraysOfShort extends SizeValidatorForArray<Long> {

	public SizeValidatorForArraysOfShort(SizeConstraint p) {
		super(p);
	}

}