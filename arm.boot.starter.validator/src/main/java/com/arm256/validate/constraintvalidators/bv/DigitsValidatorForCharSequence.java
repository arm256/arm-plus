package com.arm256.validate.constraintvalidators.bv;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.DigitsConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("DigitsChar")
public class DigitsValidatorForCharSequence extends AbstractConstraintValidator<CharSequence> {


	public DigitsValidatorForCharSequence(DigitsConstraint c) {
		super(c);
	}

	@Override
	public boolean isValid(CharSequence charSequence, int context) {
		//null values are valid
		if ( charSequence == null ) {
			return false;
		}

		BigDecimal bigNum = getBigDecimalValue( charSequence );
		if ( bigNum == null ) {
			return false;
		}

		int maxIntegerLength = getParams(context, 0);
		int maxFractionLength = getParams(context, 1);

		int integerPartLength = bigNum.precision() - bigNum.scale();
		int fractionPartLength = bigNum.scale() < 0 ? 0 : bigNum.scale();

		return ( maxIntegerLength >= integerPartLength && maxFractionLength >= fractionPartLength );
	}

	private BigDecimal getBigDecimalValue(CharSequence charSequence) {
		BigDecimal bd;
		try {
			bd = new BigDecimal( charSequence.toString() );
		}
		catch (NumberFormatException nfe) {
			return null;
		}
		return bd;
	}

}
