package com.arm256.validate.constraintvalidators.bv;

import org.springframework.stereotype.Service;

import com.arm256.validator.AbstractConstraintValidator;


@Service("Null")
public class NullValidator extends AbstractConstraintValidator<Object> {

	@Override
	public boolean isValid(Object object, int context) {
		return object == null;
	}

}
