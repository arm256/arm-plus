
package com.arm256.validate.constraintvalidators.bv.time.pastorpresent;

import java.time.Duration;
import java.time.Instant;

import jakarta.validation.constraints.PastOrPresent;

import com.arm256.validate.constraintvalidators.bv.time.AbstractInstantBasedTimeValidator;


public abstract class AbstractPastOrPresentInstantBasedValidator<T> extends AbstractInstantBasedTimeValidator<PastOrPresent, T> {

	@Override
	protected boolean isValid(int result) {
		return result <= 0;
	}

	@Override
	protected Duration getEffectiveTemporalValidationTolerance(Duration absoluteTemporalValidationTolerance) {
		return absoluteTemporalValidationTolerance;
	}
}
