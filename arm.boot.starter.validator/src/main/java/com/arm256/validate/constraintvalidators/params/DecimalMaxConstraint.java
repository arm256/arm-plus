package com.arm256.validate.constraintvalidators.params;

import java.math.BigDecimal;

import jakarta.validation.constraints.DecimalMax;

import org.springframework.stereotype.Service;

import com.arm256.utils.Pair;
import com.arm256.validator.AbstractParametersConstraintValidator;
import com.arm256.validator.Parameters;

@Service
public class DecimalMaxConstraint extends AbstractParametersConstraintValidator<DecimalMax> {

	public DecimalMaxConstraint() {
		super(new Parameters<String>("value", false, true, String.class),
				new Parameters<Boolean>("inclusive", false, false, Boolean.class, true));
	}
	@Override
	public Object[] validatorParam(Pair<String, String>[] constraintParameter) {

		requiredParam(constraintParameter);

		String value = getParameterValue(0, "value", constraintParameter);
		BigDecimal maxValue;
		try {
			maxValue = new BigDecimal(value);
		} catch (NumberFormatException nfe) {
			throw new IllegalArgumentException(value + " does not represent a valid BigDecimal format.");
		}

		Boolean inclusive = getParameterValue(1, "inclusive", constraintParameter);

		return new Object[] { maxValue, inclusive };
	}

}
