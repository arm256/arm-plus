
package com.arm256.validate.constraintvalidators.bv.time.future;

import java.time.Clock;
import java.time.chrono.ThaiBuddhistDate;

/**
 * Check that the {@code java.time.chrono.ThaiBuddhistDate} passed is in the future.
 *
 * @author Guillaume Smet
 */
public class FutureValidatorForThaiBuddhistDate extends AbstractFutureJavaTimeValidator<ThaiBuddhistDate> {

	@Override
	protected ThaiBuddhistDate getReferenceValue(Clock reference) {
		return ThaiBuddhistDate.now( reference );
	}

}
