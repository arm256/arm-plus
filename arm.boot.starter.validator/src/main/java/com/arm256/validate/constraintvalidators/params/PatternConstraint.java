package com.arm256.validate.constraintvalidators.params;

import java.util.List;
import java.util.regex.PatternSyntaxException;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Pattern.Flag;

import org.springframework.stereotype.Service;

import com.arm256.utils.Pair;
import com.arm256.validator.AbstractParametersConstraintValidator;
import com.arm256.validator.Parameters;

@Service
public class PatternConstraint extends AbstractParametersConstraintValidator<Pattern> {

	public PatternConstraint() {
		super(new Parameters<String>("regexp", false, true, String.class),
				new Parameters<Flag>("flags", true, false, Flag.class));
	}
	@Override
	public Object[] validatorParam(Pair<String, String>[] constraintParameter) {

		requiredParam(constraintParameter);

		String regexp = getParameterValue(0, "regexp", constraintParameter);
		int intFlag = 0;
		if (constraintParameter.length > 1) {
			List<Flag> flags = getParameterValue(1, "flags", constraintParameter);
			for (Flag flag : flags) {
				intFlag = intFlag | flag.getValue();
			}
		}

		try {
			return new Object[] { java.util.regex.Pattern.compile(regexp, intFlag) };
		} catch (PatternSyntaxException e) {
			throw new IllegalArgumentException("Invalid regular expression.");
		}
	}

}
