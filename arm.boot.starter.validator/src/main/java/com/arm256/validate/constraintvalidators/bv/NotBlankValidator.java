package com.arm256.validate.constraintvalidators.bv;

import org.springframework.stereotype.Service;

import com.arm256.validator.AbstractConstraintValidator;


@Service("NotBlank")
public class NotBlankValidator extends AbstractConstraintValidator<CharSequence> {

	@Override
	public boolean isValid(CharSequence charSequence, int context) {
		if ( charSequence == null ) {
			return false;
		}

		return charSequence.toString().trim().length() > 0;
	}
}
