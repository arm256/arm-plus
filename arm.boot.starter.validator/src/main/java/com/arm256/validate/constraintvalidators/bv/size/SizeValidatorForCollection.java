
package com.arm256.validate.constraintvalidators.bv.size;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@SuppressWarnings("rawtypes")
@Service("SizeCollection")
public class SizeValidatorForCollection extends AbstractConstraintValidator<Collection> {

	public SizeValidatorForCollection(SizeConstraint p) {
		super(p);
	}

	/**
	 * Checks the number of entries in a collection.
	 *
	 * @param collection the collection to validate
	 * @param constraintValidatorContext context in which the constraint is evaluated
	 *
	 * @return {@code true} if the collection is {@code null} or the number of entries in
	 *         {@code collection} is between the specified {@code min} and {@code max} values (inclusive),
	 *         {@code false} otherwise.
	 */
	@Override
	public boolean isValid(Collection collection, int context) {
		if ( collection == null ) {
			return false;
		}

		int min = getParams(context, 0);
		int max = getParams(context, 1);

		int length = collection.size();
		return length >= min && length <= max;
	}

}
