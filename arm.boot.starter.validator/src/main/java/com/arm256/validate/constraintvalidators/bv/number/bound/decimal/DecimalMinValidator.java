
package com.arm256.validate.constraintvalidators.bv.number.bound.decimal;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.DecimalMinConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("DecimalMin")
public class DecimalMinValidator extends AbstractConstraintValidator<Number> {

	public DecimalMinValidator(DecimalMinConstraint dm) {
		super(dm);
	}



	@Override
	public boolean isValid(Number value, int context) {
		// null values are valid
		if ( value == null ) {
			return false;
		}

		BigDecimal minValue = getParams(context, 0);
		boolean inclusive = getParams(context, 1);

		int comparisonResult = DecimalNumberComparatorHelper.compare(value, minValue,
				DecimalNumberComparatorHelper.LESS_THAN);

		return inclusive ? comparisonResult >= 0 : comparisonResult > 0;
	}

}
