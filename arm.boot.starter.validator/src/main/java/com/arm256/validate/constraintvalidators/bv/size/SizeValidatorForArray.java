
package com.arm256.validate.constraintvalidators.bv.size;

import org.springframework.stereotype.Service;

import com.arm256.validate.constraintvalidators.params.SizeConstraint;
import com.arm256.validator.AbstractConstraintValidator;

@Service("SizeArray")
public class SizeValidatorForArray<T extends Object> extends AbstractConstraintValidator<T[]> {

	public SizeValidatorForArray(SizeConstraint p) {
		super(p);
	}

	@Override
	public boolean isValid(T[] array, int context) {
		if ( array == null ) {
			return false;
		}

		int min = getParams(context, 0);
		int max = getParams(context, 1);

		return array.length >= min && array.length <= max;
	}

}
