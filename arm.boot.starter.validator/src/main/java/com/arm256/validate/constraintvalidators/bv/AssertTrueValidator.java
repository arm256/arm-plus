package com.arm256.validate.constraintvalidators.bv;

import org.springframework.stereotype.Service;

import com.arm256.validator.AbstractConstraintValidator;


@Service("AssertTrue")
public class AssertTrueValidator extends AbstractConstraintValidator<Boolean> {

	@Override
	public boolean isValid(Boolean bool, int context) {
		// null values are invalid
		return bool != null && bool;
	}

}
