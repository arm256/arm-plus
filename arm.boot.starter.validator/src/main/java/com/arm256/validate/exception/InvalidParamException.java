package com.arm256.validate.exception;

@SuppressWarnings("serial")
public class InvalidParamException extends ValidationException {

	public InvalidParamException() {
		super("SYS_VAL_0", "validation.constraints.param.invalid");
	}
}
