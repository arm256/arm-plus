package com.arm256.validate.exception;

import com.arm256.exception.AbstractArmException;

@SuppressWarnings("serial")
public class ValidationException extends AbstractArmException {

	public ValidationException(String code, String messageCode) {
		super(code, messageCode);
	}
}
