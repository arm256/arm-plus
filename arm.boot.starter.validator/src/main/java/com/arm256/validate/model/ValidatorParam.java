package com.arm256.validate.model;

import com.arm256.model.AbstractModel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ValidatorParam extends AbstractModel<Long> {
	
	
	private String name;

	private Class<?> clazzType;

	private boolean multi = false;

	private String defalutValue;

	private boolean required;

}
