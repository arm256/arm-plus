package com.arm256.validate.model;

import java.util.ArrayList;
import java.util.List;

import com.arm256.model.AbstractModel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Validator extends AbstractModel<Long> {
	
	private String name;

	private Class<?> constraintViolation;

	private Class<?> acceptClazz;

	private List<ValidatorParam> params = new ArrayList<>();

}
